#include "connectivity.hpp"
#include "onboarding-basic/onboarding-client-basic.hpp"

#include "display/u8g2-display.hpp"
#include "credentials.hpp"
#include "consts.hpp"
#include "logger.hpp"

#define LOG(...) LOGGER(Main, __VA_ARGS__)

#if defined(SECURITY_ECC_128) || defined(SECURITY_ECC_HYBRID) || defined(SECURITY_ECC_80)
  OnboardingClientBasic onboardingClient(Connectivity.getFace(), BOOTSTRAP_ECC_PUBLIC, BOOTSTRAP_ECC_PRIVATE);
#endif

//U8G2Display u8g2Display;

bool m_isBtnDown, m_wasBtnDown;
bool m_firstOnboarding = true;
bool m_firstGotNetworkPrefix = true;

void
setup()
{
  Serial.begin(9600);
  Serial.println();
  ndn::setLogOutput(Serial);

  Connectivity.begin();

  LOG("Made it past initializing connectivity.");

  // u8g2Display.begin();
  // u8g2Display.addLine(Connectivity.localIP().toString().c_str());
  // u8g2Display.display();

  LOG("Made it past initializing u8g2display.");

  onboardingClient.begin();

  LOG("Made it past initializing onboarding client.");
}

void
loop()
{
  Connectivity.loop();
  delay(1);

  m_wasBtnDown = false;
  bool isBtnDown = !digitalRead(PIN_BTN);
  if (!m_wasBtnDown && isBtnDown) {
    if (!onboardingClient.onboardingCompleted()) {
      onboardingClient.initiateSignOn();
      delay(1000);
    }
  }
  m_wasBtnDown = m_isBtnDown;

  if (onboardingClient.onboardingCompleted() && m_firstOnboarding) {

    m_firstOnboarding = false;

    // u8g2Display.addLine("Network prefix:");

    ndn_NameComponent tempComps[MAX_NAME_COMPS];
    ndn::NameLite tempName(tempComps, MAX_NAME_COMPS);
    
    for (int i = 0; i < onboardingClient.getNetworkPrefix().size(); i++)
      tempName.append(onboardingClient.getNetworkPrefix().get(i));

    LOG("Network prefix: " << ndn::PrintUri(onboardingClient.getNetworkPrefix()) << endl);
    LOG("Controller assigned ID: " << ndn::PrintUri(onboardingClient.getDeviceID()) << endl);

    onboardingClient.printTimeInformation();

    // u8g2Display.addLine(tempName);
    // u8g2Display.addLine("Controller assigned ID:");
    // u8g2Display.addLine(onboardingClient.getDeviceID());
    // u8g2Display.display();

  }
}