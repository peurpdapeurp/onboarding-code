#ifndef SIGN_ON_TLVS_HPP
#define SIGN_ON_TLVS_HPP

#include <stdint.h>

// tlv types for sign-on basic
constexpr uint8_t N2_PUB_TLV_TYPE = 0x82;
constexpr uint8_t KD_PRI_ENCRYPTED_TLV_TYPE = 0x81;

// tlv types for sign-on dynamic secret
constexpr uint8_t TOKEN_BYTES_TLV_TYPE = 0x83;

#endif // SIGN_ON_TLVS_HPP