
#include "print-byte-array-helper.hpp"

#include <Print.h>
#include <Arduino.h>

PrintByteArray::PrintByteArray(const uint8_t *arr, size_t start, size_t end)
{
  m_arr = arr;
  m_start = start;
  m_end = end;
}

size_t
PrintByteArray::printTo(Print& p) const
{
  size_t len = 0;
  int newLineCounter = 1;
  for (size_t i = 0; i < m_end - m_start; ++i) {
    len += this->printTo(p, m_arr[i]);

    if (newLineCounter > 9) {
      newLineCounter = 0;
      len += p.print('\n');
    }

    newLineCounter++;
  }

  return len;
}

size_t
PrintByteArray::printTo(Print& p, uint8_t X) const
{
  size_t len = 0;

  len += p.print("0x");

  if (X < 16) {
    len += p.print("0");
  }

  len += p.print(X, HEX);
  len += p.print(", ");

  return len;
}


// void
// p(uint8_t X) {

//   Serial.print("0x");

//   if (X < 16) {
//     Serial.print("0");
//   }


//   Serial.print(X, HEX);
//   Serial.print(", ");

// }

// void
// printByteArray(const uint8_t *arr, int start, int end) {

//   int counter = 1;
//   for (int i = start; i < end; i++) {

//     p(arr[i]);

//     if (counter > 9) {
//       counter = 0;
//       Serial.println("");
//     }
//     counter++;
//   }
//   Serial.println("");
//   Serial.println("");

// }