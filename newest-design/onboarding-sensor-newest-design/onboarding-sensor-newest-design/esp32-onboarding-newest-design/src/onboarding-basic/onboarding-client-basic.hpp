#ifndef ONBOARDING_CLIENT_BASIC_HPP
#define ONBOARDING_CLIENT_BASIC_HPP

#include <esp8266ndn.h>
#include "../consts.hpp"
#include "../helpers/security-helper.hpp"
#include <PriUint64.h>

/** \brief Onboarding client for secure sign-on protocol.
 */
class OnboardingClientBasic : public ndn::PacketHandler
{
public:
  explicit
  OnboardingClientBasic(ndn::Face& face, const uint8_t* BKpub, const uint8_t* BKpri);

  ~OnboardingClientBasic() noexcept;

  bool
  begin();

  bool
  initiateSignOn();

  bool
  onboardingCompleted() {
    return m_onboarding_completed;
  }

  const ndn::NameLite&
  getDeviceID() {
    return m_deviceID;
  }

  const ndn::NameLite&
  getNetworkPrefix() {
    return m_networkPrefix;
  }

private:

  bool
  sendBootstrappingRequest();

  bool 
  sendCertificateRequest();

  bool 
  processData(const ndn::DataLite& data, uint64_t endpointId) override;

  bool
  processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId);

  bool
  processCertificateRequestResponse(const ndn::DataLite& data, uint64_t endpointId);

  bool
  AKpubRetrieved() {
    for (int i = 0; i < 64; i++) {
      if (m_AKpub[i] != 0)
        return true;
    }
    return false;
  }

public: // functions related to timer evaluations

  // this time is set as soon as the device calls the sendBootstrappingRequest function
  uint64_t m_lastOnboardingStartTime;
  // this time is set as soon as the device sends the bootstrapping request
  uint64_t m_lastOnboardingSentBootstrapRequestTime;
  // this time is set as soon as the device receives the bootstrapping response
  uint64_t m_lastOnboardingReceivedBootstrapResponseTime;
  // this time is set as soon as the device finishes processing the bootstrapping response
  uint64_t m_lastOnboardingFinishedProcessingBootstrappingResponseTime;
  // this time is set as soon as the device sends the certificate request
  uint64_t m_lastOnboardingSentCertificateRequestTime;
  // this time is set as soon as the device receives the certificate request response
  uint64_t m_lastOnboardingReceivedCertificateRequestResponseTime;
  // this time is set as soon as the device finishes processing the certificate request response
  uint64_t m_lastOnboardingFinishTime;

  void
  printTimeInformation() {
    uint64_t T1 = m_lastOnboardingSentBootstrapRequestTime - m_lastOnboardingStartTime;
    uint64_t T2 = m_lastOnboardingReceivedBootstrapResponseTime - m_lastOnboardingSentBootstrapRequestTime;
    uint64_t T3 = m_lastOnboardingFinishedProcessingBootstrappingResponseTime - m_lastOnboardingReceivedBootstrapResponseTime;
    uint64_t T4 = m_lastOnboardingSentCertificateRequestTime - m_lastOnboardingFinishedProcessingBootstrappingResponseTime;
    uint64_t T5 = m_lastOnboardingReceivedCertificateRequestResponseTime - m_lastOnboardingSentCertificateRequestTime;
    uint64_t T6 = m_lastOnboardingFinishTime - m_lastOnboardingReceivedCertificateRequestResponseTime;
    uint64_t T7 = m_lastOnboardingFinishTime - m_lastOnboardingStartTime;
    uint64_t T8 = T1 + T3 + T4 + T6;

    Serial << "Timers from last onboarding:" << endl;
    Serial << "T1: " << PriUint64<DEC>(T1) << endl;
    Serial << "T2: " << PriUint64<DEC>(T2) << endl;
    Serial << "T3: " << PriUint64<DEC>(T3) << endl;
    Serial << "T4: " << PriUint64<DEC>(T4) << endl;
    Serial << "T5: " << PriUint64<DEC>(T5) << endl;
    Serial << "T6: " << PriUint64<DEC>(T6) << endl;
    Serial << "T7: " << PriUint64<DEC>(T7) << endl;
    Serial << "T8: " << PriUint64<DEC>(T8) << endl;

    Serial << "CSV format: " << endl;
    Serial << T1 << "," << T2 << "," << T3 << "," << T4 << "," << T5 << "," << T6 << "," << T7 << "," << T8 << endl;
  }

  

private:
  ndn::Face& m_face;

  char m_bootstrapPrefixString[13] = "/ndn/sign-on";
  ndn_NameComponent m_bootstrapPrefixComps[2];
  ndn::NameLite m_bootstrapPrefix;

  char m_certNameComponentString[6] = "/cert";
  ndn_NameComponent m_certNameComponentComps[1];
  ndn::NameLite m_certNameComponent;

  const uint8_t* m_BKpub;
  const uint8_t* m_BKpri;

  ndn_NameComponent m_dummyKeyComp[1];
  ndn::NameLite m_dummyKeyName;

  ndn_NameComponent m_deviceIDNameComp[1];
  ndn::NameLite m_deviceID;

  ndn_NameComponent m_networkPrefixComps[MAX_NAME_COMPS];
  ndn::NameLite m_networkPrefix;

  std::unique_ptr<ndn::PacketBuffer> m_lastBootstrapResponse;

  uint8_t m_N1pub[ECDH_AND_KD_PUBLIC_KEY_SIZE];
  uint8_t m_N1pri[ECDH_AND_KD_PRIVATE_KEY_SIZE];

  uint8_t m_N2pub[ECDH_AND_KD_PUBLIC_KEY_SIZE];

  uint8_t m_N1pubXORN2pub[64];

  uint8_t m_DkPub[ECDH_AND_KD_PUBLIC_KEY_SIZE];
  uint8_t m_DkPri[ECDH_AND_KD_PRIVATE_KEY_SIZE];

  uint8_t m_ECC_PUBLIC_DIGEST[ndn_SHA256_DIGEST_SIZE];
  uint8_t m_BKpubMacByDKpub[ndn_SHA256_DIGEST_SIZE];
  char m_BKpubDigestChars[65];

  ndn::EcPrivateKey m_bootstrapPrivateKey;
  uint8_t m_sharedSecret[32];

  SecurityHelper m_securityHelper;

  uint8_t m_AKpub[64];
  uint8_t m_trustAnchor[3000];
  size_t m_trustAnchorLength;

  uint8_t m_MY_DH_TOKEN[DH_ARRAY_SIZES];
  uint8_t m_OTHER_DH_TOKEN[DH_ARRAY_SIZES];
  uint8_t m_DH_SHARED_SECRET[DH_ARRAY_SIZES];

  uint8_t m_trustAnchorDigest[ndn_SHA256_DIGEST_SIZE];

  bool m_onboarding_completed;

};

#endif // ONBOARDING_CLIENT_BASIC_HPP
