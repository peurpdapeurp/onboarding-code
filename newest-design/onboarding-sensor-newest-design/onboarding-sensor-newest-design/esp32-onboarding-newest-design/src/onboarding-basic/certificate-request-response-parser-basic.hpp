#ifndef CERTIFICATE_REQUEST_RESPONSE_PARSER_HPP
#define CERTIFICATE_REQUEST_RESPONSE_PARSER_HPP

#include <esp8266ndn.h>
#include "../helpers/security-helper.hpp"

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class CertificateResponseParserBasic
{
public:
  explicit
  CertificateResponseParserBasic(const uint8_t* payload, size_t payloadLen);

  ~CertificateResponseParserBasic();

  // returns true if it successfully finds all the expected TLV types and TLV lengths with
  // correct lengthed TLV values
  // returns false if there are any errors during parsing
  bool
  checkForExpectedTlvs();

  bool
  verifyReceivedCertificateAndGetDeviceID(const uint8_t *AKpub, ndn::NameLite &nameDestination);

  bool
  getDevicePrivateKey(uint8_t *key, size_t keyLength, uint8_t *output);

  // populates destination name with controller assigned device ID (one name component)
  bool
  getDeviceID(ndn::NameLite &nameDestination);

private:
  const uint8_t* m_payload;
  size_t m_payloadLen;

  ndn_TlvDecoder m_received_certificate_decoder;

  size_t m_receivedDkPriEncryptedTlvTypeAndLengthSize;
  size_t m_receivedDkPriEncryptedTlvValueSize;
  size_t m_receivedCertificateTlvTypeAndLengthSize;
  size_t m_receivedCertificateTlvValueSize;

  char m_keyNameComponentString[4] = "KEY";
  ndn_NameComponent m_keyNameComp[1];
  ndn::NameLite m_keyNameComponent;

  SecurityHelper m_securityHelper;

};

#endif // CERTIFICATE_REQUEST_RESPONSE_PARSER_HPP