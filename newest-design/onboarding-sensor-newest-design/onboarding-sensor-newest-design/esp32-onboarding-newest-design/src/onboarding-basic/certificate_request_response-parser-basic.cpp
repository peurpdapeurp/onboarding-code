
#include "certificate-request-response-parser-basic.hpp"
#include "../consts.hpp"
#include <esp8266ndn.h>
#include "../helpers/tlv-helpers.hpp"
#include "../logger.hpp"
#include "../signOnTLVs/sign-on-tlvs.hpp"

#define LOG(...) LOGGER(CertificateResponseParserBasic, __VA_ARGS__)

CertificateResponseParserBasic::CertificateResponseParserBasic(const uint8_t* payload, size_t payloadLen)
  : m_keyNameComponent(m_keyNameComp, 1)
{
  m_payload = payload;
  m_payloadLen = payloadLen;

  ndn::parseNameFromUri(m_keyNameComponent, m_keyNameComponentString);
}

CertificateResponseParserBasic::~CertificateResponseParserBasic()
{

}

bool
CertificateResponseParserBasic::checkForExpectedTlvs()
{

  ndn_TlvDecoder_initialize(&m_received_certificate_decoder, m_payload, m_payloadLen);

  return readTypeAndLength(m_received_certificate_decoder, KD_PRI_ENCRYPTED_TLV_TYPE, 
    m_receivedDkPriEncryptedTlvTypeAndLengthSize, m_receivedDkPriEncryptedTlvValueSize, "KdPri encrypted") &&
    readTypeAndLength(m_received_certificate_decoder, DATA_TLV_TYPE, 
    m_receivedCertificateTlvTypeAndLengthSize, m_receivedCertificateTlvValueSize, "received certificate");

}

bool
CertificateResponseParserBasic::verifyReceivedCertificateAndGetDeviceID(const uint8_t *AKpub, 
  ndn::NameLite &nameDestination)
{
  ndn_TlvDecoder_initialize(&m_received_certificate_decoder, m_payload, m_payloadLen);

  if (!readTypeAndLength(m_received_certificate_decoder, KD_PRI_ENCRYPTED_TLV_TYPE, 
    m_receivedDkPriEncryptedTlvTypeAndLengthSize, m_receivedDkPriEncryptedTlvValueSize, "KdPri encrypted")) {
    return false;
  }

  if (!readTypeAndLength(m_received_certificate_decoder, DATA_TLV_TYPE, 
    m_receivedCertificateTlvTypeAndLengthSize, m_receivedCertificateTlvValueSize, "received certificate")) {
    return false;
  }

  ndn_NameComponent receivedCertificateNameComps[MAX_NAME_COMPS];
  ndn_NameComponent receivedCertificateKeyLocatorNameComps[MAX_NAME_COMPS];
  ndn::DataLite receivedCertificate(receivedCertificateNameComps, MAX_NAME_COMPS, 
    receivedCertificateKeyLocatorNameComps, MAX_NAME_COMPS);

  uint8_t encoding[3000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t receivedCertificateSize = m_receivedCertificateTlvTypeAndLengthSize + m_receivedCertificateTlvValueSize;
  size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::decodeData
      (receivedCertificate, m_payload  + m_receivedDkPriEncryptedTlvTypeAndLengthSize + m_receivedDkPriEncryptedTlvValueSize, 
      m_receivedCertificateTlvTypeAndLengthSize + m_receivedCertificateTlvValueSize, &signedPortionBeginOffset,
      &signedPortionEndOffset))) {
      LOG("Error decoding received certificate.");
      LOG("Error: " << ndn_getErrorString(error) << endl);
      return false;
  }

  if ((error = ndn::Tlv0_2WireFormatLite::encodeData
      (receivedCertificate, &signedPortionBeginOffset,
      &signedPortionEndOffset, output, &encodingLength))) {
      LOG("Error encoding received certificate.");
      LOG("Error: " << ndn_getErrorString(error) << endl);
      return false;
  }

//   uint8_t pubKeyWithPointIdentifier[65];
//   pubKeyWithPointIdentifier[0] = 0x04;
//   for (int i = 0; i < 64; i++) {
//       pubKeyWithPointIdentifier[i + 1] = AKpub[i];
//   }

  ndn::EcPublicKey controllerPubKey(AKpub);

  if (controllerPubKey.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
        receivedCertificate.getSignature().getSignature().buf(), 
        receivedCertificate.getSignature().getSignature().size())) {
      LOG("Successfully verified received certificate.");
  }
  else {
      LOG("Received certificate failed to verify.");
      //return false;
  }

  ndn_NameComponent temp_name_comps[10];
  ndn::NameLite CKpubKeyLocatorName(temp_name_comps, 10);
  for (int i = 0; i < receivedCertificate.getSignature().getKeyLocator().getKeyName().size(); i++) {
    CKpubKeyLocatorName.append(receivedCertificate.getSignature().getKeyLocator().getKeyName().get(i));
  }
  LOG("Key locator of received certificate: " << ndn::PrintUri(CKpubKeyLocatorName) << endl);

  int keyNameComponentIndex = -1;
  for (keyNameComponentIndex = 0; keyNameComponentIndex < receivedCertificate.getName().size(); 
      keyNameComponentIndex++) {
    if (receivedCertificate.getName().get(keyNameComponentIndex).equals(m_keyNameComponent.get(0))) {
      LOG("Found the KEY name component at index " << keyNameComponentIndex << endl);
      break;
    }
  }

  if (keyNameComponentIndex == -1) {
      LOG("Didn't find key name component in received certificate name.");
      return false;
  }

  if (keyNameComponentIndex - 1 < 0) {
      LOG("The received certificate name was too short to get the device ID (length " 
        << receivedCertificate.getName().size() << ")" << endl);
      return false;
  }

  nameDestination.append(receivedCertificate.getName().get(keyNameComponentIndex - 1));

  return true;
}

bool
CertificateResponseParserBasic::getDevicePrivateKey(uint8_t *key, size_t keyLength, uint8_t *output) {
  ndn_TlvDecoder_initialize(&m_received_certificate_decoder, m_payload, m_payloadLen);

  if (!readTypeAndLength(m_received_certificate_decoder, KD_PRI_ENCRYPTED_TLV_TYPE, 
    m_receivedDkPriEncryptedTlvTypeAndLengthSize, m_receivedDkPriEncryptedTlvValueSize, "KdPri encrypted")) {
    return false;
  }

  m_securityHelper.decrypt(m_payload + m_receivedDkPriEncryptedTlvTypeAndLengthSize,
    m_receivedDkPriEncryptedTlvValueSize, key, keyLength, output);
  
}