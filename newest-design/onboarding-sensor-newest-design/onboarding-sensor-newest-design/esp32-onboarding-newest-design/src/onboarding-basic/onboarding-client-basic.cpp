#include "onboarding-client-basic.hpp"
#include "../logger.hpp"
#include "../credentials.hpp"

#include <lwip/def.h>

#include "mbedtls/sha256.h"

#include "../helpers/print-byte-array-helper.hpp"
#include "../helpers/other-helpers.hpp"

#include "bootstrapping-response-parser-basic.hpp"
#include "certificate-request-response-parser-basic.hpp"

#include <uECC.h>

#define LOG(...) LOGGER(OnboardingClientBasic, __VA_ARGS__)

OnboardingClientBasic::OnboardingClientBasic(ndn::Face& face, const uint8_t* BKpub, const uint8_t* BKpri)
  : m_face(face)
  , m_bootstrapPrefix(m_bootstrapPrefixComps, 2)
  , m_certNameComponent(m_certNameComponentComps, 1)
  , m_dummyKeyName(m_dummyKeyComp, 1)
  , m_bootstrapPrivateKey(m_dummyKeyName)
  , m_deviceID(m_deviceIDNameComp, 1)
  , m_networkPrefix(m_networkPrefixComps, MAX_NAME_COMPS)
{
  m_BKpub = BKpub;
  m_BKpri = BKpri;

  m_bootstrapPrivateKey.import(BKpri);

  ndn_digestSha256(m_BKpub, 91, m_ECC_PUBLIC_DIGEST);

  for (int i = 0; i < 63; i += 2) {
    putInCharBuf(m_BKpubDigestChars, i, m_ECC_PUBLIC_DIGEST[i / 2]);
  }
  m_BKpubDigestChars[64] = '\0';

  ndn::parseNameFromUri(m_bootstrapPrefix, m_bootstrapPrefixString);
  ndn::parseNameFromUri(m_certNameComponent, m_certNameComponentString);
  
  LOG("Checking that bootstrap prefix was parsed correctly: " << ndn::PrintUri(m_bootstrapPrefix) << endl);
  LOG("Checking that certificate request name component was parsed correctly " << ndn::PrintUri(m_certNameComponent) << endl);

  // making sure that m_AKpub is all zeros so we can check to see if it has been initialized
  for (int i = 0; i < 64; i++) {
    m_AKpub[i] = 0;
  }

  m_onboarding_completed = false;
}


OnboardingClientBasic::~OnboardingClientBasic()
{
  m_face.removeHandler(this);
}

bool
OnboardingClientBasic::begin()
{
  m_face.addHandler(this);

  return true;
}

bool
OnboardingClientBasic::initiateSignOn()
{
  sendBootstrappingRequest();
}

bool
OnboardingClientBasic::sendBootstrappingRequest()
{
  m_lastOnboardingStartTime = esp_timer_get_time();

  ndn_NameComponent nameComps[10];
  ndn::InterestLite interest(nameComps, 10, nullptr, 0, nullptr, 0);
  interest.setName(m_bootstrapPrefix);
  //interest.getName().append(m_BKpubDigestChars);
  interest.setMustBeFresh(true);

  size_t tokenSize;
  m_securityHelper.generateDHKeyPair(m_MY_DH_TOKEN, &tokenSize, DH_PRIVATE_EXPONENT_SIZE_BYTES);

  interest.getName().append(m_MY_DH_TOKEN, tokenSize);

  uint8_t sigBuf[500];
  size_t sigLength;
  m_securityHelper.generateApplicationLevelInterestAsymmetricSignatureRSA(interest, sigBuf, &sigLength);

  interest.getName().append(sigBuf, sigLength);

  m_face.sendInterest(interest);

  /*
  m_securityHelper.generateEcKeyPair(m_N1pub, m_N1pri);

  LOG("Bytes of generated N1:" << endl << PrintByteArray(m_N1pub, 0, ECDH_AND_KD_PUBLIC_KEY_SIZE) << endl);
  
  LOG("Bytes of generated N1 private key:" << endl << PrintByteArray(m_N1pri, 0, ECDH_AND_KD_PRIVATE_KEY_SIZE) << endl);

  interest.getName().append(DEVICE_IDENTIFIER, sizeof(DEVICE_IDENTIFIER));
  interest.getName().append(CAPABILITIES, sizeof(CAPABILITIES));
  interest.getName().append(m_N1pub, ECDH_AND_KD_PUBLIC_KEY_SIZE);

  //m_face.setSigningKey(m_bootstrapPrivateKey);

  uint8_t signatureBuffer[300];
  size_t signatureLength;
  m_securityHelper.generateApplicationLevelInterestSignature(interest, m_BKpri, sizeof(m_BKpri),
    signatureBuffer, &signatureLength);

  size_t asnEncodedSignatureLength = m_securityHelper.encodeSignatureBits(signatureBuffer);

  LOG("Length of raw signature: " << PriUint64<DEC>(signatureLength) << endl);

  LOG("Hex of ASN encoded signature: " << endl << PrintByteArray(signatureBuffer, 0, asnEncodedSignatureLength));
  LOG("Length of ASN encoded signature: " << PriUint64<DEC>(asnEncodedSignatureLength) << endl);

  interest.getName().append(signatureBuffer, asnEncodedSignatureLength);

  //m_face.sendSignedInterest(interest);
  m_face.sendInterest(interest);

  m_lastOnboardingSentBootstrapRequestTime = esp_timer_get_time();

  LOG("<I " << ndn::PrintUri(interest.getName()) << endl << endl);

  return true;
  */
}

bool
OnboardingClientBasic::sendCertificateRequest()
{
  LOG("Sending certificate request interest.");

  ndn_NameComponent nameComps[MAX_NAME_COMPS];
  ndn::InterestLite interest(nameComps, MAX_NAME_COMPS, nullptr, 0, nullptr, 0);

  for (int i = 0; i < m_networkPrefix.size(); i++) {
    interest.getName().append(m_networkPrefix.get(i));
  }
  interest.getName().append("cert");
  interest.getName().append(DEVICE_IDENTIFIER, sizeof(DEVICE_IDENTIFIER));

  ndn_digestSha256(m_trustAnchor, m_trustAnchorLength, m_trustAnchorDigest);

  interest.getName().append(m_trustAnchorDigest, ndn_SHA256_DIGEST_SIZE);

  //m_securityHelper.calculateXORofN1pubAndN2pub(m_N1pub, m_N2pub, m_N1pubXORN2pub);

  interest.getName().append(m_N1pub, ECDH_AND_KD_PUBLIC_KEY_SIZE);

  uint8_t N2pubDigest[ndn_SHA256_DIGEST_SIZE];

  ndn_digestSha256(m_N2pub, ECDH_AND_KD_PUBLIC_KEY_SIZE, N2pubDigest);

  LOG("Digest of N2pub: " << endl << PrintByteArray(N2pubDigest, 0, ndn_SHA256_DIGEST_SIZE) << endl);

  interest.getName().append(N2pubDigest, ndn_SHA256_DIGEST_SIZE);

  uint8_t signatureBuffer[300];
  size_t signatureLength;
  m_securityHelper.generateApplicationLevelInterestAsymmetricSignatureECC(interest, m_BKpri, sizeof(m_BKpri),
    signatureBuffer, &signatureLength);

  size_t asnEncodedSignatureLength = m_securityHelper.encodeSignatureBits(signatureBuffer);

  LOG("Length of raw signature: " << PriUint64<DEC>(signatureLength) << endl);

  LOG("Hex of ASN encoded signature: " << endl << PrintByteArray(signatureBuffer, 0, asnEncodedSignatureLength));
  LOG("Length of ASN encoded signature: " << PriUint64<DEC>(asnEncodedSignatureLength) << endl);

  interest.getName().append(signatureBuffer, asnEncodedSignatureLength);

  // interest.setCanBePrefix(true);
  interest.setMustBeFresh(true);

  m_face.setSigningKey(m_bootstrapPrivateKey);
  m_face.sendInterest(interest);
  //m_face.sendSignedInterest(interest);
  LOG("<I " << ndn::PrintUri(interest.getName()) << endl);

  m_lastOnboardingSentCertificateRequestTime = esp_timer_get_time();

  return true;
}

bool
OnboardingClientBasic::processData(const ndn::DataLite& data, uint64_t endpointId)
{
  LOG("<D " << ndn::PrintUri(data.getName()) << endl << endl);

  const ndn::NameLite& name = data.getName();

  bool (OnboardingClientBasic::*f)(const ndn::DataLite&, uint64_t) = nullptr;
  // old values were -7 and -6, from top to bottom
  if (name.get(-4).equals(m_bootstrapPrefix.get(0)) &&
      name.get(-3).equals(m_bootstrapPrefix.get(1))) {
    LOG("Received data in response to bootstrapping request." << endl);
    f = &OnboardingClientBasic::processBootstrappingResponse;
  }
  else if (name.get(-7).equals(m_certNameComponent.get(0))) {
    LOG("Received data in response to certificate request." << endl);
    f = &OnboardingClientBasic::processCertificateRequestResponse;
  }
  else {
    LOG("Did not recognize data response as bootstrapping response or certificate request response." << endl);
    return false;
  }

  return (this->*f)(data, endpointId);
}

bool
OnboardingClientBasic::processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId)
{
  m_lastOnboardingReceivedBootstrapResponseTime = esp_timer_get_time();

  LOG("Processing bootstrapping interest response..." << endl);

  LOG("Size of data content: " << data.getContent().size() << endl);
  LOG("Bytes of data content: " << endl << PrintByteArray(data.getContent().buf(), 0, data.getContent().size()) << endl);

  size_t sharedSecretLength;
  m_securityHelper.readDHPublicAndDeriveSharedSecret(data.getContent().buf(), data.getContent().size(),
    m_DH_SHARED_SECRET, &sharedSecretLength);
  

  /*
  const uint8_t* dataContentBytes = data.getContent().buf();

  BootstrappingResponseParserBasic BootstrappingResponseParserBasic(dataContentBytes, data.getContent().size());

  if (!BootstrappingResponseParserBasic.checkForExpectedTlvs()) {
    LOG("Failed to parse bootstrapping response data content for expected TLV's." << endl);
    return false;
  }

  LOG("Successfully parsed response data content for expected TLV's." << endl);

  m_securityHelper.deriveSharedSecretECDH(dataContentBytes + BootstrappingResponseParserBasic.getN2pubTlvTypeAndLengthSize(), 
    m_N1pri, m_sharedSecret);

  memcpy(m_N2pub, dataContentBytes + BootstrappingResponseParserBasic.getN2pubTlvTypeAndLengthSize(), ECDH_AND_KD_PUBLIC_KEY_SIZE);

  LOG("Bytes of N2 pub: " << endl << PrintByteArray(m_N2pub, 0, ECDH_AND_KD_PUBLIC_KEY_SIZE));

  LOG("Bytes of generated shared secret:" << endl << PrintByteArray(m_sharedSecret, 0, 32) << endl);

  if (!m_securityHelper.verifyDataBySymmetricKey(SECURE_SIGN_ON_CODE, sizeof(SECURE_SIGN_ON_CODE), data)) {
    LOG("Failed to verify response data by Kc, ignoring bootstrapping response...");
    return false;
  }
  else {
    LOG("Successfully verified bootstrapping response by Kc.");
  }

  if (!BootstrappingResponseParserBasic.getNetworkPrefixAndAKpub(m_networkPrefix, m_AKpub)) {
    LOG("Failed to get network prefix and / or AKpub from received trust anchor.");
    return false;
  }
  else {
    LOG("Network prefix received: " << ndn::PrintUri(m_networkPrefix));
  }

  m_trustAnchorLength = BootstrappingResponseParserBasic.getTrustAnchorTlvTypeAndLengthSize() +
    BootstrappingResponseParserBasic.getTrustAnchorTlvValueSize();
  memcpy(m_trustAnchor, dataContentBytes + BootstrappingResponseParserBasic.getN2pubTlvTypeAndLengthSize() +
    BootstrappingResponseParserBasic.getN2pubTlvValueSize(), m_trustAnchorLength);

  //LOG("Bytes of stored trust anchor: " << endl << PrintByteArray(m_trustAnchor, 0, m_trustAnchorLength));

  m_lastBootstrapResponse.reset(m_face.swapPacketBuffer(nullptr));

  m_lastOnboardingFinishedProcessingBootstrappingResponseTime = esp_timer_get_time();

  sendCertificateRequest();

  return true;
  */
}

bool
OnboardingClientBasic::processCertificateRequestResponse(const ndn::DataLite& data, uint64_t endpointId)
{
  m_lastOnboardingReceivedCertificateRequestResponseTime = esp_timer_get_time();
  LOG("Processing certificate request response..." << endl);

  if (!m_securityHelper.verifyDataBySymmetricKey(m_sharedSecret, ndn_SHA256_DIGEST_SIZE, data)) {
    LOG("Failed to verify certificate request response by TSK.");
  } else {
    LOG("Successfully verified certificate request response by TSK.");
  }

  const uint8_t* dataContentBytes = data.getContent().buf();

  CertificateResponseParserBasic CertificateResponseParserBasic(dataContentBytes, data.getContent().size());
  if (!CertificateResponseParserBasic.checkForExpectedTlvs()) {
    LOG("Failed to parse certificate request response data content for expected TLV's." << endl);
    return false;
  }

  if (!CertificateResponseParserBasic.verifyReceivedCertificateAndGetDeviceID(m_AKpub, m_deviceID)) {
    m_deviceID.clear();
    LOG("Failed to verify received certificate or get device ID." << endl);
    return false;
  }
  else {
    LOG("Device ID assigned by controller: " << ndn::PrintUri(m_deviceID));
  }

  CertificateResponseParserBasic.getDevicePrivateKey(m_sharedSecret, 16, m_DkPri);

  LOG("Received DKpri bytes: " << endl << PrintByteArray(m_DkPri, 0, ECDH_AND_KD_PRIVATE_KEY_SIZE) << endl);

  m_onboarding_completed = true;

  m_lastOnboardingFinishTime = esp_timer_get_time();
}