#ifndef ONBOARDING_CLIENT_DYNAMIC_SECRET_HPP
#define ONBOARDING_CLIENT_DYNAMIC_SECRET_HPP

#include <esp8266ndn.h>
#include "../consts.hpp"
#include "../helpers/security-helper.hpp"
#include <PriUint64.h>

/** \brief Onboarding client for secure sign-on protocol.
 */
class OnboardingClientDynamicSecret : public ndn::PacketHandler
{
public:
  explicit
  OnboardingClientDynamicSecret(ndn::Face& face);

  ~OnboardingClientDynamicSecret() noexcept;

  bool
  begin();

  bool
  initiateSignOn();

  bool
  onboardingCompleted() {
    return m_onboarding_completed;
  }

  const ndn::NameLite&
  getDeviceID() {
    return m_deviceID;
  }

  const ndn::NameLite&
  getNetworkPrefix() {
    return m_networkPrefix;
  }

private:

  bool
  sendBootstrappingRequest();

  bool 
  processData(const ndn::DataLite& data, uint64_t endpointId) override;

  bool
  processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId);

  bool
  AKpubRetrieved() {
    for (int i = 0; i < 64; i++) {
      if (m_AKpub[i] != 0)
        return true;
    }
    return false;
  }

public:

  // this time is set as soon as the device calls the sendBootstrappingRequest function
  uint64_t m_lastOnboardingStartTime;
  // this time is set as soon as the device sends the bootstrapping request
  uint64_t m_lastOnboardingSentBootstrapRequestTime;
  // this time is set as soon as the device receives the bootstrapping response
  uint64_t m_lastOnboardingReceivedBootstrapResponseTime;
  // this time is set as soon as the device finishes processing the bootstrapping response
  uint64_t m_lastOnboardingFinishedProcessingBootstrappingResponseTime;

  void
  printTimeInformation() {
    uint64_t T1 = m_lastOnboardingSentBootstrapRequestTime - m_lastOnboardingStartTime;
    uint64_t T2 = m_lastOnboardingReceivedBootstrapResponseTime - m_lastOnboardingSentBootstrapRequestTime;
    uint64_t T3 = m_lastOnboardingFinishedProcessingBootstrappingResponseTime - m_lastOnboardingReceivedBootstrapResponseTime;
    uint64_t T4 = m_lastOnboardingFinishedProcessingBootstrappingResponseTime - m_lastOnboardingStartTime;
    uint64_t T5 = T1 + T3;

    Serial << "Timers from last onboarding:" << endl;
    Serial << "T1: " << PriUint64<DEC>(T1) << endl;
    Serial << "T2: " << PriUint64<DEC>(T2) << endl;
    Serial << "T3: " << PriUint64<DEC>(T3) << endl;
    Serial << "T4: " << PriUint64<DEC>(T4) << endl;
    Serial << "T5: " << PriUint64<DEC>(T5) << endl;

    Serial << "CSV format: " << endl;
    Serial << T1 << "," << T2 << "," << T3 << "," << T4 << "," << T5 << endl;
  }

private:
  ndn::Face& m_face;

  char m_bootstrapPrefixString[13] = "/ndn/sign-on";
  ndn_NameComponent m_bootstrapPrefixComps[2];
  ndn::NameLite m_bootstrapPrefix;

  char m_certNameComponentString[6] = "/cert";
  ndn_NameComponent m_certNameComponentComps[1];
  ndn::NameLite m_certNameComponent;

  ndn_NameComponent m_dummyKeyComp[1];
  ndn::NameLite m_dummyKeyName;

  ndn_NameComponent m_deviceIDNameComp[1];
  ndn::NameLite m_deviceID;

  ndn_NameComponent m_networkPrefixComps[MAX_NAME_COMPS];
  ndn::NameLite m_networkPrefix;

  std::unique_ptr<ndn::PacketBuffer> m_lastBootstrapResponse;

  uint8_t m_DkPub[ECDH_AND_KD_PUBLIC_KEY_SIZE];
  uint8_t m_DkPri[ECDH_AND_KD_PRIVATE_KEY_SIZE];

  uint8_t m_tokenBytes[4];

  SecurityHelper m_securityHelper;

  uint8_t m_AKpub[64];
  uint8_t m_trustAnchor[3000];
  size_t m_trustAnchorLength;

  bool m_onboarding_completed;

};

#endif // ONBOARDING_CLIENT_DYNAMIC_SECRET_HPP
