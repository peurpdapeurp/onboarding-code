#include "onboarding-client-dynamic-secret.hpp"
#include "../logger.hpp"
#include "../credentials.hpp"

#include <lwip/def.h>

#include "mbedtls/sha256.h"

#include "../helpers/print-byte-array-helper.hpp"
#include "../helpers/other-helpers.hpp"

#include "bootstrapping-response-parser-dynamic-secret.hpp"

#include <uECC.h>

#define LOG(...) LOGGER(OnboardingClientDynamicSecret, __VA_ARGS__)

OnboardingClientDynamicSecret::OnboardingClientDynamicSecret(ndn::Face& face)
  : m_face(face)
  , m_bootstrapPrefix(m_bootstrapPrefixComps, 2)
  , m_certNameComponent(m_certNameComponentComps, 1)
  , m_dummyKeyName(m_dummyKeyComp, 1)
  , m_deviceID(m_deviceIDNameComp, 1)
  , m_networkPrefix(m_networkPrefixComps, MAX_NAME_COMPS)
{
  ndn::parseNameFromUri(m_bootstrapPrefix, m_bootstrapPrefixString);
  ndn::parseNameFromUri(m_certNameComponent, m_certNameComponentString);
  
  LOG("Checking that bootstrap prefix was parsed correctly: " << ndn::PrintUri(m_bootstrapPrefix) << endl);
  LOG("Checking that certificate request name component was parsed correctly " << ndn::PrintUri(m_certNameComponent) << endl);

  // making sure that m_AKpub is all zeros so we can check to see if it has been initialized
  for (int i = 0; i < 64; i++) {
   m_AKpub[i] = 0;
  }

  m_onboarding_completed = false;
}


OnboardingClientDynamicSecret::~OnboardingClientDynamicSecret()
{
  m_face.removeHandler(this);
}

bool
OnboardingClientDynamicSecret::begin()
{
  m_face.addHandler(this);
  return true;
}

bool
OnboardingClientDynamicSecret::initiateSignOn()
{
  sendBootstrappingRequest();
}

bool
OnboardingClientDynamicSecret::sendBootstrappingRequest()
{
  m_lastOnboardingStartTime = esp_timer_get_time();

  ndn_NameComponent nameComps[10];
  ndn::InterestLite interest(nameComps, 10, nullptr, 0, nullptr, 0);
  interest.setName(m_bootstrapPrefix);
  interest.setMustBeFresh(true);

  interest.getName().append(DEVICE_IDENTIFIER, sizeof(DEVICE_IDENTIFIER));
  interest.getName().append(CAPABILITIES, sizeof(CAPABILITIES));

  uint32_t token = esp_random();
  uint8_t *tokenBytes = (uint8_t *) &token;

  m_tokenBytes[0] = tokenBytes[0];
  m_tokenBytes[1] = tokenBytes[1];
  m_tokenBytes[2] = tokenBytes[2];
  m_tokenBytes[3] = tokenBytes[3];

  LOG("Value of token: " << token << endl);
  LOG("Bytes of token: " << endl << PrintByteArray(m_tokenBytes, 0, 4) << endl);

  interest.getName().append(m_tokenBytes, 4);

  ndn::HmacKey dynamicSecretKey(SECURE_SIGN_ON_CODE, sizeof(SECURE_SIGN_ON_CODE));

  m_face.setSigningKey(dynamicSecretKey);

  m_face.sendSignedInterest(interest);

  m_lastOnboardingSentBootstrapRequestTime = esp_timer_get_time();

  LOG("<I " << ndn::PrintUri(interest.getName()) << endl << endl);

  return true;
}

bool
OnboardingClientDynamicSecret::processData(const ndn::DataLite& data, uint64_t endpointId)
{
  LOG("<D " << ndn::PrintUri(data.getName()) << endl << endl);

  const ndn::NameLite& name = data.getName();

  bool (OnboardingClientDynamicSecret::*f)(const ndn::DataLite&, uint64_t) = nullptr;
  if (name.get(-7).equals(m_bootstrapPrefix.get(0)) &&
      name.get(-6).equals(m_bootstrapPrefix.get(1))) {
    LOG("Received data in response to bootstrapping request." << endl);
    f = &OnboardingClientDynamicSecret::processBootstrappingResponse;
  }
  else {
    LOG("Did not recognize data response as bootstrapping response or certificate request response." << endl);
    return false;
  }

  return (this->*f)(data, endpointId);
  
}

bool
OnboardingClientDynamicSecret::processBootstrappingResponse(const ndn::DataLite& data, uint64_t endpointId)
{
  
  m_lastOnboardingReceivedBootstrapResponseTime = esp_timer_get_time();

  LOG("Processing bootstrapping interest response..." << endl);

  const uint8_t* dataContentBytes = data.getContent().buf();

  BootstrappingResponseParserDynamicSecret bootstrappingResponseParser(dataContentBytes, data.getContent().size());

  if (!bootstrappingResponseParser.checkForExpectedTlvs()) {
    LOG("Failed to parse bootstrapping response data content for expected TLV's." << endl);
    return false;
  }

  LOG("Successfully parsed response data content for expected TLV's." << endl);

  /*
  m_securityHelper.deriveSharedSecretECDH(dataContentBytes + bootstrappingResponseParser.getN2pubTlvTypeAndLengthSize(), 
    m_N1pri, m_sharedSecret);

  memcpy(m_N2pub, dataContentBytes + bootstrappingResponseParser.getN2pubTlvTypeAndLengthSize(), ECDH_AND_KD_PUBLIC_KEY_SIZE);

  LOG("Bytes of N2 pub: " << endl << PrintByteArray(m_N2pub, 0, ECDH_AND_KD_PUBLIC_KEY_SIZE));

  LOG("Bytes of generated shared secret:" << endl << PrintByteArray(m_sharedSecret, 0, 32) << endl);

  if (!m_securityHelper.verifyDataBySymmetricKey(SECURE_SIGN_ON_CODE, sizeof(SECURE_SIGN_ON_CODE), data)) {
    LOG("Failed to verify response data by Kc, ignoring bootstrapping response...");
    return false;
  }
  else {
    LOG("Successfully verified bootstrapping response by Kc.");
  }

  if (!bootstrappingResponseParser.getNetworkPrefixAndAKpub(m_networkPrefix, m_AKpub)) {
    LOG("Failed to get network prefix and / or AKpub from received trust anchor.");
    return false;
  }
  else {
    LOG("Network prefix received: " << ndn::PrintUri(m_networkPrefix));
  }

  m_trustAnchorLength = bootstrappingResponseParser.getTrustAnchorTlvTypeAndLengthSize() +
    bootstrappingResponseParser.getTrustAnchorTlvValueSize();
  memcpy(m_trustAnchor, dataContentBytes + bootstrappingResponseParser.getN2pubTlvTypeAndLengthSize() +
    bootstrappingResponseParser.getN2pubTlvValueSize(), m_trustAnchorLength);

  //LOG("Bytes of stored trust anchor: " << endl << PrintByteArray(m_trustAnchor, 0, m_trustAnchorLength));

  m_lastBootstrapResponse.reset(m_face.swapPacketBuffer(nullptr));

  m_lastOnboardingFinishedProcessingBootstrappingResponseTime = esp_timer_get_time();

  m_onboarding_completed = true;
  
  return true;
  */
}