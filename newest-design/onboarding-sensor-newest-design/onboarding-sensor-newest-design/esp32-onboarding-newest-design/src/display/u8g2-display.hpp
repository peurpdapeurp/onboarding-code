#ifndef U8G2_DISPLAY_HPP
#define U8G2_DISPLAY_HPP

#include <U8g2lib.h>
#include <esp8266ndn.h>

/** \brief Onboarding client for secure sign-on protocol.
 */
class U8G2Display
{
public:
  explicit
  U8G2Display();

  ~U8G2Display();

  bool
  begin();

  bool
  addLine(const char* message);

  bool
  addLine(const ndn::NameLite &name);

  bool
  display();

private:
  U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2;
  int m_currentLine;
  int m_lineSpacing;
};

#endif // U8G2_DISPLAY_HPP