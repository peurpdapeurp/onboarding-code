package com.example.edwar.ndniotcontroller;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.edwar.ndniotcontroller.utils.TemporaryDHAgreementTesting;
import com.example.edwar.ndniotcontroller.utils.TemporaryRSASignatureTesting;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import net.named_data.jndn.Data;
import net.named_data.jndn.encoding.EncodingException;
import net.named_data.jndn.security.EcKeyParams;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;
import net.named_data.jndn.util.Common;

import org.spongycastle.jce.ECNamedCurveTable;
import org.spongycastle.jce.spec.ECNamedCurveParameterSpec;
import org.spongycastle.util.encoders.Base64;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.ECParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.generateECKeyPair;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.getEcPrivateKeyAsHex;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.getEcPublicKeyAsHex;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.getPemPrivateKey;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.getPemPublicKey;
import static com.example.edwar.ndniotcontroller.utils.SecurityModeConstants.SECURE_SIGN_ON_CODE_SIZE;


public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    // variables to distinguish between onActivityResult callbacks
    // *** there is no request code for QR code scanning because it is hidden in zxing's implementation
    private final int LOGIN_REQUEST_CODE = 35;

    // home prefix that secure sign-on controller will be initialized with
    String g_homePrefix = null;

    // reference to manage the nfd service
    private NFDService g_nfdService;
    // object to do qr code scanning
    private IntentIntegrator g_qrCodeScanner;

    // references to UI objects
    private TextView g_text_homePrefix;
    private Button g_button_scanQRCode;

    // listens for broadcasts from nfd service for nfd related events
    private final BroadcastReceiver g_nfdStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DEBUG(TAG, "NFD status listener got intent with action: " + intent.getAction());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        launchLoginActivity();

        LocalBroadcastManager.getInstance(this).registerReceiver(g_nfdStatusListener, NFDService.getIntentFilter());

        g_qrCodeScanner = new IntentIntegrator(this);

        initializeUI();

        Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);

        TemporaryRSASignatureTesting.test();

        TemporaryDHAgreementTesting.test();

    }

    private void initializeUI() {
        g_text_homePrefix = (TextView) findViewById(R.id.text_homePrefix);

        g_button_scanQRCode = (Button) findViewById(R.id.button_scanQrCode);
        g_button_scanQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                g_qrCodeScanner.initiateScan();
            }
        });

    }

    // get the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        // process the activity result from the login activity
        if (requestCode == LOGIN_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Log.e(TAG, "FAILED TO GET HOME PREFIX FROM LOGIN ACTIVITY");
                return;
            }

            String[] secureSignOnControllerInfo = data.getStringArrayExtra(SecureSignOnController.INITIALIZATION_INFO);
            String homePrefix = secureSignOnControllerInfo[SecureSignOnController.INITIALIZATION_INFO_HOME_PREFIX];

            g_homePrefix = homePrefix;

            g_text_homePrefix.setText("Home Prefix: " + g_homePrefix);

            startNFDService(g_homePrefix);
        }
        // process the activity result from the qr code scanning activity
        else {
            if (result != null) {
                String scanResults = result.getContents();

                if (scanResults != null) {
                    byte[] decodeData = null;
                    try {
                        decodeData = Common.base64Decode(scanResults);
                    }
                    catch (Exception e) {
                        Toast.makeText(this,
                                "Error base 64 decoding QR code contents: " + e, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        return;
                    }

                    byte[] deviceIdentifierBytes = new byte[12];
                    byte[] secureSignOnCodeBytes = new byte[SECURE_SIGN_ON_CODE_SIZE];
                    byte[] BKpubCertificateBytes = new byte[decodeData.length - 12 - SECURE_SIGN_ON_CODE_SIZE];

                    System.arraycopy(decodeData, 0, deviceIdentifierBytes, 0, 12);
                    System.arraycopy(decodeData, 12, secureSignOnCodeBytes, 0, SECURE_SIGN_ON_CODE_SIZE);
                    System.arraycopy(decodeData, 12 + SECURE_SIGN_ON_CODE_SIZE, BKpubCertificateBytes, 0,decodeData.length - 12 - SECURE_SIGN_ON_CODE_SIZE);

                    Data BKpubCertificateDataPacket = new Data();
                    try {
                        BKpubCertificateDataPacket.wireDecode(new Blob(BKpubCertificateBytes));
                    } catch (EncodingException e) {
                        Toast.makeText(this,
                                "Failed to interpret last QR code scan as data packet: " + e, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        return;
                    }

                    CertificateV2 BKpubCertificate = null;

                    try {
                        BKpubCertificate = new CertificateV2(BKpubCertificateDataPacket);
                    } catch (CertificateV2.Error error) {
                        Toast.makeText(this,
                                "Failed to interpret last QR code scan as certificate v2: " + error, Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        return;
                    }

                    try {
                        g_nfdService.m_secureSignOnController.addDevice(deviceIdentifierBytes, secureSignOnCodeBytes, BKpubCertificate);
                    } catch (CertificateV2.Error error) {
                        Toast.makeText(this,
                                "Failed to add device to secure sign-on controller: " + error, Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        return;
                    }
                }
            }
        }
    }

    private void launchLoginActivity() {
        startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST_CODE);
    }

    // starts the nfd service
    private void startNFDService(String homePrefix) {
        Intent nfdIntent = new Intent(MainActivity.this, NFDService.class);

        String[] secureSignOnControllerInitializationInfo = new String[1];
        secureSignOnControllerInitializationInfo[0] = homePrefix;

        nfdIntent.putExtra(SecureSignOnController.INITIALIZATION_INFO, secureSignOnControllerInitializationInfo);

        boolean testNfdServiceStart = bindService(nfdIntent, g_nfdServiceConnection, BIND_AUTO_CREATE);
        if (!testNfdServiceStart) {
            Toast toast = Toast.makeText(MainActivity.this, "NFD Service failed to bind.", Toast.LENGTH_SHORT);
            toast.show();
            startNFDService(homePrefix);
        }
    }

    // moniters connection between nfd service and main activity
    private final ServiceConnection g_nfdServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            g_nfdService = ((NFDService.LocalBinder) service).getService();
            if (!g_nfdService.initialize())
                Toast.makeText(MainActivity.this, "Failed to initialize secure sign-on controller.", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            g_nfdService = null;
            startNFDService(g_homePrefix);
        }
    };

    @Override
    protected void onDestroy() {

        if (g_nfdServiceConnection != null)
            unbindService(g_nfdServiceConnection);

        super.onDestroy();
    }
}
