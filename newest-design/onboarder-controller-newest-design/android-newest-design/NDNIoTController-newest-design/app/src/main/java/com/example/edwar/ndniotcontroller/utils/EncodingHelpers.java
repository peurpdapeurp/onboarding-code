package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Name;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.security.PublicKey;

import static com.example.edwar.ndniotcontroller.signOnTLVs.SignOnTLVs.*;
import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;

public class EncodingHelpers {

    private final static String TAG = "EncodingHelpers";

    public static Data constructBootstrappingResponseECDH(Name name, CertificateV2 anchorCertificate, byte[] N2pubRawBytes) {

        // push these things into a data packet so that they end up in this order:
        // 1) EKpub TLV (token2 bytes) (TLV type 130)
        // 3) wire encoded trust anchor

        // sign data packet by TSK and send it out

        byte[] anchorCertificateTLV = anchorCertificate.wireEncode().getImmutableArray();

        byte[] N2pubRawTLV = new byte[N2pubRawBytes.length + 2];
        N2pubRawTLV[0] = (byte) N2_PUB_TLV_TYPE;
        N2pubRawTLV[1] = (byte) N2pubRawBytes.length;
        System.arraycopy(N2pubRawBytes, 0, N2pubRawTLV, 2, N2pubRawBytes.length);

        byte[] finalContentByteArray = new byte[N2pubRawTLV.length + anchorCertificateTLV.length];
        System.arraycopy(N2pubRawTLV, 0, finalContentByteArray, 0, N2pubRawTLV.length);
        System.arraycopy(anchorCertificateTLV, 0, finalContentByteArray,
                N2pubRawTLV.length, anchorCertificateTLV.length);

        Blob content = new Blob(finalContentByteArray);

        Name dataName = new Name(name);

        dataName.appendVersion(System.currentTimeMillis());
        Data data = new Data(dataName);
        data.setContent(content);

        return data;

    }

    public static Data constructBootstrappingResponseDynamicSecret(Name name, CertificateV2 anchorCertificate, CertificateV2 KdPubCertificate,
                                                                  byte[] encryptionKey, byte[] KdPriRawBytes, byte[] tokenBytes) {

        // push these things into a data packet so that they end up in this order:
        // 1) EKpub TLV (token2 bytes) (TLV type 130)
        // 3) wire encoded trust anchor

        // sign data packet by TSK and send it out

        byte[] anchorCertificateTLV = anchorCertificate.wireEncode().getImmutableArray();
        byte[] KdPubCertificateTLV = KdPubCertificate.wireEncode().getImmutableArray();

        byte[] encryptedKdPriRawBytes = null;
        try {
            encryptedKdPriRawBytes = encrypt(KdPriRawBytes, encryptionKey);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Blob encryptedKdPriBlob = new Blob(encryptedKdPriRawBytes);
        DEBUG(TAG, "Hex string of encrypted DKpri: " + encryptedKdPriBlob.toHex());

        byte[] KdPriEncryptedTLV = new byte[encryptedKdPriRawBytes.length + 2];
        KdPriEncryptedTLV[0] = (byte) KD_PRI_ENCRYPTED_TLV_TYPE;
        KdPriEncryptedTLV[1] = (byte) encryptedKdPriRawBytes.length;
        System.arraycopy(encryptedKdPriRawBytes, 0, KdPriEncryptedTLV, 2, encryptedKdPriRawBytes.length);

        byte[] tokenBytesTLV = new byte[tokenBytes.length + 2];
        tokenBytesTLV[0] = (byte) TOKEN_BYTES_TLV_TYPE;
        tokenBytesTLV[1] = (byte) tokenBytes.length;
        System.arraycopy(tokenBytes, 0, tokenBytesTLV, 2, tokenBytes.length);

        byte[] finalContentByteArray = new byte[anchorCertificateTLV.length + KdPubCertificateTLV.length +
                KdPriEncryptedTLV.length + tokenBytesTLV.length];
        System.arraycopy(anchorCertificateTLV, 0, finalContentByteArray, 0, anchorCertificateTLV.length);
        System.arraycopy(KdPubCertificateTLV, 0, finalContentByteArray,
                anchorCertificateTLV.length, KdPubCertificateTLV.length);
        System.arraycopy(KdPriEncryptedTLV, 0, finalContentByteArray,
                anchorCertificateTLV.length + KdPubCertificateTLV.length, KdPriEncryptedTLV.length);
        System.arraycopy(tokenBytesTLV, 0, finalContentByteArray,
                anchorCertificateTLV.length + KdPubCertificateTLV.length + KdPriEncryptedTLV.length, tokenBytes.length);

        Blob content = new Blob(finalContentByteArray);

        Name dataName = new Name(name);

        dataName.appendVersion(System.currentTimeMillis());
        Data data = new Data(dataName);
        data.setContent(content);

        return data;

    }

    public static Data constructCertificateRequestResponse(Name name, CertificateV2 DkPubCertificate,
                                                           byte[] DKpriRawBytes, byte[] encryptionKey) {

        // push these things into a data packet so that they end up in this order:
        // 1) DkPubCertificate by AKpri
        // 3) Kt encrypted DkPri (TLV type 129)

        // sign data packet by TSK and send it out

        byte[] DkPubCertificateTLV = DkPubCertificate.wireEncode().getImmutableArray();

        byte[] encryptedDKpriRawBytes = null;
        try {
            encryptedDKpriRawBytes = encrypt(DKpriRawBytes, encryptionKey);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Blob encryptedDKpriBlob = new Blob(encryptedDKpriRawBytes);
        DEBUG(TAG, "Hex string of encrypted DKpri: " + encryptedDKpriBlob.toHex());

        byte[] DKpriEncryptedTLV = new byte[encryptedDKpriRawBytes.length + 2];
        DKpriEncryptedTLV[0] = (byte) KD_PRI_ENCRYPTED_TLV_TYPE;
        DKpriEncryptedTLV[1] = (byte) encryptedDKpriRawBytes.length;
        System.arraycopy(encryptedDKpriRawBytes, 0, DKpriEncryptedTLV, 2, encryptedDKpriRawBytes.length);

        byte[] finalContentByteArray = new byte[DKpriEncryptedTLV.length + DkPubCertificateTLV.length];
        System.arraycopy(DKpriEncryptedTLV, 0, finalContentByteArray, 0, DKpriEncryptedTLV.length);
        System.arraycopy(DkPubCertificateTLV, 0, finalContentByteArray,
                DKpriEncryptedTLV.length, DkPubCertificateTLV.length);

        Blob content = new Blob(finalContentByteArray);

        Name dataName = new Name(name);

        dataName.appendVersion(System.currentTimeMillis());
        Data data = new Data(dataName);
        data.setContent(content);

        return data;

    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

}
