package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.util.Blob;

import org.spongycastle.jcajce.provider.asymmetric.dh.BCDHPrivateKey;
import org.spongycastle.jcajce.provider.asymmetric.dh.BCDHPublicKey;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.KeyAgreement;
import javax.crypto.spec.DHParameterSpec;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityModeConstants.DHParametersPEM;
import static com.example.edwar.ndniotcontroller.utils.SecurityModeConstants.DH_PRIVATE_KEY_BITS_SIZE;

public class TemporaryDHAgreementTesting {

    private static final String TAG = "TempDHAgreeTesting";

    public static void test() {
        /*
        DHParameterSpec spec = null;
        try {
            spec = loadPemDHParameters(DHParametersPEM, DH_PRIVATE_KEY_BITS_SIZE);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        KeyPairGenerator keyGen = null;
        try {
            keyGen = KeyPairGenerator.getInstance("DH");
            try {
                keyGen.initialize(spec, new SecureRandom());
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        KeyAgreement aKeyAgree = null;
        try {
             aKeyAgree = KeyAgreement.getInstance("DH");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        KeyPair aPair = keyGen.generateKeyPair();

        BCDHPublicKey aPub = (BCDHPublicKey) aPair.getPublic();
        byte[] publicExponentBytes = aPub.getY().toByteArray();
        Blob publicExponentBytesBlob = new Blob(publicExponentBytes);
        Blob publicASNEncoding = new Blob(aPub.getEncoded());
        DEBUG(TAG, "ASN encoded public key bytes: " + publicASNEncoding.toHex());
        DEBUG(TAG, "Y bytes: " + publicExponentBytesBlob.toHex());

        byte[] asnEncodedBPub = new byte[0];
        try {
            asnEncodedBPub = asnEncodeRawDHPublicKeyBytes(hardcodedPublic);
        } catch (Exception e) {
            e.printStackTrace();
        }
        BCDHPublicKey bPub = null;
        try {
            bPub = (BCDHPublicKey) convertASNEncodedECPublicKeyToPublicKeyObject(asnEncodedBPub, "DH");
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        byte[] sharedSecret = null;
        try {
            aKeyAgree.init(aPair.getPrivate());
            aKeyAgree.doPhase(bPub, true);
            sharedSecret = aKeyAgree.generateSecret();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        Blob sharedSecretBlob = new Blob(sharedSecret);
        DEBUG(TAG, "Hex string of shared secret: " + sharedSecretBlob.toHex());
        */

    }


    private static byte[] hardcodedPublic = new byte[] {
            (byte) 0x2C, (byte) 0xFB, (byte) 0x89, (byte) 0xF5, (byte) 0x86, (byte) 0xA1, (byte) 0xDE, (byte) 0x6B, (byte) 0x4E, (byte) 0x60,
            (byte) 0x72, (byte) 0x2E, (byte) 0xA4, (byte) 0xA4, (byte) 0xD7, (byte) 0x2D, (byte) 0xAB, (byte) 0x16, (byte) 0x96, (byte) 0x3F,
            (byte) 0xF2, (byte) 0x96, (byte) 0x41, (byte) 0xE0, (byte) 0xAF, (byte) 0x27, (byte) 0x1B, (byte) 0xD7, (byte) 0x42, (byte) 0x40,
            (byte) 0xEC, (byte) 0x51, (byte) 0xFF, (byte) 0x46, (byte) 0x74, (byte) 0x75, (byte) 0xDD, (byte) 0x30, (byte) 0x64, (byte) 0x15,
            (byte) 0xCB, (byte) 0x87, (byte) 0x03, (byte) 0x87, (byte) 0x98, (byte) 0xE6, (byte) 0xE8, (byte) 0x51, (byte) 0x72, (byte) 0x7D,
            (byte) 0xF1, (byte) 0xF7, (byte) 0xD1, (byte) 0x69, (byte) 0xBC, (byte) 0x19, (byte) 0x8C, (byte) 0x90, (byte) 0x82, (byte) 0xD1,
            (byte) 0x6A, (byte) 0x91, (byte) 0x7B, (byte) 0x9F, (byte) 0x6E, (byte) 0x44, (byte) 0x03, (byte) 0x41, (byte) 0x5F, (byte) 0x07,
            (byte) 0x95, (byte) 0xD8, (byte) 0xCA, (byte) 0xE9, (byte) 0x44, (byte) 0xFB, (byte) 0x82, (byte) 0xD6, (byte) 0xDA, (byte) 0x87,
            (byte) 0x2E, (byte) 0x30, (byte) 0x9C, (byte) 0xE7, (byte) 0x20, (byte) 0xE0, (byte) 0xF8, (byte) 0x18, (byte) 0xA3, (byte) 0x38,
            (byte) 0xFD, (byte) 0xF9, (byte) 0xF9, (byte) 0x00, (byte) 0x49, (byte) 0xC8, (byte) 0x80, (byte) 0xE9, (byte) 0x52, (byte) 0x86,
            (byte) 0x2B, (byte) 0x10, (byte) 0x48, (byte) 0xE1, (byte) 0x80, (byte) 0xB8, (byte) 0xB5, (byte) 0xAD, (byte) 0xDB, (byte) 0xB3,
            (byte) 0x3B, (byte) 0x47, (byte) 0x37, (byte) 0x47, (byte) 0xF7, (byte) 0x9D, (byte) 0x04, (byte) 0xC6, (byte) 0x5B, (byte) 0x08,
            (byte) 0x37, (byte) 0x5C, (byte) 0x74, (byte) 0xD1, (byte) 0x6E, (byte) 0xB7, (byte) 0x98, (byte) 0x91, (byte) 0x83, (byte) 0x94,
            (byte) 0x04, (byte) 0x2D, (byte) 0x4C, (byte) 0xB2, (byte) 0x73, (byte) 0xCE, (byte) 0x9C, (byte) 0xF0, (byte) 0x76, (byte) 0x1A,
            (byte) 0x53, (byte) 0x7A, (byte) 0xD2, (byte) 0x58, (byte) 0x45, (byte) 0x3D, (byte) 0xD8, (byte) 0x07, (byte) 0xA3, (byte) 0xC5,
            (byte) 0x06, (byte) 0x36, (byte) 0xC7, (byte) 0x7A, (byte) 0x0D, (byte) 0x9B, (byte) 0xD0, (byte) 0x28, (byte) 0x39, (byte) 0x08,
            (byte) 0x4D, (byte) 0x07, (byte) 0x9F, (byte) 0xA9, (byte) 0xD7, (byte) 0x40, (byte) 0xC1, (byte) 0xF6, (byte) 0xAC, (byte) 0x3F,
            (byte) 0xEC, (byte) 0xC5, (byte) 0x96, (byte) 0x44, (byte) 0x1D, (byte) 0xE5, (byte) 0xCA, (byte) 0x4D, (byte) 0xDA, (byte) 0x3E,
            (byte) 0x1E, (byte) 0xB9, (byte) 0x2E, (byte) 0x90, (byte) 0x97, (byte) 0xEE, (byte) 0x24, (byte) 0xA2, (byte) 0xEA, (byte) 0x1C,
            (byte) 0x08, (byte) 0x6E, (byte) 0x5F, (byte) 0x68, (byte) 0x06, (byte) 0x71, (byte) 0x54, (byte) 0x7A, (byte) 0x89, (byte) 0xE6,
            (byte) 0xCF, (byte) 0x3D, (byte) 0x9C, (byte) 0x2D, (byte) 0x10, (byte) 0x87, (byte) 0x6B, (byte) 0x72, (byte) 0x3F, (byte) 0x65,
            (byte) 0xA3, (byte) 0xD0, (byte) 0x46, (byte) 0x5E, (byte) 0x77, (byte) 0x64, (byte) 0xA3, (byte) 0x0A, (byte) 0x8A, (byte) 0xB1,
            (byte) 0x47, (byte) 0xA8, (byte) 0x5B, (byte) 0x74, (byte) 0x7E, (byte) 0x3D, (byte) 0x02, (byte) 0x52, (byte) 0xF3, (byte) 0xB5,
            (byte) 0x95, (byte) 0x63, (byte) 0x37, (byte) 0x12, (byte) 0x8C, (byte) 0x61, (byte) 0x04, (byte) 0xE3, (byte) 0xDB, (byte) 0x5D,
            (byte) 0x01, (byte) 0xFD, (byte) 0xC6, (byte) 0xC3, (byte) 0xEB, (byte) 0xDC, (byte) 0x8B, (byte) 0x17, (byte) 0xE4, (byte) 0x4C,
            (byte) 0x9F, (byte) 0x32, (byte) 0x2D, (byte) 0xDE, (byte) 0x6A, (byte) 0xE6,
    };

}
