package com.example.edwar.ndniotcontroller;

import net.named_data.jndn.security.v2.CertificateV2;

import java.security.KeyPair;
import java.security.PublicKey;

public class DeviceInfo {
    public CertificateV2 BKpubCertificate;
    public PublicKey BKpub;
    public byte[] Kt;
    public byte[] secureSignOnCode;
    public KeyPair N2KeyPair;
    public byte[] N2pubRawBytes;
    public byte[] N1pubRawBytes;
    public KeyPair KdKeyPair;
    public boolean onboarded = false;


}
