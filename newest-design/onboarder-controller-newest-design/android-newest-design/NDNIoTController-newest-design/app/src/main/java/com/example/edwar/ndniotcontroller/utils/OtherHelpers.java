package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.util.Blob;

import java.io.IOException;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;

public class OtherHelpers {

    private static final String TAG = "OtherHelpers";

    public static void signDataBySymmetricKeyAndPublish(final byte[] key, final Face face, final Data data) {

        KeyChain.signWithHmacWithSha256(data, new Blob(key));

        class OneShotTask implements Runnable {
            Data data;

            OneShotTask(Data d) {
                data = d;
            }

            public void run() {
                try {
                    DEBUG(TAG, "Sending data with name: " + data.getName());
                    face.putData(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Thread t = new Thread(new OneShotTask(data));
        t.start();

    }

}
