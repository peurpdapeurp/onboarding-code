package com.example.edwar.ndniotcontroller;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;

import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.Name;
import net.named_data.jndn.NetworkNack;
import net.named_data.jndn.OnData;
import net.named_data.jndn.OnNetworkNack;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.OnTimeout;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.pib.Pib;
import net.named_data.jndn.security.pib.PibIdentity;
import net.named_data.jndn.security.pib.PibImpl;
import net.named_data.jndn.security.tpm.Tpm;
import net.named_data.jndn.security.tpm.TpmBackEnd;
import net.named_data.jndn.security.v2.CertificateV2;

import java.io.IOException;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;


public class NFDService extends Service {

    private static String TAG = "NFDService";

    private final String NETWORK_THREAD_STOPPED = "NETWORK_THREAD_STOPPED";

    private final IBinder mBinder = new NFDService.LocalBinder();

    private Face m_face;
    private KeyChain m_keyChain;
    private PibIdentity m_myIdentity;
    private boolean m_networkThreadShouldStop = false;
    private Thread m_networkThread;
    private Thread m_heartbeatInterestThread;

    private CertificateV2 m_anchorCertificate;
    private Name m_homePrefix;
    public SecureSignOnController m_secureSignOnController;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        m_homePrefix = new Name(intent.getExtras().getStringArray(
                SecureSignOnController.INITIALIZATION_INFO)[SecureSignOnController.INITIALIZATION_INFO_HOME_PREFIX]);

        return mBinder;
    }

    public class LocalBinder extends Binder {
        NFDService getService() {
            return NFDService.this;
        }
    }

    private void initializeKeyChain() {
        try {
            m_keyChain = new KeyChain("pib-memory:", "tpm-memory:");
        } catch (KeyChain.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        m_keyChain.setFace(m_face);
        m_myIdentity = null;
        try {
            m_myIdentity = m_keyChain.createIdentityV2(new Name(m_homePrefix));
            m_keyChain.setDefaultIdentity(m_myIdentity);
        } catch (Pib.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        } catch (Tpm.Error error) {
            error.printStackTrace();
        } catch (TpmBackEnd.Error error) {
            error.printStackTrace();
        } catch (KeyChain.Error error) {
            error.printStackTrace();
        }

        try {
            m_anchorCertificate = m_myIdentity.getDefaultKey().getDefaultCertificate();
        } catch (Pib.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        }


    }

    private void setCommandSigningInfo() {
        Name defaultCertificateName;
        try {
            defaultCertificateName = m_keyChain.getDefaultCertificateName();
        } catch (SecurityException e) {
            Name testIdName = new Name("/iot-controller");
            try {
                defaultCertificateName = m_keyChain.createIdentityAndCertificate(testIdName);
                m_keyChain.getIdentityManager().setDefaultIdentity(testIdName);
            } catch (SecurityException e2) {
                defaultCertificateName = new Name("/bogus/certificate/name");
            }
        }
        m_face.setCommandSigningInfo(m_keyChain, defaultCertificateName);
    }

    BroadcastReceiver networkThreadStoppedListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            createAndStartNewNetworkThread();
        }
    };

    public boolean initialize() {

        m_face = new Face("localhost");

        LocalBroadcastManager.getInstance(this).registerReceiver(networkThreadStoppedListener, new IntentFilter(NETWORK_THREAD_STOPPED));

        try {
            m_secureSignOnController = new SecureSignOnController();
        } catch (Exception e) {
            Log.e(TAG, "Failed to start secure sign-on controller: " + e.getMessage());
            e.printStackTrace();
            return false;
        }

        initializeKeyChain();

        createAndStartNewNetworkThread();

        return true;

    }

    private void createAndStartNewNetworkThread() {
        m_networkThread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    if (m_face == null)
                        m_face = new Face(getString(R.string.nfd_address));

                    setCommandSigningInfo();

                } catch (Exception e) {

                    e.printStackTrace();

                }

                createAndStartNewHeartbeatInterestThread();

                try {
                    m_secureSignOnController.initialize(m_face, m_keyChain, m_homePrefix,
                            onRegisterSuccess, onRegisterFailed,
                            m_anchorCertificate);
                } catch (IOException e) {
                    Log.w(TAG, "Error initializing secure sign on controller: " + e.getMessage());
                    e.printStackTrace();
                    stopNetworkThread();
                } catch (SecurityException e) {
                    Log.w(TAG, "Error initializing secure sign on controller: " + e.getMessage());
                    e.printStackTrace();
                    stopNetworkThread();
                } catch (Exception e) {
                    Log.w(TAG, "Error initializing secure sign on controller: " + e.getMessage());
                    e.printStackTrace();
                    stopNetworkThread();
                }

                while (!m_networkThreadShouldStop) {
                    try {
                        m_face.processEvents();
                        Thread.sleep(100); // avoid hammering the CPU
                    } catch (IOException e) {
                        Log.w(TAG, "Error in processEvents loop: " + e.getMessage());
                        e.printStackTrace();
                        stopNetworkThread();
                    } catch (Exception e) {
                        Log.w(TAG, "Error in processEvents loop: " + e.getMessage());
                        e.printStackTrace();
                        stopNetworkThread();
                    }
                }
                doFinalCleanup();
                DEBUG(TAG, "Network thread stopped.");
                LocalBroadcastManager.getInstance(NFDService.this).sendBroadcast(new Intent(NETWORK_THREAD_STOPPED));
            }
        });
        m_networkThreadShouldStop = false;
        m_networkThread.start();
    }

    private void createAndStartNewHeartbeatInterestThread() {
        // thread to periodically send localhop interests to NFD-Android to get notified of failures quickly
        m_heartbeatInterestThread = new Thread(new Runnable() {

            public void run() {
                while (!m_networkThreadShouldStop) {
                    try {
                        m_face.expressInterest(new Name("localhost/heartbeat"), onData, onTimeout, onNack);
                    } catch (IOException e) {
                        stopNetworkThread();
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        });

        m_heartbeatInterestThread.start();
    }

    private OnData onData = new OnData() {
        @Override
        public void onData(Interest interest, Data data) {

        }
    };

    private OnTimeout onTimeout = new OnTimeout() {
        @Override
        public void onTimeout(Interest interest) {

        }
    };

    private OnNetworkNack onNack = new OnNetworkNack() {
        @Override
        public void onNetworkNack(Interest interest, NetworkNack networkNack) {

        }
    };

    private final OnRegisterSuccess onRegisterSuccess = new OnRegisterSuccess() {
        @Override
        public void onRegisterSuccess(Name prefix, long registeredPrefixId) {
            DEBUG(TAG, "Successfully register prefix: " + prefix);
        }
    };

    private final OnRegisterFailed onRegisterFailed = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            DEBUG(TAG, "Failed to register prefix: " + prefix);
        }
    };

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();


        return filter;
    }

    private void stopNetworkThread() {
        m_networkThreadShouldStop = true;

    }

    private void doFinalCleanup() {
        if (m_face != null) m_face.shutdown();
        m_face = null;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(networkThreadStoppedListener);

        super.onDestroy();
        m_face.shutdown();
        m_networkThread.interrupt();
    }

}