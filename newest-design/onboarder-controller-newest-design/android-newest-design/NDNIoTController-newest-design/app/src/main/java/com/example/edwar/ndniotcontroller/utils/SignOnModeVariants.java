package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import com.example.edwar.ndniotcontroller.DeviceInfo;
import com.example.edwar.ndniotcontroller.SecureSignOnController;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.InterestFilter;
import net.named_data.jndn.Name;
import net.named_data.jndn.OnInterestCallback;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.VerificationHelpers;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import org.spongycastle.jcajce.provider.asymmetric.dh.BCDHPublicKey;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.KeyAgreement;
import javax.crypto.spec.DHParameterSpec;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;
import static com.example.edwar.ndniotcontroller.utils.EncodingHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.OtherHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityModeConstants.*;

public class SignOnModeVariants {

    private static String TAG;
    private Face face_;
    private Name homePrefix_;
    private CertificateV2 anchorCertificate_;
    private KeyChain keyChain_;
    private HashMap<String, DeviceInfo> devices_;
    private MessageDigest sha256_;
    private long deviceCounter_;

    public SignOnModeVariants(Face face, Name homePrefix, CertificateV2 anchorCertificate, KeyChain keyChain,
                              HashMap<String, DeviceInfo> devices, MessageDigest sha256, long deviceCounter, String TAG) {
        face_ = face;
        homePrefix_ = homePrefix;
        anchorCertificate_ = anchorCertificate;
        keyChain_ = keyChain;
        devices_ = devices;
        sha256_ = sha256;
        deviceCounter_ = deviceCounter;
        this.TAG = TAG;
    }


//    // interest callbacks and timer variables for sign on basic
//
//    public OnInterestCallback onBootstrappingInterest_ = new OnInterestCallback() {
//        @Override
//        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
//
//            lastOnboardingReceivedBootstrappingRequestTime = System.nanoTime();;
//
//            DEBUG(TAG, "Got a bootstrapping interest with name: " + interest.getName().toUri());
//
//            Name bootstrappingInterestName = interest.getName();
//
//            int deviceIDOffset = -4;
//            int deviceCapabilitiesOffset = -3;
//            int N1pubRawOffset = -2;
//
//            byte[] deviceIdentifier = bootstrappingInterestName.get(deviceIDOffset).getValue().getImmutableArray();
//            Blob deviceIdentifierBlob = new Blob(deviceIdentifier);
//            String deviceIdentifierString = deviceIdentifierBlob.toHex();
//            DEBUG(TAG, "Hex string of device identifier received from bootstrapping interest: " + deviceIdentifierString);
//
//            if (!devices_.containsKey(deviceIdentifierString)) {
//                DEBUG(TAG, "Received a bootstrapping interest from a device not scanned yet. Device identifier hex string: "
//                        + deviceIdentifierString);
//                return;
//            }
//
//            DeviceInfo currentDeviceInfo = devices_.get(deviceIdentifierString);
//
//            try {
//                if (!SecurityHelpers.verify(currentDeviceInfo.BKpub, interest)) {
//                    DEBUG(TAG, "Failed to verify bootstrapping interest from device. Device identifier hex string: "
//                            + deviceIdentifierString);
//                    //return;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                //return;
//            }
//
//            lastOnboardingValidatedBootstrappingRequestTime = System.nanoTime();
//
//            KeyPair N2KeyPair = null;
//            try {
//                N2KeyPair = generateECKeyPair(ECDH_AND_KD_CURVE);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            } catch (InvalidAlgorithmParameterException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            currentDeviceInfo.N2KeyPair = N2KeyPair;
//
//            byte[] N2pubASNEncoded = N2KeyPair.getPublic().getEncoded();
//            Blob N2pubASNEncodedBlob = new Blob(N2pubASNEncoded);
//            DEBUG(TAG, "Hex string of N2pub ASN encoded: " + N2pubASNEncodedBlob.toHex());
//
//            byte[] N1pubRaw = bootstrappingInterestName.get(N1pubRawOffset).getValue().getImmutableArray();
//            currentDeviceInfo.N1pubRawBytes = Arrays.copyOf(N1pubRaw, N1pubRaw.length);
//            Blob N1pubRawBlob = new Blob(N1pubRaw);
//            DEBUG(TAG, "Hex of received N1pubRaw: " + N1pubRawBlob.toHex());
//
//            byte[] N2pubRawBytes = hexStringToByteArray(getEcPublicKeyAsHex(N2KeyPair.getPublic(), SecurityHelpers.EC_ECDH));
//            Blob N2pubRawCheckerBlob = new Blob(N2pubRawBytes);
//            DEBUG(TAG, "Hex string of N2pubRaw: " + N2pubRawCheckerBlob.toHex());
//
//            currentDeviceInfo.N2pubRawBytes = Arrays.copyOf(N2pubRawBytes, N2pubRawBytes.length);
//
//            Data bootstrappingResponse = constructBootstrappingResponseECDH(interest.getName(), anchorCertificate_,
//                    N2pubRawBytes);
//
//            signDataBySymmetricKeyAndPublish(currentDeviceInfo.secureSignOnCode, face_, bootstrappingResponse);
//
//            lastOnboardingSentBootstrappingResponseTime = System.nanoTime();
//        }
//    };
//
//    public OnInterestCallback onCertificateRequestInterest_ = new OnInterestCallback() {
//        @Override
//        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
//
//
//            lastOnboardingReceivedCertificateRequestTime = System.nanoTime();
//
//            int deviceIdentifierOffset = -5;
//            int trustAnchorDigestFromDeviceOffset = -4;
//            int N1pubRawBytesOffset = -3;
//            int N2pubHashOffset = -2;
//
//            DEBUG(TAG, "Got a certificate request interest with name: " + interest.getName().toUri());
//
//            Name certificateRequestInterestName = interest.getName();
//
//            byte[] deviceIdentifier = certificateRequestInterestName.get(deviceIdentifierOffset).getValue().getImmutableArray();
//            Blob deviceIdentifierBlob = new Blob(deviceIdentifier);
//            String deviceIdentifierString = deviceIdentifierBlob.toHex();
//            DEBUG(TAG, "Hex string of device identifier received from certificate request interest: " + deviceIdentifierString);
//
//            if (!devices_.containsKey(deviceIdentifierString)) {
//                DEBUG(TAG, "Received a certificate request interest from a device not scanned yet. Device identifier hex string: "
//                        + deviceIdentifierString);
//                return;
//            }
//
//            DeviceInfo currentDeviceInfo = devices_.get(deviceIdentifierString);
//
//            try {
//                if (!SecurityHelpers.verify(currentDeviceInfo.BKpub, interest)) {
//                    DEBUG(TAG, "Failed to verify certificate request interest from device. Device identifier hex string: "
//                            + deviceIdentifierString);
//                    //return;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                //return;
//            }
//
//            sha256_.update(anchorCertificate_.wireEncode().getImmutableArray());
//            byte[] trustAnchorDigest = sha256_.digest();
//            byte[] trustAnchorDigestFromDevice = certificateRequestInterestName.get(trustAnchorDigestFromDeviceOffset).getValue().getImmutableArray();
//
//            if (!checkBuffersMatch(trustAnchorDigest, trustAnchorDigestFromDevice)) {
//                DEBUG(TAG, "Got a wrong digest of trust anchor from device. Device identifier hex string: " +
//                        deviceIdentifierString);
//                return;
//            }
//
//            //byte[] N1pubXORN2pub = calculateN1pubXORN2pub(currentDeviceInfo.N1pubRawBytes, currentDeviceInfo.N2pubRawBytes);
//            //byte[] N1pubXORN2pubFromDevice = certificateRequestInterestName.get(-3).getValue().getImmutableArray();
//
//            byte[] N1pubRawBytesFromDevice = certificateRequestInterestName.get(N1pubRawBytesOffset).getValue().getImmutableArray();
//            byte[] N2sha256DigestFromDevice = certificateRequestInterestName.get(N2pubHashOffset).getValue().getImmutableArray();
//
//            if (!checkBuffersMatch(currentDeviceInfo.N1pubRawBytes, N1pubRawBytesFromDevice)) {
//                DEBUG(TAG, "Got a wrong N1pub from device. Device identifier hex string: " +
//                        deviceIdentifierString);
//                return;
//            }
//
//            sha256_.reset();
//            sha256_.update(currentDeviceInfo.N2pubRawBytes);
//            byte[] N2sha256Digest = sha256_.digest();
//            Blob N2sha256DigestBlob = new Blob(N2sha256Digest);
//            DEBUG(TAG, "N2pub sha256 we calculated: " + N2sha256DigestBlob.toHex());
//            Blob N2sha256DigestFromDeviceBlob = new Blob(N2sha256DigestFromDevice);
//            DEBUG(TAG, "N2pub sha256 digest from device: " + N2sha256DigestFromDeviceBlob.toHex());
//
//            if (!checkBuffersMatch(N2sha256Digest, N2sha256DigestFromDevice)) {
//                DEBUG(TAG, "Got a wrong N2pub sha256 digest from device. Device identifier hex srting: " +
//                        deviceIdentifierString);
//                return;
//            }
//
//            lastOnboardingValidatedCertificateRequestTime = System.nanoTime();
//
//            KeyPair N2KeyPair = currentDeviceInfo.N2KeyPair;
//
//            byte[] N1pubASNEncoded = new byte[0];
//            try {
//                N1pubASNEncoded = asnEncodeRawECPublicKeyBytes(currentDeviceInfo.N1pubRawBytes);
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }
//
//            PublicKey N1pub = null;
//            try {
//                N1pub = convertASNEncodedECPublicKeyToPublicKeyObject(N1pubASNEncoded, "EC");
//            } catch (InvalidKeySpecException e) {
//                e.printStackTrace();
//                return;
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            byte[] sharedSecret = null;
//            try {
//                sharedSecret = deriveSharedSecretECDH(N2KeyPair.getPrivate(), N1pub);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            } catch (InvalidKeyException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            Blob sharedSecretBlob = new Blob(sharedSecret);
//            DEBUG(TAG, "Hex of shared secret: " + sharedSecretBlob.toHex());
//
//            currentDeviceInfo.Kt = Arrays.copyOf(sharedSecret, sharedSecret.length);
//
//            KeyPair KdKeyPair = null;
//            try {
//                KdKeyPair = generateECKeyPair(ECDH_AND_KD_CURVE);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            } catch (InvalidAlgorithmParameterException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            byte[] KdPubRawBytes = hexStringToByteArray(getEcPublicKeyAsHex(KdKeyPair.getPublic(), SecurityHelpers.EC_KS));
//            byte[] KdPriRawBytes = hexStringToByteArray(getEcPrivateKeyAsHex(KdKeyPair.getPrivate()));
//
//            Blob KdPriRawBlob = new Blob(KdPriRawBytes);
//            DEBUG(TAG, "Hex string of KdPriRawBytes: " + KdPriRawBlob.toHex());
//
//            long currentTime = System.currentTimeMillis();
//
//            CertificateV2 KdPubCertificate = new CertificateV2();
//            KdPubCertificate.setName(homePrefix_);
//            KdPubCertificate.getName().append("DEVICE" + deviceCounter_);
//            KdPubCertificate.getName().append("KEY");
//            KdPubCertificate.getName().append(Name.Component.fromTimestamp(currentTime));
//            try {
//                KdPubCertificate.setContent(new Blob(asnEncodeRawECPublicKeyBytes(KdPubRawBytes)));
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }
//            // set validity period of certificate to ten days
//            KdPubCertificate.getValidityPeriod().setPeriod(currentTime, currentTime + 1000 * 60 * 60 * 24 * 10);
//
//            try {
//                keyChain_.sign(KdPubCertificate, keyChain_.getDefaultCertificateName());
//            } catch (SecurityException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            if (!VerificationHelpers.verifyDataSignature(KdPubCertificate, anchorCertificate_)) {
//                DEBUG(TAG, "Failed to verify the CKpub certificate we generated using the anchor certificate.");
//                return;
//            }
//
//            // only use the first 16 bytes of symmetric key
//            byte[] encryptionKey = new byte[16];
//            System.arraycopy(currentDeviceInfo.Kt, 0, encryptionKey, 0, 16);
//
//            Data certificateRequestResponse = constructCertificateRequestResponse(interest.getName(), KdPubCertificate,
//                    KdPriRawBytes, encryptionKey);
//
//            signDataBySymmetricKeyAndPublish(currentDeviceInfo.Kt, face_, certificateRequestResponse);
//
//            lastOnboardingSentCertificateRequestResponseTime = System.nanoTime();
//
//            Log.d(TAG, "Times for the last sign-on procedure:");
//            long nanoSecondToMicroSecondDivider = 1000;
//            long T1 = (lastOnboardingValidatedBootstrappingRequestTime - lastOnboardingReceivedBootstrappingRequestTime) / nanoSecondToMicroSecondDivider;
//            long T2 = (lastOnboardingSentBootstrappingResponseTime - lastOnboardingValidatedBootstrappingRequestTime) / nanoSecondToMicroSecondDivider;
//            long T3 = (lastOnboardingReceivedCertificateRequestTime - lastOnboardingSentBootstrappingResponseTime) / nanoSecondToMicroSecondDivider;
//            long T4 = (lastOnboardingValidatedCertificateRequestTime - lastOnboardingReceivedCertificateRequestTime) / nanoSecondToMicroSecondDivider;
//            long T5 = (lastOnboardingSentCertificateRequestResponseTime - lastOnboardingValidatedCertificateRequestTime) / nanoSecondToMicroSecondDivider;
//            long T6 = (lastOnboardingSentCertificateRequestResponseTime - lastOnboardingValidatedBootstrappingRequestTime) / nanoSecondToMicroSecondDivider;
//            long T7 = (T1 + T2 + T4 + T5);
//            Log.d(TAG, "T1: " + T1);
//            Log.d(TAG, "T2: " + T2);
//            Log.d(TAG, "T3: " + T3);
//            Log.d(TAG, "T4: " + T4);
//            Log.d(TAG, "T5: " + T5);
//            Log.d(TAG, "T6: " + T6);
//            Log.d(TAG, "T7: " + T7);
//            Log.d(TAG, "CSV format: ");
//            Log.d(TAG, "," + T1 + "," + T2 + "," + T3 + "," + T4 + "," + T5 + "," + T6 + "," + T7 + ",");
//
//        }
//    };
//
//    // temporary variables for timing and evaluation
//    public static long lastOnboardingReceivedBootstrappingRequestTime;
//    public static long lastOnboardingValidatedBootstrappingRequestTime;
//    public static long lastOnboardingSentBootstrappingResponseTime;
//    public static long lastOnboardingReceivedCertificateRequestTime;
//    public static long lastOnboardingValidatedCertificateRequestTime;
//    public static long lastOnboardingSentCertificateRequestResponseTime;

    // interest callbacks and timer variables for sign on dynamic secret

//    public OnInterestCallback onBootstrappingInterest_ = new OnInterestCallback() {
//        @Override
//        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
//
//            lastOnboardingReceivedBootstrappingRequestTime = System.nanoTime();
//            ;
//
//            DEBUG(TAG, "Got a bootstrapping interest with name: " + interest.getName().toUri());
//
//            Name bootstrappingInterestName = interest.getName();
//
//            int deviceIDOffset = -5;
//            int deviceCapabilitiesOffset = -4;
//            int tokenBytesOffset = -3;
//
//            byte[] deviceIdentifier = bootstrappingInterestName.get(deviceIDOffset).getValue().getImmutableArray();
//            Blob deviceIdentifierBlob = new Blob(deviceIdentifier);
//            String deviceIdentifierString = deviceIdentifierBlob.toHex();
//            DEBUG(TAG, "Hex string of device identifier received from bootstrapping interest: " + deviceIdentifierString);
//
//            if (!devices_.containsKey(deviceIdentifierString)) {
//                DEBUG(TAG, "Received a bootstrapping interest from a device not scanned yet. Device identifier hex string: "
//                        + deviceIdentifierString);
//                return;
//            }
//
//            DeviceInfo currentDeviceInfo = devices_.get(deviceIdentifierString);
//
//            // HACK WITH A HARDCODED SYMMETRIC KEY, FOR NOW THE DEVICE IS VERIFIED USING ITS SECURE SIGN ON CODE;
//            // WE PRETEND LIKE THIS CODE IS A DYNAMICALLY GENERATED SECRET
//            if (!KeyChain.verifyInterestWithHmacWithSha256(interest, new Blob(currentDeviceInfo.secureSignOnCode))) {
//                DEBUG(TAG, "Failed to verify bootstrapping interest from device. Device identifier hex string: "
//                        + deviceIdentifierString);
//                return;
//            }
//
//            lastOnboardingValidatedBootstrappingRequestTime = System.nanoTime();
//
//            KeyPair KdKeyPair = null;
//            try {
//                KdKeyPair = generateECKeyPair(ECDH_AND_KD_CURVE);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            } catch (InvalidAlgorithmParameterException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            currentDeviceInfo.KdKeyPair = KdKeyPair;
//
//            byte[] KdPubASNEncoded = KdKeyPair.getPublic().getEncoded();
//            Blob N2pubASNEncodedBlob = new Blob(KdPubASNEncoded);
//            DEBUG(TAG, "Hex string of N2pub ASN encoded: " + N2pubASNEncodedBlob.toHex());
//
//            byte[] tokenBytes = bootstrappingInterestName.get(tokenBytesOffset).getValue().getImmutableArray();
//            Blob tokenBytesBlob = new Blob(tokenBytes);
//            DEBUG(TAG, "Hex of received token bytes: " + tokenBytesBlob.toHex());
//
//            byte[] KdPriRawBytes = hexStringToByteArray(getEcPublicKeyAsHex(KdKeyPair.getPublic(), SecurityHelpers.EC_ECDH));
//            Blob KdPriRawCheckerBlob = new Blob(KdPriRawBytes);
//            DEBUG(TAG, "Hex string of Kd Pri raw bytes: " + KdPriRawCheckerBlob.toHex());
//
//            long currentTime = System.currentTimeMillis();
//
//            CertificateV2 KdPubCertificate = new CertificateV2();
//            KdPubCertificate.setName(homePrefix_);
//            KdPubCertificate.getName().append("DEVICE" + deviceCounter_);
//            KdPubCertificate.getName().append("KEY");
//            KdPubCertificate.getName().append(Name.Component.fromTimestamp(currentTime));
//            try {
//                KdPubCertificate.setContent(new Blob(KdPubASNEncoded));
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }
//            // set validity period of certificate to ten days
//            KdPubCertificate.getValidityPeriod().setPeriod(currentTime, currentTime + 1000 * 60 * 60 * 24 * 10);
//
//            try {
//                keyChain_.sign(KdPubCertificate, keyChain_.getDefaultCertificateName());
//            } catch (SecurityException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            if (!VerificationHelpers.verifyDataSignature(KdPubCertificate, anchorCertificate_)) {
//                DEBUG(TAG, "Failed to verify the CKpub certificate we generated using the anchor certificate.");
//                return;
//            }
//
//            Data bootstrappingResponse = constructBootstrappingResponseDynamicSecret(interest.getName(), anchorCertificate_,
//                    KdPubCertificate, currentDeviceInfo.secureSignOnCode, KdPriRawBytes, tokenBytes);
//
//            signDataBySymmetricKeyAndPublish(currentDeviceInfo.secureSignOnCode, face_, bootstrappingResponse);
//
//            lastOnboardingSentBootstrappingResponseTime = System.nanoTime();
//
//            Log.d(TAG, "Times for the last sign-on procedure:");
//            long nanoSecondToMicroSecondDivider = 1000;
//            long T1 = (lastOnboardingValidatedBootstrappingRequestTime - lastOnboardingReceivedBootstrappingRequestTime) / nanoSecondToMicroSecondDivider;
//            long T2 = (lastOnboardingSentBootstrappingResponseTime - lastOnboardingValidatedBootstrappingRequestTime) / nanoSecondToMicroSecondDivider;
//            long T3 = (T1 + T2);
//            Log.d(TAG, "T1: " + T1);
//            Log.d(TAG, "T2: " + T2);
//            Log.d(TAG, "T3: " + T1 + T2);
//            Log.d(TAG, "CSV format: ");
//            Log.d(TAG, "," + T1 + "," + T2 + "," + T3 + ",");
//        }
//    };
//
//    public OnInterestCallback onCertificateRequestInterest_ = new OnInterestCallback() {
//        @Override
//        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
//
//                /*
//
//            lastOnboardingReceivedCertificateRequestTime = System.nanoTime();
//
//            int deviceIdentifierOffset = -5;
//            int trustAnchorDigestFromDeviceOffset = -4;
//            int N1pubRawBytesOffset = -3;
//            int N2pubHashOffset = -2;
//
//            DEBUG(TAG, "Got a certificate request interest with name: " + interest.getName().toUri());
//
//            Name certificateRequestInterestName = interest.getName();
//
//            byte[] deviceIdentifier = certificateRequestInterestName.get(deviceIdentifierOffset).getValue().getImmutableArray();
//            Blob deviceIdentifierBlob = new Blob(deviceIdentifier);
//            String deviceIdentifierString = deviceIdentifierBlob.toHex();
//            DEBUG(TAG, "Hex string of device identifier received from certificate request interest: " + deviceIdentifierString);
//
//            if (!devices_.containsKey(deviceIdentifierString)) {
//                DEBUG(TAG, "Received a certificate request interest from a device not scanned yet. Device identifier hex string: "
//                        + deviceIdentifierString);
//                return;
//            }
//
//            DeviceInfo currentDeviceInfo = devices_.get(deviceIdentifierString);
//
//            try {
//                if (!SecurityHelpers.verify(currentDeviceInfo.BKpub, interest)) {
//                    DEBUG(TAG, "Failed to verify certificate request interest from device. Device identifier hex string: "
//                            + deviceIdentifierString);
//                    //return;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                //return;
//            }
//
//            sha256_.update(anchorCertificate_.wireEncode().getImmutableArray());
//            byte[] trustAnchorDigest = sha256_.digest();
//            byte[] trustAnchorDigestFromDevice = certificateRequestInterestName.get(trustAnchorDigestFromDeviceOffset).getValue().getImmutableArray();
//
//            if (!checkBuffersMatch(trustAnchorDigest, trustAnchorDigestFromDevice)) {
//                DEBUG(TAG, "Got a wrong digest of trust anchor from device. Device identifier hex string: " +
//                        deviceIdentifierString);
//                return;
//            }
//
//            //byte[] N1pubXORN2pub = calculateN1pubXORN2pub(currentDeviceInfo.N1pubRawBytes, currentDeviceInfo.N2pubRawBytes);
//            //byte[] N1pubXORN2pubFromDevice = certificateRequestInterestName.get(-3).getValue().getImmutableArray();
//
//            byte[] N1pubRawBytesFromDevice = certificateRequestInterestName.get(N1pubRawBytesOffset).getValue().getImmutableArray();
//            byte[] N2sha256DigestFromDevice = certificateRequestInterestName.get(N2pubHashOffset).getValue().getImmutableArray();
//
//            if (!checkBuffersMatch(currentDeviceInfo.N1pubRawBytes, N1pubRawBytesFromDevice)) {
//                DEBUG(TAG, "Got a wrong N1pub from device. Device identifier hex string: " +
//                        deviceIdentifierString);
//                return;
//            }
//
//            sha256_.reset();
//            sha256_.update(currentDeviceInfo.N2pubRawBytes);
//            byte[] N2sha256Digest = sha256_.digest();
//            Blob N2sha256DigestBlob = new Blob(N2sha256Digest);
//            DEBUG(TAG, "N2pub sha256 we calculated: " + N2sha256DigestBlob.toHex());
//            Blob N2sha256DigestFromDeviceBlob = new Blob(N2sha256DigestFromDevice);
//            DEBUG(TAG, "N2pub sha256 digest from device: " + N2sha256DigestFromDeviceBlob.toHex());
//
//            if (!checkBuffersMatch(N2sha256Digest, N2sha256DigestFromDevice)) {
//                DEBUG(TAG, "Got a wrong N2pub sha256 digest from device. Device identifier hex srting: " +
//                        deviceIdentifierString);
//                return;
//            }
//
//            lastOnboardingValidatedCertificateRequestTime = System.nanoTime();
//
//            KeyPair N2KeyPair = currentDeviceInfo.N2KeyPair;
//
//            byte[] N1pubASNEncoded = new byte[0];
//            try {
//                N1pubASNEncoded = asnEncodeRawECPublicKeyBytes(currentDeviceInfo.N1pubRawBytes);
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }
//
//            PublicKey N1pub = null;
//            try {
//                N1pub = convertASNEncodedECPublicKeyToPublicKeyObject(N1pubASNEncoded);
//            } catch (InvalidKeySpecException e) {
//                e.printStackTrace();
//                return;
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            byte[] sharedSecret = null;
//            try {
//                sharedSecret = deriveSharedSecretECDH(N2KeyPair.getPrivate(), N1pub);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            } catch (InvalidKeyException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            Blob sharedSecretBlob = new Blob(sharedSecret);
//            DEBUG(TAG, "Hex of shared secret: " + sharedSecretBlob.toHex());
//
//            currentDeviceInfo.Kt = Arrays.copyOf(sharedSecret, sharedSecret.length);
//
//            KeyPair KdKeyPair = null;
//            try {
//                KdKeyPair = generateECKeyPair(ECDH_AND_KD_CURVE);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//                return;
//            } catch (InvalidAlgorithmParameterException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            byte[] KdPubRawBytes = hexStringToByteArray(getEcPublicKeyAsHex(KdKeyPair.getPublic(), SecurityHelpers.EC_KS));
//            byte[] KdPriRawBytes = hexStringToByteArray(getEcPrivateKeyAsHex(KdKeyPair.getPrivate()));
//
//            Blob KdPriRawBlob = new Blob(KdPriRawBytes);
//            DEBUG(TAG, "Hex string of KdPriRawBytes: " + KdPriRawBlob.toHex());
//
//            long currentTime = System.currentTimeMillis();
//
//            CertificateV2 KdPubCertificate = new CertificateV2();
//            KdPubCertificate.setName(homePrefix_);
//            KdPubCertificate.getName().append("DEVICE" + deviceCounter_);
//            KdPubCertificate.getName().append("KEY");
//            KdPubCertificate.getName().append(Name.Component.fromTimestamp(currentTime));
//            try {
//                KdPubCertificate.setContent(new Blob(asnEncodeRawECPublicKeyBytes(KdPubRawBytes)));
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }
//            // set validity period of certificate to ten days
//            KdPubCertificate.getValidityPeriod().setPeriod(currentTime, currentTime + 1000 * 60 * 60 * 24 * 10);
//
//            try {
//                keyChain_.sign(KdPubCertificate, keyChain_.getDefaultCertificateName());
//            } catch (SecurityException e) {
//                e.printStackTrace();
//                return;
//            }
//
//            if (!VerificationHelpers.verifyDataSignature(KdPubCertificate, anchorCertificate_)) {
//                DEBUG(TAG, "Failed to verify the CKpub certificate we generated using the anchor certificate.");
//                return;
//            }
//
//            // only use the first 16 bytes of symmetric key
//            byte[] encryptionKey = new byte[16];
//            System.arraycopy(currentDeviceInfo.Kt, 0, encryptionKey, 0, 16);
//
//            Data certificateRequestResponse = constructCertificateRequestResponse(interest.getName(), KdPubCertificate,
//                    KdPriRawBytes, encryptionKey);
//
//            signDataBySymmetricKeyAndPublish(currentDeviceInfo.Kt, face_, certificateRequestResponse);
//
//            lastOnboardingSentCertificateRequestResponseTime = System.nanoTime();
//
//            */
//
//        }
//    };
//
//    // temporary variables for timing and evaluation
//    public static long lastOnboardingReceivedBootstrappingRequestTime;
//    public static long lastOnboardingValidatedBootstrappingRequestTime;
//    public static long lastOnboardingSentBootstrappingResponseTime;

    public OnInterestCallback onBootstrappingInterest_ = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {

            DEBUG(TAG, "Got a bootstrapping interest with name: " + interest.getName().toUri());

            PublicKey deviceBootstrappingPublicKey = null;
            try {
                deviceBootstrappingPublicKey = getPemPublicKey(RSA_PUBLIC_PEM_STRING, "RSA");
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            try {
                if (!verifyApplicationLevelInterestAsymmetricSignatureRSA(interest.getName().getPrefix(
                            interest.getName().size() - 1).wireEncode().getImmutableArray(), deviceBootstrappingPublicKey,
                            interest.getName().get(-1).getValue().getImmutableArray())) {
                    DEBUG(TAG, "Failed to verify interest with rsa signature");
                    return;
                }
                else {
                    DEBUG(TAG, "SUCCESSFULLY VERIFIED RSA SIGNATURE");
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            } catch (SignatureException e) {
                e.printStackTrace();
                return;
            } catch (InvalidKeyException e) {
                e.printStackTrace();
                return;
            }

            byte[] otherPublicKeyBytes = interest.getName().get(-2).getValue().getImmutableArray();

            DHParameterSpec spec = null;
            try {
                spec = loadPemDHParameters(DHParametersPEM, DH_PRIVATE_KEY_BITS_SIZE);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            KeyPairGenerator keyGen = null;
            try {
                keyGen = KeyPairGenerator.getInstance("DH");
                try {
                    keyGen.initialize(spec, new SecureRandom());
                } catch (InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            KeyAgreement aKeyAgree = null;
            try {
                aKeyAgree = KeyAgreement.getInstance("DH");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            KeyPair aPair = keyGen.generateKeyPair();

            BCDHPublicKey aPub = (BCDHPublicKey) aPair.getPublic();
            byte[] publicYBytes = aPub.getY().toByteArray();
            Blob publicExponentBytesBlob = new Blob(publicYBytes);
            Blob publicASNEncoding = new Blob(aPub.getEncoded());
            DEBUG(TAG, "ASN encoded public key bytes: " + publicASNEncoding.toHex());
            DEBUG(TAG, "Y bytes: " + publicExponentBytesBlob.toHex());

            byte[] asnEncodedDeviceToken = new byte[0];
            try {
                asnEncodedDeviceToken = asnEncodeRawDHPublicKeyBytes(otherPublicKeyBytes);
            } catch (Exception e) {
                e.printStackTrace();
            }
            BCDHPublicKey deviceDHToken = null;
            try {
                Blob asnEncodedDeviceTokenBlob = new Blob(asnEncodedDeviceToken);
                Log.d(TAG, "Hex string of asn encoded device token: " + asnEncodedDeviceTokenBlob.toHex());
                Blob rawDeviceTokenBlob = new Blob(otherPublicKeyBytes);
                Log.d(TAG, "Hex string of raw device token: " + rawDeviceTokenBlob.toHex());
                deviceDHToken = (BCDHPublicKey) convertASNEncodedECPublicKeyToPublicKeyObject(asnEncodedDeviceToken, "DH");
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            byte[] sharedSecret = null;
            try {
                aKeyAgree.init(aPair.getPrivate());
                aKeyAgree.doPhase(deviceDHToken, true);
                sharedSecret = aKeyAgree.generateSecret();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

            Blob sharedSecretBlob = new Blob(sharedSecret);
            DEBUG(TAG, "Hex string of shared secret: " + sharedSecretBlob.toHex());

            Data data = new Data();
            byte[] publicYBytesTrimmed = null;
            // just a check to get rid of the 0 padding on the ASN encoding of the raw public key bytes,
            // since mbedtls only works without the 0 padding; the logic here is that if the number of key bytes
            // is odd, then that means there has to be an extra 0 padding in the raw key bytes
            if (publicYBytes.length % 2 != 0) {
                publicYBytesTrimmed = new byte[publicYBytes.length - 1];
                System.arraycopy(publicYBytes, 1, publicYBytesTrimmed, 0, publicYBytes.length - 1);
            }
            else {
                publicYBytesTrimmed = new byte[publicYBytes.length];
                System.arraycopy(publicYBytes, 0, publicYBytesTrimmed, 0, publicYBytes.length);
            }
            data.setContent(new Blob(publicYBytesTrimmed));
            data.setName(interest.getName());

            signDataBySymmetricKeyAndPublish(new byte[] {1}, face_, data);

        }
    };

    public OnInterestCallback onCertificateRequestInterest_ = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {

            DEBUG(TAG, "Got a certificate request interest with name: " + interest.getName().toUri());



        }
    };


}
