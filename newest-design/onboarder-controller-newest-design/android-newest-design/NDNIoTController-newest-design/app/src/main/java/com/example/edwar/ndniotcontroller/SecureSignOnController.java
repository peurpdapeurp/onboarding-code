package com.example.edwar.ndniotcontroller;

import android.util.Log;

import com.example.edwar.ndniotcontroller.utils.SecurityHelpers;
import com.example.edwar.ndniotcontroller.utils.SignOnModeVariants;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.InterestFilter;
import net.named_data.jndn.Name;
import net.named_data.jndn.OnInterestCallback;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.VerificationHelpers;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;
import static com.example.edwar.ndniotcontroller.utils.EncodingHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.OtherHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityModeConstants.ECDH_AND_KD_CURVE;

public class SecureSignOnController {

    private final String TAG = "SecureSignOnController";

    public static String INITIALIZATION_INFO = "INITIALIZATION_INFO";
    public static int INITIALIZATION_INFO_HOME_PREFIX = 0;

    public SecureSignOnController() throws Exception {
        deviceCounter_ = 0;
        devices_ = new HashMap<>();

        try {
            sha256_ = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException exception) {
            throw new Error ("MessageDigest: SHA-256 is not supported: " + exception.getMessage());
        }

    }

    public void initialize(Face face, KeyChain keyChain, Name homePrefix, OnRegisterSuccess onRegisterSuccess, OnRegisterFailed onRegisterFailed,
                           CertificateV2 anchorCertificate) throws IOException, SecurityException, Exception {

        if (face == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null face.");
        if (homePrefix == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null home prefix.");
        if (onRegisterFailed == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null prefix registration failure callback.");
        if (onRegisterSuccess == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null prefix registration success callback.");
        if (anchorCertificate == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null anchor certificate.");

        face_ = face;
        homePrefix_ = new Name(homePrefix);
        keyChain_ = keyChain;
        anchorCertificate_ = anchorCertificate;

        signOnModeVariants_ = new
                SignOnModeVariants(face_, homePrefix_, anchorCertificate_, keyChain_, devices_, sha256_, deviceCounter_, TAG);

        face_.registerPrefix(new Name("/ndn/sign-on"), signOnModeVariants_.onBootstrappingInterest_, onRegisterFailed, onRegisterSuccess);
        face_.registerPrefix(homePrefix_.append("cert"), signOnModeVariants_.onCertificateRequestInterest_, onRegisterFailed, onRegisterSuccess);



    }

    public void addDevice(byte[] deviceIdentifierBytes, byte[] secureSignOnCodeBytes, CertificateV2 BKpubCertificate) throws CertificateV2.Error {

        Blob deviceIdentifierBlob = new Blob(deviceIdentifierBytes);
        String deviceIdentifierString = deviceIdentifierBlob.toHex();
        DEBUG(TAG, "Device identifier hex string being added to device list: " + deviceIdentifierString);

        Blob secureSignOnCodeBlob = new Blob(secureSignOnCodeBytes);
        String secureSignOnCodeString = secureSignOnCodeBlob.toHex();
        DEBUG(TAG, "Hex string of secure sign on code for this device: " + secureSignOnCodeString);

        byte[] BKpubASNEncoded = null;
        try {
            BKpubASNEncoded = BKpubCertificate.getPublicKey().getImmutableArray();
        } catch (CertificateV2.Error error) {
            error.printStackTrace();
            return;
        }
        Blob BKpubASNEncodedBlob = new Blob(BKpubASNEncoded);
        DEBUG(TAG, "Hex string of BKpub ASN encoded: " + BKpubASNEncodedBlob.toHex());

        PublicKey BKpub = null;
        try {
            BKpub = convertASNEncodedECPublicKeyToPublicKeyObject(BKpubASNEncoded, "EC");
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return;
        }

        if (!devices_.containsKey(deviceIdentifierString)) {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.BKpubCertificate = BKpubCertificate;
            deviceInfo.secureSignOnCode = Arrays.copyOf(secureSignOnCodeBytes, secureSignOnCodeBytes.length);
            deviceInfo.BKpub = BKpub;
            devices_.put(deviceIdentifierString, deviceInfo);
        }

    }

    private Face face_;
    private Name homePrefix_;
    private CertificateV2 anchorCertificate_;
    private KeyChain keyChain_;
    private HashMap<String, DeviceInfo> devices_;
    private MessageDigest sha256_;
    private long deviceCounter_;

    private SignOnModeVariants signOnModeVariants_;


}