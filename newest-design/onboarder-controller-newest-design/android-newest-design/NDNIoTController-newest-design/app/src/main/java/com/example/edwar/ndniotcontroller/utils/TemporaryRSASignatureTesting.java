package com.example.edwar.ndniotcontroller.utils;

import net.named_data.jndn.util.Blob;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

import static com.example.edwar.ndniotcontroller.utils.Debugger.DEBUG;
import static com.example.edwar.ndniotcontroller.utils.RSA_KEY_CONSTANTS.privateKeyRSA3072PEM;
import static com.example.edwar.ndniotcontroller.utils.RSA_KEY_CONSTANTS.publicKeyRSA3072PEM;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityModeConstants.*;

public class TemporaryRSASignatureTesting {

    private static final String TAG = "TempRSASigTesting";

    public static void test() {

        PrivateKey testPrivateKey = null;
        try {
            testPrivateKey = getPemPrivateKey(privateKeyRSA3072PEM, "RSA");
        } catch (Exception e) {
            e.printStackTrace();
        }

        PublicKey testPublicKey = null;
        try {
            testPublicKey = getPemPublicKey(publicKeyRSA3072PEM, "RSA");
        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] clearText = {0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10};

        Signature sig = null;
        byte[] signatureBytes = null;
        try {
            sig = Signature.getInstance("SHA256WithRSA");
            sig.initSign(testPrivateKey);
            sig.update(clearText);
            signatureBytes = sig.sign();
            sig.initVerify(testPublicKey);
            sig.update(clearText);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return;
        } catch (SignatureException e) {
            e.printStackTrace();
            return;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            return;
        }

        Blob signatureBytesBlob = new Blob(signatureBytes);
        DEBUG(TAG, "Signature generated over cleartext: " + signatureBytesBlob.toHex());

        try {
            System.out.println(sig.verify(signatureBytes));
        } catch (SignatureException e) {
            e.printStackTrace();
            return;
        }
    }
}
