package com.example.edwar.ndniotcontroller.utils;

import static com.example.edwar.ndniotcontroller.utils.RSA_KEY_CONSTANTS.publicKeyRSA1024PEM;
import static com.example.edwar.ndniotcontroller.utils.RSA_KEY_CONSTANTS.publicKeyRSA3072PEM;

public class SecurityModeConstants {

    // constants for ecc-256 security
    public static final int Ks_PUBLIC_KEY_LENGTH = 64;
    public static final int Ks_PRIVATE_KEY_LENGTH = 32;
    public static final int ECDH_PUBLIC_KEY_LENGTH = 64;
    public static final int ECDH_PRIVATE_KEY_LENGTH = 32;
    public static final String ECDH_AND_KD_CURVE = "secp256r1";
    public static final int SECURE_SIGN_ON_CODE_SIZE = 16;

//    // constants for ecc-hybrid security
//    public static final int Ks_PUBLIC_KEY_LENGTH = 64;
//    public static final int Ks_PRIVATE_KEY_LENGTH = 32;
//    public static final int ECDH_PUBLIC_KEY_LENGTH = 40;
//    public static final int ECDH_PRIVATE_KEY_LENGTH = 20;
//    public static final String ECDH_AND_KD_CURVE = "secp160r1";
//    public static final int SECURE_SIGN_ON_CODE_SIZE = 16;

//    // constants for ecc-160 security
//    public static final int Ks_PUBLIC_KEY_LENGTH = 40;
//    public static final int Ks_PRIVATE_KEY_LENGTH = 20;
//    public static final int ECDH_PUBLIC_KEY_LENGTH = 40;
//    public static final int ECDH_PRIVATE_KEY_LENGTH = 20;
//    public static final String ECDH_AND_KD_CURVE = "secp160r1";
//    public static final int SECURE_SIGN_ON_CODE_SIZE = 10;

    // constants for rsa-1024 security
    // this is a 1024 RSA diffie hellman parameters pem file (source: https://wiki.openssl.org/index.php/Diffie-Hellman_parameters)
    public static String DHParametersPEM =
            "-----BEGIN DH PARAMETERS-----\n" +
            "MIGHAoGBAP//////////yQ/aoiFowjTExmKLgNwc0SkCTgiKZ8x0Agu+pjsTmyJR\n" +
            "Sgh5jjQE3e+VGbPNOkMbMCsKbfJfFDdP4TVtbVHCReSFtXZiXn7G9ExC6aY37WsL\n" +
            "/1y29Aa37e44a/taiZ+lrp8kEXxLH+ZJKGZR7OZTgf//////////AgEC\n" +
            "-----END DH PARAMETERS-----";
    public static final int DH_PRIVATE_KEY_BITS_SIZE = 1024;
    public static final String RSA_PUBLIC_PEM_STRING = publicKeyRSA1024PEM;

//    // constants for rsa-3072 security
//    // this is a 3072 rsa diffie hellman parameters pem file (source: https://wiki.openssl.org/index.php/Diffie-Hellman_parameters)
//    public static String DHParametersPEM  =
//            "-----BEGIN DH PARAMETERS-----\n" +
//    "MIIBiAKCAYEA///////////JD9qiIWjCNMTGYouA3BzRKQJOCIpnzHQCC76mOxOb\n" +
//            "IlFKCHmONATd75UZs806QxswKwpt8l8UN0/hNW1tUcJF5IW1dmJefsb0TELppjft\n" +
//            "awv/XLb0Brft7jhr+1qJn6WunyQRfEsf5kkoZlHs5Fs9wgB8uKFjvwWY2kg2HFXT\n" +
//            "mmkWP6j9JM9fg2VdI9yjrZYcYvNWIIVSu57VKQdwlpZtZww1Tkq8mATxdGwIyhgh\n" +
//            "fDKQXkYuNs474553LBgOhgObJ4Oi7Aeij7XFXfBvTFLJ3ivL9pVYFxg5lUl86pVq\n" +
//            "5RXSJhiY+gUQFXKOWoqqxC2tMxcNBFB6M6hVIavfHLpk7PuFBFjb7wqK6nFXXQYM\n" +
//            "fbOXD4Wm4eTHq/WujNsJM9cejJTgSiVhnc7j0iYa0u5r8S/6BtmKCGTYdgJzPshq\n" +
//            "ZFIfKxgXeyAMu+EXV3phXWx3CYjAutlG4gjiT6B05asxQ9tb/OD9EI5LgtEgqTrS\n" +
//            "yv//////////AgEC\n" +
//            "-----END DH PARAMETERS-----";
//    public static final int DH_PRIVATE_KEY_BITS_SIZE = 1024;
//    public static final String RSA_PUBLIC_PEM_STRING = publicKeyRSA3072PEM;

//    // constants for dynamic secret
//    public static final int Ks_PUBLIC_KEY_LENGTH = 64;
//    public static final int Ks_PRIVATE_KEY_LENGTH = 32;
//    public static final int ECDH_PUBLIC_KEY_LENGTH = 64;
//    public static final int ECDH_PRIVATE_KEY_LENGTH = 32;
//    public static final String ECDH_AND_KD_CURVE = "secp256r1";
//    public static final int SECURE_SIGN_ON_CODE_SIZE = 16;

}
