package com.example.edwar.ndniotcontroller.signOnTLVs;

public class SignOnTLVs {

    // sign-on TLV types for basic
    public static int N2_PUB_TLV_TYPE = 0x82;
    public static int KD_PRI_ENCRYPTED_TLV_TYPE = 0x81;

    // sign-on TLV types for dynamic secret
    public static int TOKEN_BYTES_TLV_TYPE = 0x83;

}
