#ifndef ONBOARDING_CLIENT_HPP
#define ONBOARDING_CLIENT_HPP

#include <esp8266ndn.h>
#include "../consts.hpp"
#include "security-helper.hpp"

/** \brief Onboarding client for secure sign-on protocol.
 */
class OnboardingClient : public ndn::PacketHandler
{
public:
  explicit
  OnboardingClient(ndn::Face& face, const uint8_t* BKpub, const uint8_t* BKpri);

  ~OnboardingClient() noexcept;

  bool
  begin(const ndn::NameLite& topName);

  bool
  initiateSignOn();

  bool
  onboardingCompleted() {
    return m_onboarding_completed;
  }

  const ndn::NameLite&
  getDeviceID() {
    return m_deviceID;
  }

  const ndn::NameLite&
  getNetworkPrefix() {
    return m_networkPrefix;
  }

private:

  bool
  sendBootstrappingRequest();

  bool 
  sendCertificateRequest();

  bool 
  processData(const ndn::DataLite& data, uint64_t endpointId) override;

  bool
  processBootstrappingInterestResponse(const ndn::DataLite& data, uint64_t endpointId);

  bool
  processCertificateRequestInterestResponse(const ndn::DataLite& data, uint64_t endpointId);

  bool
  verifyBKpubMAC();

  bool
  AKpubRetrieved() {
    for (int i = 0; i < 64; i++) {
      if (m_AKpub[i] != 0)
        return true;
    }
    return false;
  }

private:
  ndn::Face& m_face;

  char m_bootstrapPrefixString[13] = "/ndn/sign-on";
  ndn_NameComponent m_bootstrapPrefixComps[2];
  ndn::NameLite m_bootstrapPrefix;

  char m_certNameComponentString[6] = "/cert";
  ndn_NameComponent m_certNameComponentComps[1];
  ndn::NameLite m_certNameComponent;

  const uint8_t* m_BKpub;
  const uint8_t* m_BKpri;

  ndn_NameComponent m_dummyKeyComp[1];
  ndn::NameLite m_dummyKeyName;

  ndn_NameComponent m_deviceIDNameComp[1];
  ndn::NameLite m_deviceID;

  ndn_NameComponent m_networkPrefixComps[MAX_NAME_COMPS];
  ndn::NameLite m_networkPrefix;

  std::unique_ptr<ndn::PacketBuffer> m_lastBootstrapResponse;

  uint8_t m_DKpub[64];
  uint8_t m_DKpri[32];

  uint8_t m_CKpub[64];
  uint8_t m_CKpri[32];

  uint8_t m_ECC_PUBLIC_DIGEST[ndn_SHA256_DIGEST_SIZE];
  uint8_t m_BKpubMacByDKpub[ndn_SHA256_DIGEST_SIZE];
  char m_BKpubDigestChars[65];

  ndn::EcPrivateKey m_bootstrapPrivateKey;
  uint8_t m_sharedSecret[32];

  SecurityHelper m_securityHelper;

  uint8_t m_AKpub[64];

  bool m_onboarding_completed;
};

#endif // SENSING_SERVER_HPP
