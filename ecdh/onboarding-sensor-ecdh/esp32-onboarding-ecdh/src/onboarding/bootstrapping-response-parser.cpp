
#include "bootstrapping-response-parser.hpp"
#include <ndn-cpp/c/encoding/tlv/tlv-decoder.h>
#include <ndn-cpp/c/errors.h>
#include "../consts.hpp"
#include <Streaming.h>
#include "../helpers/print-byte-array-helper.hpp"
#include "../logger.hpp"
#include <PriUint64.h>
#include "../helpers/tlv-helpers.hpp"

#define LOG(...) LOGGER(BootstrappingResponseParser, __VA_ARGS__)

BootstrappingResponseParser::BootstrappingResponseParser(const uint8_t* payload, size_t payloadLen)
  : m_keyNameComponent(m_keyNameComp, 1)
{
  m_payload = payload;
  m_payloadLen = payloadLen;

  ndn::parseNameFromUri(m_keyNameComponent, m_keyNameComponentString);
}

BootstrappingResponseParser::~BootstrappingResponseParser()
{

}

bool
BootstrappingResponseParser::checkForExpectedTlvs()
{
  ndn_TlvDecoder_initialize(&m_bootstrapping_data_decoder, m_payload, m_payloadLen);

  return readTypeAndLength(m_bootstrapping_data_decoder, EK_PUB_TLV_TYPE, 
          m_EKpubTlvTypeAndLengthSize, m_EKpubTlvValueSize, "EK pub TLV") && 
        readTypeAndLength(m_bootstrapping_data_decoder, BK_PUB_MAC_TLV_TYPE, 
          m_BKpubMacTlvTypeAndLengthSize, m_BKpubMacTlvValueSize, "BK pub MAC TLV") && 
        readTypeAndLength(m_bootstrapping_data_decoder, DATA_TLV_TYPE, 
          m_trustAnchorTlvTypeAndLengthSize, m_trustAnchorTlvValueSize, "trust anchor TLV");
}

bool
BootstrappingResponseParser::getNetworkPrefixAndAKpub(ndn::NameLite &nameDestination, uint8_t *AKpubDestination)
{
  ndn_TlvDecoder_initialize(&m_bootstrapping_data_decoder, m_payload, m_payloadLen);

  if (!(readTypeAndLength(m_bootstrapping_data_decoder, EK_PUB_TLV_TYPE, 
          m_EKpubTlvTypeAndLengthSize, m_EKpubTlvValueSize, "EK pub TLV") &&
        readTypeAndLength(m_bootstrapping_data_decoder, BK_PUB_MAC_TLV_TYPE, 
          m_BKpubMacTlvTypeAndLengthSize, m_BKpubMacTlvValueSize, "BK pub MAC TLV"))) {
    return false;
  }

  ndn_TlvDecoder_initialize(&m_trust_anchor_decoder, m_payload + m_bootstrapping_data_decoder.offset + m_trustAnchorTlvTypeAndLengthSize,
    m_bootstrapping_data_decoder.inputLength - m_bootstrapping_data_decoder.offset - m_trustAnchorTlvTypeAndLengthSize);

  LOG("Bytes of trust anchor (without header): " << endl << 
    PrintByteArray(m_trust_anchor_decoder.input, 0, m_trust_anchor_decoder.inputLength));

  size_t trustAnchorNameTlvValueSize, trustAnchorNameTlvTypeAndLengthSize;

  if (!(readTypeAndLength(m_trust_anchor_decoder, NAME_TLV_TYPE, 
          trustAnchorNameTlvTypeAndLengthSize, trustAnchorNameTlvValueSize, "Trust anchor Name TLV"))) {
    return false;
  }

  ndn_NameComponent trustAnchorNameComps[MAX_NAME_COMPS];
  ndn::NameLite trustAnchorName(trustAnchorNameComps, MAX_NAME_COMPS);

  ndn_Error nameDecodingError;
  size_t nameSignedPortionBeginOffset, nameSignedPortionEndOffset, inputLength;
  if (nameDecodingError = ndn::Tlv0_2WireFormatLite::decodeName(trustAnchorName, 
      m_trust_anchor_decoder.input, 
      trustAnchorNameTlvTypeAndLengthSize + trustAnchorNameTlvValueSize,
      &nameSignedPortionBeginOffset, &nameSignedPortionEndOffset)) {
    LOG("Error decoding name of trust anchor.");
    LOG("Error: ");
    LOG(ndn_getErrorString(nameDecodingError));
    return false;
  }

  int keyNameComponentIndex = 0;
    for (keyNameComponentIndex = 0; keyNameComponentIndex < trustAnchorName.size(); keyNameComponentIndex++) {
      if (trustAnchorName.get(keyNameComponentIndex).equals(m_keyNameComponent.get(0))) {
        LOG("Found the KEY name component in trust anchor name at index " << keyNameComponentIndex);
        break;
    }
  }

  nameDestination.clear();
  for (int i = 0; i < keyNameComponentIndex; i++) {
      nameDestination.append(trustAnchorName.get(i));
  }

  size_t trustAnchorMetaInfoTlvValueSize, trustAnchorMetaInfoTlvTypeAndLengthSize, 
    trustAnchorContentTlvValueSize, trustAnchorContentTlvTypeAndLengthSize;
  
  if (!(readTypeAndLength(m_trust_anchor_decoder, METAINFO_TLV_TYPE, 
          trustAnchorMetaInfoTlvTypeAndLengthSize, trustAnchorMetaInfoTlvValueSize, "Trust anchor Name TLV") &&
        readTypeAndLength(m_trust_anchor_decoder, CONTENT_TLV_TYPE, 
          trustAnchorContentTlvTypeAndLengthSize, trustAnchorContentTlvValueSize, "Trust anchor Name TLV"))) {
    return false;
  }

  const uint8_t *trustAnchorPayload = m_trust_anchor_decoder.input +
                                      trustAnchorNameTlvTypeAndLengthSize + trustAnchorNameTlvValueSize +
                                      trustAnchorMetaInfoTlvTypeAndLengthSize + trustAnchorMetaInfoTlvValueSize +
                                      trustAnchorContentTlvTypeAndLengthSize;

  // assumes the payload of the trust anchor content is an ASN encoded public key, so it skips 27 bytes for the ASN header
  for (int i = 0; i < 64; i++) {
    AKpubDestination[i] = trustAnchorPayload[27 + i];
  }

  LOG("Bytes of AKpub: " << endl << PrintByteArray(AKpubDestination, 0, 64));

  return true;

}
