#ifndef SECURITY_HELPER_HPP
#define SECURITY_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/entropy.h"    // entropy gathering
#include "mbedtls/ctr_drbg.h"   // CSPRNG
#include "mbedtls/error.h"      // error code to string conversion
#include "mbedtls/md.h"         // hash algorithms
#include "mbedtls/sha256.h"

#include <esp8266ndn.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class SecurityHelper
{
public:
  explicit
  SecurityHelper();

  ~SecurityHelper();

  void
  sha256(const uint8_t* input, size_t inputLen, uint8_t* output);

  void
  deriveSharedSecretECDH(const uint8_t *EKpub, const uint8_t *DKpri, uint8_t *sharedSecretBytes);

  void
  generateBKpubMacByDKpub(const uint8_t *DKpub, const uint8_t *BKpub, uint8_t *BKpubMacByDKpub);

  bool
  checkBKpubMacFromController(const uint8_t *myBKpubMac, const uint8_t *controllerBKpubMac);

  bool
  verifyDataByTSK(const uint8_t *tskBytes, const ndn::DataLite &data);

private:

private:
  mbedtls_entropy_context m_entropy;
  mbedtls_ctr_drbg_context m_ctr_drbg;
  mbedtls_md_context_t m_ctx;
  mbedtls_md_type_t m_md_type = MBEDTLS_MD_SHA256;

};

#endif // SECURITY_HELPER_HPP