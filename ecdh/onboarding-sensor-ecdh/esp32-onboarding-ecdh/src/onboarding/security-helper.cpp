
#include "security-helper.hpp"
#include "../consts.hpp"
#include <security/detail/uECC.h>
#include <esp8266ndn.h>
#include "../logger.hpp"

#define LOG(...) LOGGER(SecurityHelper, __VA_ARGS__)

SecurityHelper::SecurityHelper()
{
  mbedtls_md_init(&m_ctx);
  mbedtls_md_setup(&m_ctx, mbedtls_md_info_from_type(m_md_type), 1);
}

SecurityHelper::~SecurityHelper()
{
  mbedtls_entropy_free(&m_entropy);
  mbedtls_ctr_drbg_free(&m_ctr_drbg);
  mbedtls_md_free(&m_ctx);
}

void
SecurityHelper::deriveSharedSecretECDH(const uint8_t *EKpub, const uint8_t *DKpri, uint8_t *sharedSecretBytes)
{
  uECC_shared_secret(EKpub, DKpri, sharedSecretBytes);
}

void 
SecurityHelper::generateBKpubMacByDKpub(const uint8_t *DKpub, const uint8_t *BKpub, uint8_t *BKpubMacByDKpub)
{
  mbedtls_md_hmac_starts(&m_ctx, DKpub, 64);
  mbedtls_md_hmac_update(&m_ctx, BKpub, 64);
  mbedtls_md_hmac_finish(&m_ctx, BKpubMacByDKpub);
}

bool
SecurityHelper::checkBKpubMacFromController(const uint8_t *myBKpubMac, const uint8_t *controllerBKpubMac)
{
  for (int i = 0; i < ndn_SHA256_DIGEST_SIZE; i++) {
    if (myBKpubMac[i] != controllerBKpubMac[i])
      return false;
  }

  return true;
}

bool
SecurityHelper::verifyDataByTSK(const uint8_t *tskBytes, const ndn::DataLite &data)
{
  ndn_NameComponent dataNameComps[MAX_NAME_COMPS];
  ndn_NameComponent dataKeyLocatorNameComps[MAX_NAME_COMPS];
  ndn::DataLite tempData(dataNameComps, MAX_NAME_COMPS, dataKeyLocatorNameComps, MAX_NAME_COMPS);

  tempData.set(data);

  ndn::HmacKey tsk(tskBytes, ndn_SHA256_DIGEST_SIZE);

  uint8_t encoding[3000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeData
      (tempData, &signedPortionBeginOffset,
      &signedPortionEndOffset, output, &encodingLength))) {
    LOG("Error encoding data from bootstrapping response, in order to do verification by TSK.");
    LOG("Error: " << ndn_getErrorString(error) << endl);
    return false;
  }

    if (tsk.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
        tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
      LOG("Successfully verified bootstrapping request response data packet.");
    }
    else {
      LOG("Bootstrapping request response data packet failed to verify.");
      return false;
    }

    return true;
}