package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class SecurityHelpers {

    private final static String TAG = "SecurityHelpers";

    public static final int EC_PUBLIC_KEY_LENGTH = 64;
    public static final int EC_PRIVATE_KEY_LENGTH = 32;

    public static byte[] hmacSha256(byte[] key, byte[] message)
    throws NoSuchAlgorithmException, InvalidKeyException {
        Mac sha256_HMAC = null;
        byte[] mac;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
            sha256_HMAC.init(secret_key);
            mac = sha256_HMAC.doFinal(message);
        } catch (NoSuchAlgorithmException e) {
            throw e;
        } catch (InvalidKeyException e) {
            throw e;
        }

        return mac;
    }

    public static byte[] asnEncodeRawECPublicKeyBytes(byte[] rawPublicKeyBytes) throws Exception {

        if (rawPublicKeyBytes.length != 64)
            throw new Exception("Currently only asn encoding of 64 byte raw public keys is supported.");

        byte[] pubKeyASNHeader = {
                0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, (byte) 0x86, 0x48, (byte) 0xCE,
                0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, (byte) 0x86, 0x48, (byte) 0xCE, 0x3D,
                0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04
        };

        byte[] pubKeyASNEncoded = new byte[rawPublicKeyBytes.length + pubKeyASNHeader.length];
        System.arraycopy(pubKeyASNHeader, 0, pubKeyASNEncoded, 0, pubKeyASNHeader.length);
        System.arraycopy(rawPublicKeyBytes, 0, pubKeyASNEncoded, pubKeyASNHeader.length, rawPublicKeyBytes.length);

        return pubKeyASNEncoded;
    }

    // security helper function specific to ecdh implementation

    public static KeyPair generateECKeyPair(String curve) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        // obtain an EC key pair generator for the specified named curve
        KeyPairGenerator generator = null;
        try {
            generator = java.security.KeyPairGenerator.getInstance("EC");
            ECGenParameterSpec ecName = new ECGenParameterSpec(curve);
            generator.initialize(ecName);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw e;
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            throw e;
        }

        KeyPair ecKeyPair = generator.generateKeyPair();
        Log.d(TAG, "Hex of generated public key: " + getEcPublicKeyAsHex(ecKeyPair.getPublic()));
        Log.d(TAG, "Hex of generated private key: " + getEcPrivateKeyAsHex(ecKeyPair.getPrivate()));

        return ecKeyPair;
    }

    public static PublicKey convertASNEncodedECPublicKeyToPublicKeyObject(byte[] ASNEncodedpubKey)
            throws InvalidKeySpecException, NoSuchAlgorithmException {

        PublicKey pubKey;
        try {
            pubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(ASNEncodedpubKey));
        } catch (InvalidKeySpecException e) {
            throw e;
        } catch (NoSuchAlgorithmException e) {
            throw e;
        }

        return pubKey;
    }

    public static byte[] deriveSharedSecretECDH(PrivateKey priKey, PublicKey pubKey)
            throws NoSuchAlgorithmException, InvalidKeyException {

        KeyAgreement aKeyAgreement = null;
        try {
            aKeyAgreement = KeyAgreement.getInstance("ECDH");
        } catch (NoSuchAlgorithmException e) {
            throw e;
        }

        try {
            aKeyAgreement.init(priKey);
        } catch (InvalidKeyException e) {
            throw e;
        }

        try {
            aKeyAgreement.doPhase(pubKey, true);
        } catch (InvalidKeyException e) {
            return null;
        }

        byte[] sharedSecret = aKeyAgreement.generateSecret();

        return sharedSecret;
    }

    public static String getEcPublicKeyAsHex(PublicKey publicKey) {

        ECPublicKey ecPublicKey = (ECPublicKey) publicKey;
        ECPoint ecPoint = ecPublicKey.getW();

        byte[] publicKeyBytes = new byte[EC_PUBLIC_KEY_LENGTH];
        writeToStream(publicKeyBytes, 0, ecPoint.getAffineX(), EC_PRIVATE_KEY_LENGTH);
        writeToStream(publicKeyBytes, EC_PRIVATE_KEY_LENGTH, ecPoint.getAffineY(), EC_PRIVATE_KEY_LENGTH);

        Blob hexBlob = new Blob(publicKeyBytes);
        String hex = hexBlob.toHex();

        return hex;
    }

    public static String getEcPrivateKeyAsHex(PrivateKey privateKey) {

        ECPrivateKey ecPrivateKey = (ECPrivateKey) privateKey;
        BigInteger ecPoint = ecPrivateKey.getS();

        byte[] privateKeyBytes = ecPoint.toByteArray();

        Blob hexBlob = new Blob(privateKeyBytes);
        String hex = hexBlob.toHex();

        return hex;

    }

    private static void writeToStream(byte[] stream, int start, BigInteger value, int size) {
        byte[] data = value.toByteArray();
        int length = Math.min(size, data.length);
        int writeStart = start + size - length;
        int readStart = data.length - length;
        System.arraycopy(data, readStart, stream, writeStart, length);
    }
}
