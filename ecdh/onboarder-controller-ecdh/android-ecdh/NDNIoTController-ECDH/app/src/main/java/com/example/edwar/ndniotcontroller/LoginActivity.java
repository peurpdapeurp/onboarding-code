package com.example.edwar.ndniotcontroller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private EditText m_home_prefix_input = null;
    private Button m_button_ok = null;

    // shared preferences object to store login parameters for next time
    SharedPreferences mPreferences;
    SharedPreferences.Editor mPreferencesEditor;
    private static String HOME_PREFIX = "HOME_PREFIX";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mPreferences = getSharedPreferences("mPreferences", Context.MODE_PRIVATE);
        mPreferencesEditor = mPreferences.edit();

        m_home_prefix_input = (EditText) findViewById(R.id.input_home_prefix);

        m_button_ok = (Button) findViewById(R.id.button_ok);

        m_home_prefix_input.setText(mPreferences.getString(HOME_PREFIX, "/default/home/prefix"));

        m_button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String homePrefix = m_home_prefix_input.getText().toString().trim();

                if (homePrefix.equals("")) {
                    Toast toast = Toast.makeText(LoginActivity.this, "Please enter a home prefix", Toast.LENGTH_SHORT);
                    toast.show();

                    return;
                }

                // all the inputs are good, save them for next time

                mPreferencesEditor.putString(HOME_PREFIX, homePrefix).commit();

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                String[] secureSignOnControllerInitializationInfo = new String[1];
                secureSignOnControllerInitializationInfo[SecureSignOnController.INITIALIZATION_INFO_HOME_PREFIX] = homePrefix;
                intent.putExtra(SecureSignOnController.INITIALIZATION_INFO, secureSignOnControllerInitializationInfo);

                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        hideKeyboard(this);

        return super.onTouchEvent(event);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}