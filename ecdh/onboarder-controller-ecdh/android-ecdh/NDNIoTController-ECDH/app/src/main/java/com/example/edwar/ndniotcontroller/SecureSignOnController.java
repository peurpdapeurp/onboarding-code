package com.example.edwar.ndniotcontroller;

import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.InterestFilter;
import net.named_data.jndn.Name;
import net.named_data.jndn.OnInterestCallback;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.VerificationHelpers;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;

import static com.example.edwar.ndniotcontroller.utils.EncodingHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.OtherHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;


public class SecureSignOnController {

    private final String TAG = "SecureSignOnController";

    public static String INITIALIZATION_INFO = "INITIALIZATION_INFO";
    public static int INITIALIZATION_INFO_HOME_PREFIX = 0;

    private final int PUBLIC_KEY_LENGTH = 64;
    private final int PRIVATE_KEY_LENGTH = 32;

    private class DeviceInfo {
        CertificateV2 BKpubCertificate;
        byte[] tsk;
        boolean onboarded = false;
        // the variables below will only be populated once the device is finished onboarding
        CertificateV2 CKpubCertificate;
        String deviceID;
    }

    public SecureSignOnController() throws Exception {
        deviceCounter_ = 0;
        devices_ = new HashMap<>();

        try {
            sha256_ = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException exception) {
            throw new Error ("MessageDigest: SHA-256 is not supported: " + exception.getMessage());
        }

    }

    public void initialize(Face face, KeyChain keyChain, Name homePrefix, OnRegisterSuccess onRegisterSuccess, OnRegisterFailed onRegisterFailed,
                           CertificateV2 anchorCertificate) throws IOException, SecurityException, Exception {

        if (face == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null face.");
        if (homePrefix == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null home prefix.");
        if (onRegisterFailed == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null prefix registration failure callback.");
        if (onRegisterSuccess == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null prefix registration success callback.");
        if (anchorCertificate == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null anchor certificate.");

        face_ = face;
        homePrefix_ = new Name(homePrefix);
        keyChain_ = keyChain;

        face_.registerPrefix(new Name("/ndn/sign-on"), onBootstrappingInterest_, onRegisterFailed, onRegisterSuccess);
        face_.registerPrefix(homePrefix_.append("cert"), onCertificateRequestInterest_, onRegisterFailed, onRegisterSuccess);

        anchorCertificate_ = anchorCertificate;

    }

    public void addDevice(CertificateV2 BKpubCertificate) throws CertificateV2.Error {

        try {
            sha256_.update(BKpubCertificate.getPublicKey().buf());
        } catch (CertificateV2.Error error) {
            error.printStackTrace();
            throw error;
        }

        byte[] BKpubHashBytes = sha256_.digest();
        Blob BKpubHashBlob = new Blob(BKpubHashBytes);
        String BKpubHash = BKpubHashBlob.toHex();
        Log.d(TAG, "BKpubHash of device being added to secure sign on controller's device list: " + BKpubHash);

        if (!devices_.containsKey(BKpubHash)) {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.BKpubCertificate = BKpubCertificate;
            devices_.put(BKpubHash, deviceInfo);
        }

    }

    OnInterestCallback onBootstrappingInterest_ = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
            Log.d(TAG, "Got a bootstrapping interest with name: " + interest.getName().toUri());

            Name bootstrappingInterestName = interest.getName();

            String BKpubASNEncodedHash = bootstrappingInterestName.get(-4).getValue().toString();
            Log.d(TAG, "BKpubASNEncodedHash received from bootstrapping interest: " + BKpubASNEncodedHash);

            if (!devices_.containsKey(BKpubASNEncodedHash)) {
                Log.d(TAG, "Received a bootstrapping interest from a device not scanned yet. BKpubHash: " + BKpubASNEncodedHash);
                return;
            }

            DeviceInfo currentDeviceInfo = devices_.get(BKpubASNEncodedHash);

            if (!VerificationHelpers.verifyInterestSignature(interest, currentDeviceInfo.BKpubCertificate)) {
                Log.d(TAG, "Failed to verify bootstrapping interest from device. BKpubHash: " + BKpubASNEncodedHash);
                return;
            }

            KeyPair EKKeyPair = null;
            try {
                EKKeyPair = generateECKeyPair("secp256r1");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
                return;
            }

            byte[] DKpubRaw = bootstrappingInterestName.get(-3).getValue().getImmutableArray();
            Blob DKpubRawBlob = new Blob(DKpubRaw);
            Log.d(TAG, "Hex of received DKpubRaw: " + DKpubRawBlob.toHex());

            byte[] DKpubASNEncoded = new byte[0];
            try {
                DKpubASNEncoded = asnEncodeRawECPublicKeyBytes(DKpubRaw);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            PublicKey DKpub = null;
            try {
                DKpub = convertASNEncodedECPublicKeyToPublicKeyObject(DKpubASNEncoded);
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
                return;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            }

            byte[] sharedSecret = null;
            try {
                sharedSecret = deriveSharedSecretECDH(EKKeyPair.getPrivate(), DKpub);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            } catch (InvalidKeyException e) {
                e.printStackTrace();
                return;
            }

            Blob sharedSecretBlob = new Blob(sharedSecret);
            Log.d(TAG, "Hex of shared secret: " + sharedSecretBlob.toHex());

            currentDeviceInfo.tsk = Arrays.copyOf(sharedSecret, sharedSecret.length);

            byte[] BKpubASNEncodedBytes = null;
            try {
                BKpubASNEncodedBytes = currentDeviceInfo.BKpubCertificate.getPublicKey().getImmutableArray();
            } catch (CertificateV2.Error error) {
                error.printStackTrace();
                return;
            }
            byte[] BKpubRawBytes = new byte[64];
            System.arraycopy(BKpubASNEncodedBytes, 27, BKpubRawBytes, 0, 64);

            byte[] macOfBKpubASNEncodedByDKpub = new byte[0];
            try {
                macOfBKpubASNEncodedByDKpub = hmacSha256(DKpubRaw, BKpubRawBytes);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            } catch (InvalidKeyException e) {
                e.printStackTrace();
                return;
            }

            Blob macOfBKpubASNEncodedByDKpubBlob = new Blob(macOfBKpubASNEncodedByDKpub);
            Log.d(TAG, "Hex of MAC of BKpubASNEncoded by DKpub: " + macOfBKpubASNEncodedByDKpubBlob.toHex());

            Data bootstrappingResponse = constructBootstrappingResponseECDH(interest.getName(), anchorCertificate_,
                    macOfBKpubASNEncodedByDKpub, EKKeyPair.getPublic());

            signDataBySymmetricKeyAndPublish(currentDeviceInfo.tsk, face_, bootstrappingResponse);
        }
    };
    OnInterestCallback onCertificateRequestInterest_ = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
            Log.d(TAG, "Got a certificate request interest with name: " + interest.getName().toUri());

            Name certificateRequestInterestName = interest.getName();

            String BKpubHash = certificateRequestInterestName.get(-4).getValue().toString();

            if (!devices_.containsKey(BKpubHash)) {
                Log.d(TAG, "Got certificate request from device we did not recognize. BKpubHash: " + BKpubHash);
                return;
            }

            DeviceInfo currentDeviceInfo = devices_.get(BKpubHash);

            if (!KeyChain.verifyInterestWithHmacWithSha256(interest, new Blob(currentDeviceInfo.tsk))) {
                Log.d(TAG, "Failed to verify certificate request by tsk.");
                return;
            }

            // get CKpub bytes from the proper interest name component
            byte[] CKpubRaw = certificateRequestInterestName.get(-3).getValue().getImmutableArray();

            long currentTime = System.currentTimeMillis();

            CertificateV2 CKpubCertificate = new CertificateV2();
            CKpubCertificate.setName(homePrefix_);
            CKpubCertificate.getName().append("DEVICE" + deviceCounter_);
            CKpubCertificate.getName().append("KEY");
            CKpubCertificate.getName().append(Name.Component.fromTimestamp(currentTime));
            try {
                CKpubCertificate.setContent(new Blob(asnEncodeRawECPublicKeyBytes(CKpubRaw)));
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            // set validity period of certificate to ten days
            CKpubCertificate.getValidityPeriod().setPeriod(currentTime, currentTime + 1000 * 60 * 60 * 24 * 10);

            try {
                keyChain_.sign(CKpubCertificate, keyChain_.getDefaultCertificateName());
            } catch (SecurityException e) {
                e.printStackTrace();
                return;
            }

            if (!VerificationHelpers.verifyDataSignature(CKpubCertificate, anchorCertificate_)) {
                Log.d(TAG, "Failed to verify the CKpub certificate we generated using the anchor certificate.");
                return;
            }

            Data certificateRequestResponse = constructCertificateRequestResponse(interest.getName(), CKpubCertificate);

            signDataBySymmetricKeyAndPublish(currentDeviceInfo.tsk, face_, certificateRequestResponse);
        }
    };

    private Face face_;
    private Name homePrefix_;
    private CertificateV2 anchorCertificate_;
    private KeyChain keyChain_;
    private HashMap<String, DeviceInfo> devices_;
    private MessageDigest sha256_;
    private long deviceCounter_;


}