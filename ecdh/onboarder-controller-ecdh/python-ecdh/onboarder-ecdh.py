import sys, os

from pyndn import Name, Data, HmacWithSha256Signature, KeyLocatorType
from pyndn.security import KeyChain
from pyndn.security.v2 import CertificateV2
from pyndn.util import Blob
from pyndn import Face, Interest
import time, random
from threading import Thread
from pyndn.encoding import ProtobufTlv
from datetime import datetime
import signal
import binascii
from binascii import unhexlify, hexlify
import base64
import hashlib
import hmac
from pyndn.security import VerificationHelpers
from pyndn.encoding import TlvEncoder
from pyndn.security import EcKeyParams
from pyndn.security import ValidityPeriod
from Tkinter import *
from threading import Timer

current_milli_time = lambda: int(round(time.time() * 1000))

def handler(signal, frame):
    print('Caught keyboard interrupt, exiting...\n')
    t = None
    face.shutdown();
    with open('%s' % (deviceIDListName), 'a') as the_file:
        the_file.seek(0)
        the_file.truncate(0)
    exit()

def run_face_processing():
    while True:
        face.processEvents()
        # We need to sleep for a few milliseconds so we don't use 100% of the CPU.
        time.sleep(0.01)

def clearTSK(BKpubHash, reason):
    print "Clearing tsk for device with BKpubHash: " + BKpubHash
    try:
        scannedDeviceInfos[BKpubHash] = [scannedDeviceInfos[BKpubHash][0], ""]
        deviceCertificate = CertificateV2()
        deviceCertificate.wireDecode(bytearray(scannedDeviceInfos[BKpubHash][0]))
        
        indexToDelete = check_index(onboardingDevicesList, deviceCertificate.getName().toUri())
        onboardingDevicesList.delete(indexToDelete)

        pendingDevicesList.insert(END, deviceCertificate.getName().toUri())
    except:
        print "There was no entry for " + BKpubHash
    try:
        del onboardingTimers[BKpubHash]
    except:
        print "No onboarding timer for " + BKpubHash

    if (reason == "timeout"):
        bootstrapTimeoutPopupmsg(BKpubHash)
    
def startOnboardingTimer(BKpubHash):
    t = Timer(4.0, function = lambda: clearTSK(BKpubHash, "timeout"))
    if (not BKpubHash in onboardingTimers):
        onboardingTimers[BKpubHash] = t
        onboardingTimers[BKpubHash].start()
        return True
    else:
        return False
    print "Started onboarding timer for device with BKpubHash: " + BKpubHash
    
def cancelOnboardingTimer(BKpubHash):
    print "Cancelling onboarding timer for device with BKpubHash: " + BKpubHash
    onboardingTimers[BKpubHash].cancel()
    del onboardingTimers[BKpubHash]
            
def onInterestVerificationSucceeded(self, interest):
    print "Successfully verified interest with name: " + interest.getName().toUri()

def onInterestVerificationFailed(self, interest, reason):
    print "Failed to verify interest with name: " + interest.getName().toUri() + " for reason " + reason

def generateEKKeyPair(BKpubHash):

    privateKeyPemPath = pemFilesLocation + BKpubHash + "-EKpvt" + ".pem"
    
    os.system("openssl ecparam -name prime256v1 -genkey -noout -out " + privateKeyPemPath)
    os.system("openssl ec -text -in " + privateKeyPemPath)

    if (not os.path.isfile(privateKeyPemPath)):
        return ""

    return privateKeyPemPath

def asnEncodeRawPubBytes(BKpubHash, PubBytes, pemID):

    publicKeyPemPath = pemFilesLocation + BKpubHash + pemID + ".pem"
    
    print "Hex string of Pub bytes received with pemID " + pemID + ": " + binascii.hexlify(PubBytes)
            
    ASNEncodedPubBytes = asnPubKeyPrefix + PubBytes

    ASNEncodedPubBytesHexString = binascii.hexlify(ASNEncodedPubBytes)
    print "Hex string of Pub bytes with ASN prefix prepended: " + ASNEncodedPubBytesHexString

    os.system("echo \"" + ASNEncodedPubBytesHexString + "\
    \"| xxd -r -p - | openssl ec -inform der -pubin -pubout -out " + publicKeyPemPath)

    os.system("openssl ec -pubin -in " + publicKeyPemPath + " -text");
    
    if (not os.path.isfile(publicKeyPemPath)):
        return ""

    return publicKeyPemPath

def asnEncodeRawPubBytesGetHexString(BKpubHash, PubBytes):

    ASNEncodedPubBytes = asnPubKeyPrefix + PubBytes

    ASNEncodedPubBytesHexString = binascii.hexlify(ASNEncodedPubBytes)

    return ASNEncodedPubBytesHexString

def generateSharedSecret(privateKeyPemPath, publicKeyPemPath):

    sharedSecretHexString = os.popen("openssl pkeyutl -derive -inkey " + privateKeyPemPath + " -peerkey " + publicKeyPemPath + " | xxd -p | tr -d '\n'").read()

    if (len(sharedSecretHexString) != PRIVATE_KEY_LEN * 2):
        return ""

    print "Shared secret hex string: " + sharedSecretHexString
    
    return sharedSecretHexString

def getPubKeyHexString(pvtKeyPemPath):

    pubKeyHexString = os.popen(" openssl ec -text -in " + pvtKeyPemPath + " | tr -d '\n' | tr -d ' ' | sed 's/^.*pub:/pub:/' | cut -c7-198 | tr -d ':' | tr -d '\n'").read()

    print "Pub key hex string: " + pubKeyHexString

    return pubKeyHexString

def generateEKpubTLV(EKpvtPemPath):
    publicKeyBitsTLVHeader = bytearray([
        0x82, PUBLIC_KEY_LEN
    ])

    EKpubHexString = getPubKeyHexString(EKpvtPemPath)
        
    EKpubTLV = publicKeyBitsTLVHeader + bytearray.fromhex(EKpubHexString)

    return EKpubTLV

def signDataWithTSKAndPutToFace(dataName, sharedSecretHexString, tlvEncoder):
    data = Data(dataName.appendVersion(current_milli_time()))
    signature = HmacWithSha256Signature()
    signature.getKeyLocator().setType(KeyLocatorType.KEYNAME)
    keyName = keyChain.getPib().getIdentity(Name(networkPrefix)).getDefaultKey().getDefaultCertificate().getName()
    print "Key name we are signing data with: " + keyName.toUri()
    signature.getKeyLocator().setKeyName(keyName)
    print "Name of key locator in signature info we are sending: " + signature.getKeyLocator().getKeyName().toUri()
    data.setSignature(signature)
    content = Blob(tlvEncoder.getOutput())
    data.setContent(content)
    print "Signing data packet " + data.getName().toUri()
    sharedSecretKey = Blob(bytearray.fromhex(sharedSecretHexString))        
    keyChain.signWithHmacWithSha256(data, sharedSecretKey)
    face.putData(data)
        
class BootstrappingListener(object):
    def __init__(self, keyChain, certificateName):
        self._keyChain = keyChain
        self._certificateName = certificateName

    def onInterest(self, prefix, interest, face, interestFilterId, filter):
        print "Got a bootstrapping interest with name: " + interest.getName().toUri()

        BKpubHash = str(interest.getName().get(-4).getValue())
        print "BK pub hash of received interest: " + BKpubHash
    
        if (BKpubHash in scannedDeviceInfos):
            print "We got a bootstrapping interest from a device we previously scanned, proceeding to process its bootstrapping interest."
        else:
            print "Did not recognize the BKpubHash " + BKpubHash + ", ignoring bootstrapping interest."
            bootstrapFailurePopupmsg(BKpubHash)
            return
        
        if (BKpubHash in successfullyOnboardedDevices):
            print "This device was already successfully onboarded, ignoring this onboarding request."
            bootstrapDuplicatePopupmsg(BKpubHash)
            return

        if (startOnboardingTimer(BKpubHash)):
            print "Successfully started timer for bootstrapping from device with BKpubHash: " + BKpubHash
        else:
            print "There was already a bootstrapping timer started for device with BKpubHash " + BKpubHash + ", meaning there is a pending bootstrapping request; ignoring this request."
            return
        
        deviceCertificate = CertificateV2()
        deviceCertificate.wireDecode(bytearray(scannedDeviceInfos[BKpubHash][0]))

        onboardingDevicesList.insert(END, deviceCertificate.getName().toUri())
        indexToDelete = check_index(pendingDevicesList, deviceCertificate.getName().toUri())
        pendingDevicesList.delete(indexToDelete)
        
        print "Identity of this device's certificate: " + deviceCertificate.getIdentity().toUri()

        if (VerificationHelpers.verifyInterestSignature(interest, deviceCertificate)):
            print "Successfully verified bootstrapping interest."
        else:
            print "Failed to verify bootstrapping interest."

        EKpvtPemPath = generateEKKeyPair(BKpubHash)
        if (EKpvtPemPath == ""):
            print "Problem generating EK key pair for device with BKpubhash " + BKpubHash
            return;
        else:
            print "Successfully generated EK key pair for ECDH with this device."

        DKpubBytes = bytearray(bytes(interest.getName().get(-3).getValue()))

        DKpubPemPath = asnEncodeRawPubBytes(BKpubHash, DKpubBytes, "DKpub")
        if (DKpubPemPath == ""):
            print "Problem asn encoding DK pub bytes for device with BKpubHash " + BKpubHash
            return;
        else:
            print "Successfully asn encoded DK pub bytes for ECDH with this device."

        sharedSecretHexString = generateSharedSecret(EKpvtPemPath, DKpubPemPath)
        if (sharedSecretHexString == ""):
            print "Problem generating shared secret for device with hBKpubHash " + BKpubHash
            return;
        else:
            print "Successfully generated shared secret for this device."

        scannedDeviceInfos[BKpubHash] = [scannedDeviceInfos[BKpubHash][0], sharedSecretHexString]
            
        tlvEncoder = TlvEncoder()

        # generate a mac signature of BKpub by the shared secret
        
        BKpubHexString = binascii.hexlify(bytearray(deviceCertificate.getPublicKey().buf()))[54:]

        print "Hex string of device certificate public key (ASN encoding header stripped): " + BKpubHexString
        print "Hex string of shared secret: " + sharedSecretHexString
        
        BKpubByteArray = bytearray.fromhex(BKpubHexString)
        sharedSecretByteArray = bytearray.fromhex(sharedSecretHexString)

        #print "EKpub hex string: " + getPubKeyHexString(EKpvtPemPath)
        
        #EKpubByteArray = bytearray.fromhex(getPubKeyHexString(EKpvtPemPath))
        
        BKpubDigest = hmac.new(DKpubBytes, BKpubByteArray, digestmod=hashlib.sha256).digest()

        print "Our generated BKpub digest by token1 (DKpub): " + binascii.hexlify(BKpubDigest)
        print "Length of BKpubDigest: " + str(len(BKpubDigest))
        
        BKpubDigestHeader = bytearray([
            0x81, len(BKpubDigest)
        ])
        
        BKpubDigestTLV = BKpubDigestHeader + BKpubDigest
        
        # push our wire encoded trust anchor into the content
        tlvEncoder.writeBuffer(trustAnchorCertificate.wireEncode().buf())

        # push the mac signature of BKpub by shared secret into content, type 129

        print "BKpub digest by TSK TLV hex string: " + binascii.hexlify(BKpubDigestTLV)
        
        tlvEncoder.writeBuffer(BKpubDigestTLV);
        
        # push our public key bits into content, type 130
        EKpubTLV = generateEKpubTLV(EKpvtPemPath)

        print "EKpubTLV hex string: " + binascii.hexlify(EKpubTLV)
        
        tlvEncoder.writeBuffer(EKpubTLV)
        
        signDataWithTSKAndPutToFace(interest.getName(), sharedSecretHexString, tlvEncoder)
        
    def onRegisterFailed(self, prefix):
        print "Register failed for prefix" + prefix.toUri()

class CertificateRequestListener(object):
    def __init__(self, keyChain, certificateName):
        self._keyChain = keyChain
        self._certificateName = certificateName

    def onInterest(self, prefix, interest, face, interestFilterId, filter):

        print "Got a certificate request interest with name: " + interest.getName().toUri()

        BKpubHash = str(interest.getName().get(-4).getValue())
        print "BK pub hash of received certificate request interest: " + BKpubHash

        if (BKpubHash in scannedDeviceInfos):
            print "We got a bootstrapping interest from a device we previously scanned, proceeding to process its bootstrapping interest."
        else:
            print "Did not recognize the BKpubHash " + BKpubHash + ", ignoring bootstrapping interest."
            return

        if (BKpubHash in onboardingTimers):
            cancelOnboardingTimer(BKpubHash)
        else:
            print "There was no pending timer for this certificate request, meaning it's too late to accept it."
            certRequestTooLatePopupmsg(BKpubHash)
            return
        
        sharedSecretHexString = scannedDeviceInfos[BKpubHash][1]

        print "Shared secret hex string for this device: " + sharedSecretHexString
        
        sharedSecretBlob = Blob(bytearray.fromhex(sharedSecretHexString))

        if (not keyChain.verifyInterestWithHmacWithSha256(interest, sharedSecretBlob)):
            print "Failed to verify certificate request interest with TSK for device with BKpubhash: " + BKpubHash
            clearTSK(BKpubHash, "certRequestFailVerify")
            return

        CKpubBytes = bytearray(bytes(interest.getName().get(-3).getValue()))

        CKpubASNEncodedHexString = asnEncodeRawPubBytesGetHexString(BKpubHash, CKpubBytes)
        if (CKpubASNEncodedHexString == ""):
            print "Problem asn encoding CK pub bytes for device with BKpubHash " + BKpubHash
            return;
        else:
            print "Successfully asn encoded CK pub bytes for generating certificate for this device."

        print "ASN encoded CK pub bytes hex string: " + CKpubASNEncodedHexString
            
        CKpubCertificate = CertificateV2()

        currentTime = current_milli_time()
        CKpubCertificate.setName(Name(networkPrefix))
        global numberOfDevicesOnboarded
        CKpubCertificate.getName().append("DEVICE" + str(numberOfDevicesOnboarded))
        numberOfDevicesOnboarded += 1
        CKpubCertificate.getName().appendVersion(currentTime)
        CKpubCertificate.getName().append("KEY");
        CKpubCertificate.getName().appendVersion(currentTime)
        
        CKpubCertificate.setContent(Blob(bytearray.fromhex(CKpubASNEncodedHexString)))

        currentTime = current_milli_time()
        CKpubCertificate.getSignature().setValidityPeriod(ValidityPeriod(currentTime, currentTime + 1000 * 60 * 60 * 24 * 10))
        
        keyChain.sign(CKpubCertificate, keyChain.getPib().getIdentity(Name(networkPrefix)).getDefaultKey().getDefaultCertificate().getName())

        print "Name of the CKpub certificate we generated: " + CKpubCertificate.getName().toUri()

        print "Name of key locator of CKpub certificate we generated: " + CKpubCertificate.getSignature().getKeyLocator().getKeyName().toUri()

        print "Validity period of CKpub certificate we generated: "
        print "Validity period beginning: " + str(CKpubCertificate.getSignature().getValidityPeriod().getNotBefore())
        print "Validity period ending: " + str(CKpubCertificate.getSignature().getValidityPeriod().getNotAfter())

        tlvEncoder = TlvEncoder()
        tlvEncoder.writeBuffer(CKpubCertificate.wireEncode().buf())

        signDataWithTSKAndPutToFace(interest.getName(), sharedSecretHexString, tlvEncoder)

        deviceCertificate = CertificateV2()
        deviceCertificate.wireDecode(bytearray(scannedDeviceInfos[BKpubHash][0]))
        
        indexToDelete = check_index(onboardingDevicesList, deviceCertificate.getName().toUri())
        onboardingDevicesList.delete(indexToDelete)

        onboardedDevicesList.insert(END, CKpubCertificate.getName().toUri())

        successfullyOnboardedDevices[BKpubHash] = CKpubCertificate

        # launch the python script to register the appropriate routes to this device
        # and 
        
    def onRegisterFailed(self, prefix):
        print "Register failed for prefix" + prefix.toUri()

class AndroidPhoneHashListener(object):
    def __init__(self, keyChain, certificateName):
        self._keyChain = keyChain
        self._certificateName = certificateName

    def onInterest(self, prefix, interest, face, interestFilterId, filter):

        print "Got interest with BKpub bytes from android phone."

        BKpubBytes = bytes(interest.getName().get(-1).getValue())

        print "Hex string of BKpub bytes received: " + binascii.hexlify(BKpubBytes)

        hashPopupmsg()

        if (acceptLastHash):
            loadCertificate(bytes(BKpubBytes))
        else:
            print "Ignoring these certificate bytes since we didn't scan them."
            
    def onRegisterFailed(self, prefix):
        print "Register failed for prefix" + prefix.toUri()

        
def registerBootstrappingPrefix():
    # Also use the default certificate name to sign data packets.
    bootstrappingListener = BootstrappingListener(keyChain, keyChain.getDefaultCertificateName())
    prefix = Name("ndn/sign-on")
    print "Register prefix " + prefix.toUri()
    face.registerPrefix(prefix, bootstrappingListener.onInterest, bootstrappingListener.onRegisterFailed)

def registerCertificateRequestPrefix():
    # Also use the default certificate name to sign data packets.
    certificateRequestListener = CertificateRequestListener(keyChain, keyChain.getDefaultCertificateName())
    prefix = Name(networkPrefix).append("cert")
    print "Register prefix " + prefix.toUri()
    face.registerPrefix(prefix, certificateRequestListener.onInterest, certificateRequestListener.onRegisterFailed)

def registerAndroidPhoneHashPrefix():
    # Also use the default certificate name to sign data packets.
    androidPhoneHashListener = AndroidPhoneHashListener(keyChain, keyChain.getDefaultCertificateName())
    prefix = Name("androidPhoneHash")
    print "Register prefix " + prefix.toUri()
    face.registerPrefix(prefix, androidPhoneHashListener.onInterest, androidPhoneHashListener.onRegisterFailed)
    
def loadCertificate(rawCertificateBytes):
    
    #print "Bytes of hardcoded certificate: "
    #print (binascii.hexlify(decodedCertificateBytes))

    deviceCertificate = CertificateV2()
    deviceCertificate.wireDecode(bytearray(rawCertificateBytes))

    if (CertificateV2.isValidName(deviceCertificate.getName())):
        print "Got a certificate with a valid name, adding it to our database."
    else:
        print "Got a certificate with an invalid name, ignoring it."
        return
    
    m = hashlib.sha256()
    m.update(bytearray(deviceCertificate.getPublicKey().buf()))

    digest = binascii.hexlify(m.digest())

    print "Certificate pub key digest: " + digest

    if (check_index(pendingDevicesList, deviceCertificate.getName().toUri()) != -1
        or check_index(onboardingDevicesList, deviceCertificate.getName().toUri()) != -1
        or digest in successfullyOnboardedDevices):
        print "We already got this certificate, ignoring this one."
        return;

    pendingDevicesList.insert(END, deviceCertificate.getName().toUri())
    scannedDeviceInfos[digest] = [rawCertificateBytes, "NOTSK"]
    
    print "Identity certificate: " + deviceCertificate.getIdentity().toUri()

def check_index(listbox, element):
    try:
        index = listbox.get(0, "end").index(element)
        return index
    except ValueError:
        print'Item can not be found in the list!'
    index = -1 # Or whatever value you want to assign to it by default
    return index

def certRequestTooLatePopupmsg(BKpubHash):
    popup = Tk()
    popup.geometry("400x400")
    popup.wm_title("Got a certificate request too late")
    label = Label(popup, text="Got a certificate request from a device too long after its bootstrapping interest.\n\n BKpubhash of requesting device:\n " + BKpubHash, wraplength = 350)
    label.pack(side="top", fill="x", pady=10)
    B1 = Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()

def bootstrapDuplicatePopupmsg(BKpubHash):
    popup = Tk()
    popup.geometry("400x400")
    popup.wm_title("Bootstrapping Request from Already Onboarded Device")
    label = Label(popup, text="Got a bootstrapping request from a device we already onboarded.\n\n BKpubhash of requesting device:\n " + BKpubHash + "\n\n Certificate we distributed for this device:\n" + successfullyOnboardedDevices[BKpubHash].getName().toUri(), wraplength = 350)
    label.pack(side="top", fill="x", pady=10)
    B1 = Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()

def bootstrapTimeoutPopupmsg(BKpubHash):
    popup = Tk()
    popup.geometry("400x400")
    popup.wm_title("Bootstrapping Took Too Long")
    label = Label(popup, text="It took too long to get the certificate request from a device requesting to bootstrap.\n\n BKpubhash of requesting device:\n " + BKpubHash, wraplength = 350)
    label.pack(side="top", fill="x", pady=10)
    B1 = Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()

def bootstrapFailurePopupmsg(BKpubHash):
    popup = Tk()
    popup.geometry("400x400")
    popup.wm_title("Failed Bootstrapping Request")
    label = Label(popup, text="Got a bootstrapping request from a device we have not scanned in yet.\n\n BKpubhash of requesting device:\n " + BKpubHash, wraplength = 350)
    label.pack(side="top", fill="x", pady=10)
    B1 = Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()

def hashPopupmsg():
    popup = Tk()
    popup.geometry("400x400")
    popup.wm_title("Got BKpub Bytes from phone")
    B1 = Button(popup, text="Yes, that was me", command = lambda: acceptHash() or popup.destroy())
    B2 = Button(popup, text="I did not do that", command = lambda: declineHash() or popup.destroy())
    B1.pack()
    B2.pack()
    popup.mainloop()

def acceptHash():
    print "Accepting hash"
    global acceptLastHash
    acceptLastHash = True
                
def declineHash():
    print "Declining hash"
    global acceptLastHash
    acceptLastHash = False

if len(sys.argv) < 1:
    print "Usage:"
    print "python onboarder.py <network-prefix>"
    sys.exit()

global PUBLIC_KEY_LEN
PUBLIC_KEY_LEN = 64

global PRIVATE_KEY_LEN
PRIVATE_KEY_LEN = 32
    
global face
# The default Face will connect using a Unix socket, or to "localhost".
face = Face()

global networkPrefix
networkPrefix = str(sys.argv[1])

print "Network prefix: " + networkPrefix

global pemFilesLocation
pemFilesLocation = "pemFiles/"
        
global scannedDeviceInfos
scannedDeviceInfos = {}

global numberOfDevicesOnboarded
numberOfDevicesOnboarded = 0

global acceptLastHash
acceptLastHash = False

#with open('pi-pub.key', 'r') as myfile:
#        data=myfile.read().replace('\n', '')

#decodedCertificateBytes = bytearray(base64.b64decode(data))    
#loadCertificate(decodedCertificateBytes)

global asnPubKeyPrefix
asnPubKeyPrefix = bytearray([
  0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE,
  0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D,
  0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04
])

global keyChain
# Use the system default key chain and certificate name to sign commands.
keyChain = KeyChain()
keyChain.deleteIdentity(Name(networkPrefix))
keyChain.createIdentityV2(Name(networkPrefix), EcKeyParams())

global trustAnchorCertificate
trustAnchorCertificate = keyChain.getPib().getIdentity(Name(networkPrefix)).getDefaultKey().getDefaultCertificate()

print "Name of trust anchor certificate: " + trustAnchorCertificate.getKeyName().toUri()

face.setCommandSigningInfo(keyChain, keyChain.getDefaultCertificateName())

registerBootstrappingPrefix()
registerCertificateRequestPrefix()
registerAndroidPhoneHashPrefix()

signal.signal(signal.SIGINT, handler)

global onboardingTimers
onboardingTimers = {}

global succesfullyOnboardedDevices
successfullyOnboardedDevices = {}

global root
root = Tk()

root.title("NDN IoT Network Controller")
root.geometry("1920x1080")

app = Frame(root)
app.grid()

padding = 10
widthOfLists = 230
wrapLength = 50

global pendingDevicesList
pendingDevicesLabel = Label(app, text="Devices Pending Onboarding", width=widthOfLists)
pendingDevicesLabel.grid(row=0,column=0, padx=(padding,padding))
pendingDevicesList = Listbox(app, width=widthOfLists)
pendingDevicesList.grid(row=1,column=0, padx=(padding,padding))

global onboardingDevicesList
onboardingDevicesLabel = Label(app, text="Devices Undergoing Onboarding", width=widthOfLists)
onboardingDevicesLabel.grid(row=2,column=0, padx=(padding,padding))
onboardingDevicesList = Listbox(app, width=widthOfLists)
onboardingDevicesList.grid(row=3,column=0, padx=(padding,padding))

global onboardedDevicesList
onboardedDevicesLabel = Label(app, text="Devices Successfully Onboarded", width=widthOfLists)
onboardedDevicesLabel.grid(row=4,column=0, padx=(padding,padding))
onboardedDevicesList = Listbox(app, width=widthOfLists)
onboardedDevicesList.grid(row=5,column=0, padx=(padding,padding))

app.pack()

t = Thread(target=run_face_processing)
t.daemon = True
t.start()

def task():
    t.join(600)
    if not t.isAlive() or not uiThread.isAlive():
        global t
        t = None
        face.shutdown();
        with open('%s' % (deviceIDListName), 'a') as the_file:
            the_file.seek(0)
            the_file.truncate(0)
        exit()
    root.after(100, task)
    
#root.after(100, task)
root.mainloop()
