package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class SecurityHelpers {

    private final static String TAG = "SecurityHelpers";

    public static final int EC_PUBLIC_KEY_LENGTH = 64;
    public static final int EC_PRIVATE_KEY_LENGTH = 32;

    public static byte[] hmacSha256(byte[] key, byte[] message)
    throws NoSuchAlgorithmException, InvalidKeyException {
        Mac sha256_HMAC = null;
        byte[] mac;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
            sha256_HMAC.init(secret_key);
            mac = sha256_HMAC.doFinal(message);
        } catch (NoSuchAlgorithmException e) {
            throw e;
        } catch (InvalidKeyException e) {
            throw e;
        }

        return mac;
    }

    public static byte[] asnEncodeRawECPublicKeyBytes(byte[] rawPublicKeyBytes) throws Exception {

        if (rawPublicKeyBytes.length != 64)
            throw new Exception("Currently only asn encoding of 64 byte raw public keys is supported.");

        byte[] pubKeyASNHeader = {
                0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, (byte) 0x86, 0x48, (byte) 0xCE,
                0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, (byte) 0x86, 0x48, (byte) 0xCE, 0x3D,
                0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04
        };

        byte[] pubKeyASNEncoded = new byte[rawPublicKeyBytes.length + pubKeyASNHeader.length];
        System.arraycopy(pubKeyASNHeader, 0, pubKeyASNEncoded, 0, pubKeyASNHeader.length);
        System.arraycopy(rawPublicKeyBytes, 0, pubKeyASNEncoded, pubKeyASNHeader.length, rawPublicKeyBytes.length);

        return pubKeyASNEncoded;
    }

    // security helper function specific to dh implementation



}
