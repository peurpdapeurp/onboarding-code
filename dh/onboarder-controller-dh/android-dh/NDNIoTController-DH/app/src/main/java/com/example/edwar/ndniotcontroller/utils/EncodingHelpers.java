package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Name;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.security.PublicKey;

public class EncodingHelpers {

    private final static String TAG = "EncodingHelpers";

    public static Data constructBootstrappingResponseECDH(Name name, CertificateV2 anchorCertificate, byte[] BKpubMac, byte[] controllerTokenBytes) {

        // push these things into a data packet so that they end up in this order:
        // 1) EKpub TLV (token2 bytes) (TLV type 130)
        // 2) BKpub mac (TLV type 129)
        // 3) wire encoded trust anchor

        // sign data packet by TSK and send it out

        byte[] anchorCertificateTLV = anchorCertificate.wireEncode().getImmutableArray();

        byte[] BKpubMacTLV = new byte[BKpubMac.length + 2];
        BKpubMacTLV[0] = (byte) 129;
        BKpubMacTLV[1] = (byte) BKpubMac.length;
        System.arraycopy(BKpubMac, 0, BKpubMacTLV, 2, BKpubMac.length);

        byte[] controllerTokenTLV = new byte[controllerTokenBytes.length + 2];
        controllerTokenTLV[0] = (byte) 130;
        controllerTokenTLV[1] = (byte) controllerTokenBytes.length;
        System.arraycopy(controllerTokenBytes, 0, controllerTokenTLV, 2, controllerTokenBytes.length);

        byte[] finalContentByteArray = new byte[controllerTokenTLV.length + BKpubMacTLV.length + anchorCertificateTLV.length];
        System.arraycopy(controllerTokenTLV, 0, finalContentByteArray, 0, controllerTokenTLV.length);
        System.arraycopy(BKpubMacTLV, 0, finalContentByteArray, controllerTokenTLV.length, BKpubMacTLV.length);
        System.arraycopy(anchorCertificateTLV, 0, finalContentByteArray,
                controllerTokenTLV.length + BKpubMacTLV.length, anchorCertificateTLV.length);

        Blob content = new Blob(finalContentByteArray);

        Name dataName = new Name(name);

        dataName.appendVersion(System.currentTimeMillis());
        Data data = new Data(dataName);
        data.setContent(content);

        return data;

    }

    public static Data constructCertificateRequestResponse(Name name, CertificateV2 CKpubCertificate) {

        Data data = new Data(name);
        data.getName().appendVersion(System.currentTimeMillis());
        data.setContent(CKpubCertificate.wireEncode());

        return data;

    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

}
