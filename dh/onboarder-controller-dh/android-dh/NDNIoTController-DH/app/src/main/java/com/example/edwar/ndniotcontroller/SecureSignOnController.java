package com.example.edwar.ndniotcontroller;

import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.InterestFilter;
import net.named_data.jndn.Name;
import net.named_data.jndn.OnInterestCallback;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.VerificationHelpers;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.DHPublicKeySpec;

import static com.example.edwar.ndniotcontroller.utils.EncodingHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.OtherHelpers.*;
import static com.example.edwar.ndniotcontroller.utils.SecurityHelpers.*;


public class SecureSignOnController {

    private final String TAG = "SecureSignOnController";

    public static String INITIALIZATION_INFO = "INITIALIZATION_INFO";
    public static int INITIALIZATION_INFO_HOME_PREFIX = 0;

    private final int PUBLIC_KEY_LENGTH = 64;
    private final int PRIVATE_KEY_LENGTH = 32;

    private class DeviceInfo {
        CertificateV2 BKpubCertificate;
        byte[] tsk;
        boolean onboarded = false;
        // the variables below will only be populated once the device is finished onboarding
        CertificateV2 CKpubCertificate;
        String deviceID;
    }

    public SecureSignOnController() throws Exception {
        deviceCounter_ = 0;
        devices_ = new HashMap<>();

        try {
            sha256_ = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException exception) {
            throw new Error ("MessageDigest: SHA-256 is not supported: " + exception.getMessage());
        }

    }

    public void initialize(Face face, KeyChain keyChain, Name homePrefix, OnRegisterSuccess onRegisterSuccess, OnRegisterFailed onRegisterFailed,
                           CertificateV2 anchorCertificate) throws IOException, SecurityException, Exception {

        if (face == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null face.");
        if (homePrefix == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null home prefix.");
        if (onRegisterFailed == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null prefix registration failure callback.");
        if (onRegisterSuccess == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null prefix registration success callback.");
        if (anchorCertificate == null)
            throw new Exception("Attempted to initialize secure sign-on controller with a null anchor certificate.");

        face_ = face;
        homePrefix_ = new Name(homePrefix);
        keyChain_ = keyChain;

        face_.registerPrefix(new Name("/ndn/sign-on"), onBootstrappingInterest_, onRegisterFailed, onRegisterSuccess);
        face_.registerPrefix(homePrefix_.append("cert"), onCertificateRequestInterest_, onRegisterFailed, onRegisterSuccess);

        anchorCertificate_ = anchorCertificate;

    }

    public void addDevice(CertificateV2 BKpubCertificate) throws CertificateV2.Error {

        try {
            sha256_.update(BKpubCertificate.getPublicKey().buf());
        } catch (CertificateV2.Error error) {
            error.printStackTrace();
            throw error;
        }

        byte[] BKpubHashBytes = sha256_.digest();
        Blob BKpubHashBlob = new Blob(BKpubHashBytes);
        String BKpubHash = BKpubHashBlob.toHex();
        Log.d(TAG, "BKpubHash of device being added to secure sign on controller's device list: " + BKpubHash);

        if (!devices_.containsKey(BKpubHash)) {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.BKpubCertificate = BKpubCertificate;
            devices_.put(BKpubHash, deviceInfo);
        }

    }

    OnInterestCallback onBootstrappingInterest_ = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
            Log.d(TAG, "Got a bootstrapping interest with name: " + interest.getName().toUri());

            Name bootstrappingInterestName = interest.getName();

            String BKpubASNEncodedHash = bootstrappingInterestName.get(-4).getValue().toString();
            Log.d(TAG, "BKpubASNEncodedHash received from bootstrapping interest: " + BKpubASNEncodedHash);

            if (!devices_.containsKey(BKpubASNEncodedHash)) {
                Log.d(TAG, "Received a bootstrapping interest from a device not scanned yet. BKpubHash: " + BKpubASNEncodedHash);
                return;
            }

            DeviceInfo currentDeviceInfo = devices_.get(BKpubASNEncodedHash);

            if (!VerificationHelpers.verifyInterestSignature(interest, currentDeviceInfo.BKpubCertificate)) {
                Log.d(TAG, "Failed to verify bootstrapping interest from device. BKpubHash: " + BKpubASNEncodedHash);
                return;
            }

            byte[] deviceTokenRaw = bootstrappingInterestName.get(-3).getValue().getImmutableArray();
            Blob deviceTokenBlob = new Blob(deviceTokenRaw);
            Log.d(TAG, "Device token hex string: " + deviceTokenBlob.toHex());

            byte[] ffdhe2048PBytes = {
                    (byte) 0x00, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xAD,
                    (byte) 0xF8, (byte) 0x54, (byte) 0x58, (byte) 0xA2, (byte) 0xBB, (byte) 0x4A, (byte) 0x9A, (byte) 0xAF, (byte) 0xDC, (byte) 0x56,
                    (byte) 0x20, (byte) 0x27, (byte) 0x3D, (byte) 0x3C, (byte) 0xF1, (byte) 0xD8, (byte) 0xB9, (byte) 0xC5, (byte) 0x83, (byte) 0xCE,
                    (byte) 0x2D, (byte) 0x36, (byte) 0x95, (byte) 0xA9, (byte) 0xE1, (byte) 0x36, (byte) 0x41, (byte) 0x14, (byte) 0x64, (byte) 0x33,
                    (byte) 0xFB, (byte) 0xCC, (byte) 0x93, (byte) 0x9D, (byte) 0xCE, (byte) 0x24, (byte) 0x9B, (byte) 0x3E, (byte) 0xF9, (byte) 0x7D,
                    (byte) 0x2F, (byte) 0xE3, (byte) 0x63, (byte) 0x63, (byte) 0x0C, (byte) 0x75, (byte) 0xD8, (byte) 0xF6, (byte) 0x81, (byte) 0xB2,
                    (byte) 0x02, (byte) 0xAE, (byte) 0xC4, (byte) 0x61, (byte) 0x7A, (byte) 0xD3, (byte) 0xDF, (byte) 0x1E, (byte) 0xD5, (byte) 0xD5,
                    (byte) 0xFD, (byte) 0x65, (byte) 0x61, (byte) 0x24, (byte) 0x33, (byte) 0xF5, (byte) 0x1F, (byte) 0x5F, (byte) 0x06, (byte) 0x6E,
                    (byte) 0xD0, (byte) 0x85, (byte) 0x63, (byte) 0x65, (byte) 0x55, (byte) 0x3D, (byte) 0xED, (byte) 0x1A, (byte) 0xF3, (byte) 0xB5,
                    (byte) 0x57, (byte) 0x13, (byte) 0x5E, (byte) 0x7F, (byte) 0x57, (byte) 0xC9, (byte) 0x35, (byte) 0x98, (byte) 0x4F, (byte) 0x0C,
                    (byte) 0x70, (byte) 0xE0, (byte) 0xE6, (byte) 0x8B, (byte) 0x77, (byte) 0xE2, (byte) 0xA6, (byte) 0x89, (byte) 0xDA, (byte) 0xF3,
                    (byte) 0xEF, (byte) 0xE8, (byte) 0x72, (byte) 0x1D, (byte) 0xF1, (byte) 0x58, (byte) 0xA1, (byte) 0x36, (byte) 0xAD, (byte) 0xE7,
                    (byte) 0x35, (byte) 0x30, (byte) 0xAC, (byte) 0xCA, (byte) 0x4F, (byte) 0x48, (byte) 0x3A, (byte) 0x79, (byte) 0x7A, (byte) 0xBC,
                    (byte) 0x0A, (byte) 0xB1, (byte) 0x82, (byte) 0xB3, (byte) 0x24, (byte) 0xFB, (byte) 0x61, (byte) 0xD1, (byte) 0x08, (byte) 0xA9,
                    (byte) 0x4B, (byte) 0xB2, (byte) 0xC8, (byte) 0xE3, (byte) 0xFB, (byte) 0xB9, (byte) 0x6A, (byte) 0xDA, (byte) 0xB7, (byte) 0x60,
                    (byte) 0xD7, (byte) 0xF4, (byte) 0x68, (byte) 0x1D, (byte) 0x4F, (byte) 0x42, (byte) 0xA3, (byte) 0xDE, (byte) 0x39, (byte) 0x4D,
                    (byte) 0xF4, (byte) 0xAE, (byte) 0x56, (byte) 0xED, (byte) 0xE7, (byte) 0x63, (byte) 0x72, (byte) 0xBB, (byte) 0x19, (byte) 0x0B,
                    (byte) 0x07, (byte) 0xA7, (byte) 0xC8, (byte) 0xEE, (byte) 0x0A, (byte) 0x6D, (byte) 0x70, (byte) 0x9E, (byte) 0x02, (byte) 0xFC,
                    (byte) 0xE1, (byte) 0xCD, (byte) 0xF7, (byte) 0xE2, (byte) 0xEC, (byte) 0xC0, (byte) 0x34, (byte) 0x04, (byte) 0xCD, (byte) 0x28,
                    (byte) 0x34, (byte) 0x2F, (byte) 0x61, (byte) 0x91, (byte) 0x72, (byte) 0xFE, (byte) 0x9C, (byte) 0xE9, (byte) 0x85, (byte) 0x83,
                    (byte) 0xFF, (byte) 0x8E, (byte) 0x4F, (byte) 0x12, (byte) 0x32, (byte) 0xEE, (byte) 0xF2, (byte) 0x81, (byte) 0x83, (byte) 0xC3,
                    (byte) 0xFE, (byte) 0x3B, (byte) 0x1B, (byte) 0x4C, (byte) 0x6F, (byte) 0xAD, (byte) 0x73, (byte) 0x3B, (byte) 0xB5, (byte) 0xFC,
                    (byte) 0xBC, (byte) 0x2E, (byte) 0xC2, (byte) 0x20, (byte) 0x05, (byte) 0xC5, (byte) 0x8E, (byte) 0xF1, (byte) 0x83, (byte) 0x7D,
                    (byte) 0x16, (byte) 0x83, (byte) 0xB2, (byte) 0xC6, (byte) 0xF3, (byte) 0x4A, (byte) 0x26, (byte) 0xC1, (byte) 0xB2, (byte) 0xEF,
                    (byte) 0xFA, (byte) 0x88, (byte) 0x6B, (byte) 0x42, (byte) 0x38, (byte) 0x61, (byte) 0x28, (byte) 0x5C, (byte) 0x97, (byte) 0xFF,
                    (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
            };

            byte[] ffdhe2048GBytes = { 0x02 };

            BigInteger P = new BigInteger(ffdhe2048PBytes);
            BigInteger G = new BigInteger(ffdhe2048GBytes);

            byte[] sharedSecret = null;
            DHPublicKey controllerTokenKey = null;
            try {
                DHParameterSpec dhParams = new DHParameterSpec(P, G);
                KeyPairGenerator controllerDHKeyPairGenerator = KeyPairGenerator.getInstance("DH");
                controllerDHKeyPairGenerator.initialize(dhParams);
                KeyPair controllerKeyPair = controllerDHKeyPairGenerator.generateKeyPair();
                controllerTokenKey = (DHPublicKey) controllerKeyPair.getPublic();
                KeyAgreement controllerKeyAgreement = KeyAgreement.getInstance("DH");
                controllerKeyAgreement.init(controllerKeyPair.getPrivate(), dhParams);
                BigInteger deviceToken = new BigInteger(deviceTokenRaw);
                DHPublicKeySpec dhPublicKeySpec = new DHPublicKeySpec(deviceToken, dhParams.getP(), dhParams.getG());
                KeyFactory keyFactory = KeyFactory.getInstance("DH");
                DHPublicKey deviceDHPublicKey = (DHPublicKey) keyFactory.generatePublic(dhPublicKeySpec);
                controllerKeyAgreement.doPhase(deviceDHPublicKey, true);
                sharedSecret = controllerKeyAgreement.generateSecret();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }

            Blob controllerTokenBlob = new Blob(controllerTokenKey.getY().toByteArray());
            Log.d(TAG, "Controller token hex string: " + controllerTokenBlob.toHex());

            Blob sharedSecretBlob = new Blob(sharedSecret);
            Log.d(TAG, "Shared secret derived by controller: " + sharedSecretBlob.toHex());

            currentDeviceInfo.tsk = Arrays.copyOf(sharedSecret, sharedSecret.length);

            byte[] BKpubASNEncodedBytes = null;
            try {
                BKpubASNEncodedBytes = currentDeviceInfo.BKpubCertificate.getPublicKey().getImmutableArray();
            } catch (CertificateV2.Error error) {
                error.printStackTrace();
                return;
            }
            byte[] BKpubRawBytes = new byte[64];
            System.arraycopy(BKpubASNEncodedBytes, 27, BKpubRawBytes, 0, 64);

            byte[] macOfBKpubASNEncodedByControllerToken = new byte[0];
            try {
                macOfBKpubASNEncodedByControllerToken = hmacSha256(controllerTokenKey.getEncoded(), BKpubRawBytes);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return;
            } catch (InvalidKeyException e) {
                e.printStackTrace();
                return;
            }

            Blob macOfBKpubASNEncodedByControllerTokenBlob = new Blob(macOfBKpubASNEncodedByControllerToken);
            Log.d(TAG, "Hex of MAC of BKpubASNEncoded by DKpub: " + macOfBKpubASNEncodedByControllerTokenBlob.toHex());

            Data bootstrappingResponse = constructBootstrappingResponseECDH(interest.getName(), anchorCertificate_,
                    macOfBKpubASNEncodedByControllerToken, controllerTokenKey.getY().toByteArray());

            signDataBySymmetricKeyAndPublish(currentDeviceInfo.tsk, face_, bootstrappingResponse);

        }
    };
    OnInterestCallback onCertificateRequestInterest_ = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId, InterestFilter filter) {
            /*
            Log.d(TAG, "Got a certificate request interest with name: " + interest.getName().toUri());

            Name certificateRequestInterestName = interest.getName();

            String BKpubHash = certificateRequestInterestName.get(-4).getValue().toString();

            if (!devices_.containsKey(BKpubHash)) {
                Log.d(TAG, "Got certificate request from device we did not recognize. BKpubHash: " + BKpubHash);
                return;
            }

            DeviceInfo currentDeviceInfo = devices_.get(BKpubHash);

            if (!KeyChain.verifyInterestWithHmacWithSha256(interest, new Blob(currentDeviceInfo.tsk))) {
                Log.d(TAG, "Failed to verify certificate request by tsk.");
                return;
            }

            // get CKpub bytes from the proper interest name component
            byte[] CKpubRaw = certificateRequestInterestName.get(-3).getValue().getImmutableArray();

            long currentTime = System.currentTimeMillis();

            CertificateV2 CKpubCertificate = new CertificateV2();
            CKpubCertificate.setName(homePrefix_);
            CKpubCertificate.getName().append("DEVICE" + deviceCounter_);
            CKpubCertificate.getName().append("KEY");
            CKpubCertificate.getName().append(Name.Component.fromTimestamp(currentTime));
            try {
                CKpubCertificate.setContent(new Blob(asnEncodeRawECPublicKeyBytes(CKpubRaw)));
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            // set validity period of certificate to ten days
            CKpubCertificate.getValidityPeriod().setPeriod(currentTime, currentTime + 1000 * 60 * 60 * 24 * 10);

            try {
                keyChain_.sign(CKpubCertificate, keyChain_.getDefaultCertificateName());
            } catch (SecurityException e) {
                e.printStackTrace();
                return;
            }

            if (!VerificationHelpers.verifyDataSignature(CKpubCertificate, anchorCertificate_)) {
                Log.d(TAG, "Failed to verify the CKpub certificate we generated using the anchor certificate.");
                return;
            }

            Data certificateRequestResponse = constructCertificateRequestResponse(interest.getName(), CKpubCertificate);

            signDataBySymmetricKeyAndPublish(currentDeviceInfo.tsk, face_, certificateRequestResponse);
            */
        }
    };

    private Face face_;
    private Name homePrefix_;
    private CertificateV2 anchorCertificate_;
    private KeyChain keyChain_;
    private HashMap<String, DeviceInfo> devices_;
    private MessageDigest sha256_;
    private long deviceCounter_;


}