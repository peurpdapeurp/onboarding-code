#include "connectivity.hpp"

#include <WiFi.h>

#include "credentials.hpp"
#include "logger.hpp"
#include "consts.hpp"

#define LOG(...) LOGGER(Connectivity, __VA_ARGS__)

ConnectivityClass::ConnectivityClass()
  : m_transport()
  , m_face(m_transport)
  , m_topName(m_topComps, TOP_PREFIX_NCOMPS)
  , m_pingName(m_pingComps, TOP_PREFIX_NCOMPS + 1)
  , m_pingServer(m_face, m_pingName)
{
}

void
ConnectivityClass::begin()
{
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.config(IP_CLIENT, IP_SERVER, IP_SUBNET);
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  int timeout = 15000;
  while (WiFi.status() != WL_CONNECTED) {
    if (--timeout < 0) {
      LOG("WiFi connection timeout, rebooting...");
      ESP.restart();
    }
    delay(1);
  }

  LOG("WiFi ready");

  m_transport.beginTunnel(IP_SERVER, UDP_PORT, UDP_PORT);

  static ndn::DigestKey key;
  m_face.setSigningKey(key);

  m_topName.append("localhop");
  m_topName.append("sensors");
  LOG("NDN top-level name is " << ndn::PrintUri(m_topName));

  m_pingName.set(m_topName);
  m_pingName.append("ping");
  LOG("ndnpingserver is at " << ndn::PrintUri(m_pingName));
}

IPAddress
ConnectivityClass::localIP()
{
  return WiFi.localIP();
}

void
ConnectivityClass::loop()
{
  if (WiFi.status() != WL_CONNECTED) {
    LOG("WiFi connection lost, rebooting...");
    delay(1000);
    ESP.restart();
  }

  m_face.loop();
}

ConnectivityClass Connectivity;
