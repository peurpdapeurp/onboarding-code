#ifndef LOGGER_HPP
#define LOGGER_HPP

#include "consts.hpp"

#include <Streaming.h>
#include <PriUint64.h>

#define LOGGER(module, ...) \
  do { \
    LOG_OUTPUT << _DEC(millis()) << " [" #module "] " << __VA_ARGS__ << "\n"; \
  } while (false)

//  #define LOGGER(module, ...)

#endif // LOGGER_HPP
