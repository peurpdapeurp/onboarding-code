
#include "other-helpers.hpp"

#include <Arduino.h>

void
putInCharBuf(char *buf, int pos, uint8_t X) {

  String stringOne =  String(X, HEX);

  if (X < 16) {
    buf[pos] = '0';
    buf[pos + 1] = stringOne[0];
  }
  else {
    buf[pos] = stringOne[0];
    buf[pos + 1] = stringOne[1];
  }

}