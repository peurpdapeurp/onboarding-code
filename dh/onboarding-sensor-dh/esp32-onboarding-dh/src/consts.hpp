#ifndef CONSTS_HPP
#define CONSTS_HPP

#include <cstddef>
#include <stdint.h>

#define LOG_OUTPUT Serial

#define PIN_BTN 0

#define IP_CLIENT IPAddress(192, 168, 137, 212)
#define IP_SERVER IPAddress(192, 168, 137, 243)
#define IP_SUBNET IPAddress(255, 255, 255, 0)
#define UDP_PORT 6363

#ifdef DEBUG
#undef DEBUG
#endif
#define DEBUG 1

// number of components in top prefix
// ndn:/localhop/sensors/SENSOR-ID
constexpr int TOP_PREFIX_NCOMPS = 3;

// maximum number of name components that we will process
constexpr int MAX_NAME_COMPS = 15;

constexpr uint8_t CONTROLLER_TOKEN_TLV_TYPE = 0x82;
constexpr uint8_t BK_PUB_MAC_TLV_TYPE = 0x81;
constexpr uint8_t DATA_TLV_TYPE = 0x06;
constexpr uint8_t NAME_TLV_TYPE = 0x07;
constexpr uint8_t METAINFO_TLV_TYPE = 0x14;
constexpr uint8_t CONTENT_TLV_TYPE = 0x15;



#endif // CONSTS_HPP
