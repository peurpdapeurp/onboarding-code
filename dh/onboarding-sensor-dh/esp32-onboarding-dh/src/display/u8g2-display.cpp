
#include "u8g2-display.hpp"
#include <Streaming.h>

U8G2Display::U8G2Display()
: u8g2(U8G2_R0, 15, 4, 16)
{
  m_currentLine = 0;
  m_lineSpacing = 11;
}

U8G2Display::~U8G2Display()
{
}

bool U8G2Display::begin() {
  u8g2.begin();
  u8g2.setFont(u8g2_font_helvR08_tf);
  return true;
}

bool U8G2Display::addLine(const char* message) {
  if (m_currentLine == 0) {
    u8g2.clearBuffer();
  }
  
  u8g2.setCursor(0, m_lineSpacing*(m_currentLine + 1));
  u8g2 << message;
  
  m_currentLine++;
}

bool U8G2Display::addLine(const ndn::NameLite &name) {
  if (m_currentLine == 0) {
    u8g2.clearBuffer();
  }
  u8g2.setCursor(0, m_lineSpacing*(m_currentLine + 1));
  u8g2 << ndn::PrintUri(name);

  m_currentLine++;
}

bool U8G2Display::display() {
  u8g2.sendBuffer();
  m_currentLine = 0;
}