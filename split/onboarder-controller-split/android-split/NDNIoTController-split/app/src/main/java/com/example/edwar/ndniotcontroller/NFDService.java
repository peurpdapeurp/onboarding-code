package com.example.edwar.ndniotcontroller;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;

import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.Name;
import net.named_data.jndn.NetworkNack;
import net.named_data.jndn.OnData;
import net.named_data.jndn.OnNetworkNack;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.OnTimeout;
import net.named_data.jndn.encoding.EncodingException;
import net.named_data.jndn.encoding.WireFormat;
import net.named_data.jndn.security.EcKeyParams;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.KeyParams;
import net.named_data.jndn.security.KeyType;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.pib.Pib;
import net.named_data.jndn.security.pib.PibIdentity;
import net.named_data.jndn.security.pib.PibImpl;
import net.named_data.jndn.security.pib.PibKey;
import net.named_data.jndn.security.tpm.Tpm;
import net.named_data.jndn.security.tpm.TpmBackEnd;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.io.IOException;


public class NFDService extends Service {

    private static String TAG = "NFDService";

    private final String NETWORK_THREAD_STOPPED = "NETWORK_THREAD_STOPPED";

    private final IBinder mBinder = new NFDService.LocalBinder();

    private Face m_face;
    private KeyChain m_keyChain;
    private PibIdentity m_myIdentity;
    private boolean m_networkThreadShouldStop = false;
    private Thread m_networkThread;
    private Thread m_heartbeatInterestThread;

    private CertificateV2 m_anchorCertificate;
    private Name m_homePrefix;
    public SecureSignOnController m_secureSignOnController;

    /*
    // will temporary hardcode the certificate for now, as deriving EC public keys using
    // createIdentityV2 is not implemented in jndn yet
    CertificateV2 HARDCODED_ANCHOR_CERTIFICATE;

    byte[] hardcodedAnchorCertificateBytes = {
            0x06, (byte) 0xfd, 0x01, 0x44, 0x07, 0x37, 0x08, 0x07, 0x64, 0x65,
            0x66, 0x61, 0x75, 0x6c, 0x74, 0x08, 0x04, 0x68, 0x6f, 0x6d,
            0x65, 0x08, 0x06, 0x70, 0x72, 0x65, 0x66, 0x69, 0x78, 0x08,
            0x03, 0x4b, 0x45, 0x59, 0x08, 0x08, (byte) 0xd1, (byte) 0xb6, 0x00, (byte) 0x92,
            0x71, (byte) 0xd9, 0x7d, 0x08, 0x08, 0x04, 0x73, 0x65, 0x6c, 0x66,
            0x08, 0x09, (byte) 0xfd, 0x00, 0x00, 0x01, 0x66, 0x0b, (byte) 0xa8, (byte) 0xbf,
            0x71, 0x14, 0x09, 0x18, 0x01, 0x02, 0x19, 0x04, 0x00, 0x36,
            (byte) 0xee, (byte) 0x80, 0x15, 0x5b, 0x30, 0x59, 0x30, 0x13, 0x06, 0x07,
            0x2a, (byte) 0x86, 0x48, (byte) 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08, 0x2a,
            (byte) 0x86, 0x48, (byte) 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00,
            0x04, 0x4d, (byte) 0xb5, (byte) 0x99, (byte) 0xe4, 0x06, 0x35, 0x6b, (byte) 0xa4, 0x05,
            0x05, 0x27, (byte) 0xb2, (byte) 0xe3, (byte) 0xb4, 0x52, (byte) 0xe7, 0x2a, (byte) 0xb9, 0x43,
            (byte) 0x97, 0x77, (byte) 0xb4, 0x4c, 0x6e, 0x14, 0x23, 0x0b, (byte) 0xa6, 0x4c,
            (byte) 0xc9, 0x4c, 0x0e, 0x5b, 0x6a, (byte) 0x84, 0x38, (byte) 0xf3, 0x0b, 0x08,
            (byte) 0xea, 0x37, 0x72, 0x59, 0x08, (byte) 0xb5, (byte) 0x85, 0x6f, (byte) 0xe4, (byte) 0xd3,
            0x4c, 0x5a, 0x3b, 0x77, (byte) 0xa0, (byte) 0xdb, (byte) 0xb9, (byte) 0xf1, (byte) 0xb3, (byte) 0xd9,
            0x0d, (byte) 0x95, (byte) 0xc0, 0x6a, (byte) 0xc4, 0x16, 0x57, 0x1b, 0x01, 0x03,
            0x1c, 0x28, 0x07, 0x26, 0x08, 0x07, 0x64, 0x65, 0x66, 0x61,
            0x75, 0x6c, 0x74, 0x08, 0x04, 0x68, 0x6f, 0x6d, 0x65, 0x08,
            0x06, 0x70, 0x72, 0x65, 0x66, 0x69, 0x78, 0x08, 0x03, 0x4b,
            0x45, 0x59, 0x08, 0x08, (byte) 0xd1, (byte) 0xb6, 0x00, (byte) 0x92, 0x71, (byte) 0xd9,
            0x7d, 0x08, (byte) 0xfd, 0x00, (byte) 0xfd, 0x26, (byte) 0xfd, 0x00, (byte) 0xfe, 0x0f,
            0x32, 0x30, 0x31, 0x38, 0x30, 0x39, 0x32, 0x34, 0x54, 0x31,
            0x32, 0x35, 0x38, 0x32, 0x31, (byte) 0xfd, 0x00, (byte) 0xff, 0x0f, 0x32,
            0x30, 0x33, 0x38, 0x30, 0x39, 0x31, 0x39, 0x54, 0x31, 0x32,
            0x35, 0x38, 0x32, 0x30, 0x17, 0x48, 0x30, 0x46, 0x02, 0x21,
            0x00, (byte) 0xc7, 0x70, (byte) 0xd3, (byte) 0xa5, 0x33, (byte) 0xff, 0x3a, (byte) 0xab, (byte) 0xd4,
            (byte) 0xf6, 0x30, 0x72, (byte) 0xaa, (byte) 0x85, 0x58, 0x78, (byte) 0xdf, 0x1e, 0x5f,
            0x57, (byte) 0xdd, (byte) 0x91, (byte) 0xca, 0x58, (byte) 0xd1, (byte) 0xfd, 0x09, 0x47, (byte) 0xaf,
            (byte) 0xed, 0x54, 0x72, 0x02, 0x21, 0x00, (byte) 0xd5, 0x35, 0x3c, (byte) 0x8e,
            (byte) 0x89, 0x06, (byte) 0xa0, 0x03, (byte) 0x9f, (byte) 0xe1, (byte) 0xb7, 0x58, (byte) 0x85, (byte) 0xcd,
            0x34, (byte) 0xe4, (byte) 0xc2, (byte) 0xe8, 0x5c, 0x0e, 0x6c, (byte) 0xe8, 0x41, 0x67,
            (byte) 0xd5, 0x07, (byte) 0xe2, 0x45, (byte) 0xf5, (byte) 0xc8, (byte) 0x92, (byte) 0xe4
    };
    */

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        m_homePrefix = new Name(intent.getExtras().getStringArray(
                SecureSignOnController.INITIALIZATION_INFO)[SecureSignOnController.INITIALIZATION_INFO_HOME_PREFIX]);

        return mBinder;
    }

    public class LocalBinder extends Binder {
        NFDService getService() {
            return NFDService.this;
        }
    }

    private void initializeKeyChain() {

        try {
            m_keyChain = new KeyChain("pib-memory:", "tpm-memory:");
        } catch (KeyChain.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        m_keyChain.setFace(m_face);
        m_myIdentity = null;
        try {
            m_myIdentity = m_keyChain.createIdentityV2(new Name(m_homePrefix));
            /*
            m_keyChain.setDefaultIdentity(m_myIdentity);
            PibKey newKey = m_keyChain.createKey(m_myIdentity);
            HARDCODED_ANCHOR_CERTIFICATE.setName(newKey.getDefaultCertificate().getName());
            m_keyChain.addCertificate(newKey, HARDCODED_ANCHOR_CERTIFICATE);
            m_keyChain.setDefaultKey(m_myIdentity, newKey);
            m_keyChain.setDefaultKeyForIdentity(newKey.getName());
            Log.d(TAG, "Name of default certificate for key chain: " + m_keyChain.getDefaultCertificateName());
            */
        } catch (Pib.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        } catch (Tpm.Error error) {
            error.printStackTrace();
        } catch (TpmBackEnd.Error error) {
            error.printStackTrace();
        } catch (KeyChain.Error error) {
            error.printStackTrace();
        }

        try {
            m_anchorCertificate = m_myIdentity.getDefaultKey().getDefaultCertificate();
        } catch (Pib.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        }
    }

    private void setCommandSigningInfo() {
        Name defaultCertificateName;
        try {
            defaultCertificateName = m_keyChain.getDefaultCertificateName();
        } catch (SecurityException e) {
            Name testIdName = new Name("/iot-controller");
            try {
                defaultCertificateName = m_keyChain.createIdentityAndCertificate(testIdName);
                m_keyChain.getIdentityManager().setDefaultIdentity(testIdName);
            } catch (SecurityException e2) {
                defaultCertificateName = new Name("/bogus/certificate/name");
            }
        }
        m_face.setCommandSigningInfo(m_keyChain, defaultCertificateName);
    }

    BroadcastReceiver networkThreadStoppedListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            createAndStartNewNetworkThread();
        }
    };

    public boolean initialize() {

        m_face = new Face("localhost");

        LocalBroadcastManager.getInstance(this).registerReceiver(networkThreadStoppedListener, new IntentFilter(NETWORK_THREAD_STOPPED));

        try {
            m_secureSignOnController = new SecureSignOnController();
        } catch (Exception e) {
            Log.e(TAG, "Failed to start secure sign-on controller: " + e.getMessage());
            e.printStackTrace();
            return false;
        }

        /*
        HARDCODED_ANCHOR_CERTIFICATE = null;

        Data tempData = new Data();

        try {
            tempData.wireDecode(new Blob(hardcodedAnchorCertificateBytes));
        } catch (EncodingException e) {
            Log.e(TAG, "Failed to decode hardcoded anchor certificate: " + e.getMessage());
            e.printStackTrace();
            return false;
        }

        try {
            HARDCODED_ANCHOR_CERTIFICATE = new CertificateV2(tempData);
        } catch (CertificateV2.Error error) {
            Log.e(TAG, "Failed to turn hardcoded anchor certificate from data packet to certificatev2:" + error.getMessage());
            error.printStackTrace();
            return false;
        }

        Log.d(TAG, "Name of hardcoded anchor certificate:" + HARDCODED_ANCHOR_CERTIFICATE.getName().toUri());
        */

        initializeKeyChain();

        createAndStartNewNetworkThread();

        return true;

    }

    private void createAndStartNewNetworkThread() {
        m_networkThread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    if (m_face == null)
                        m_face = new Face(getString(R.string.nfd_address));

                    setCommandSigningInfo();

                } catch (Exception e) {

                    e.printStackTrace();

                }

                createAndStartNewHeartbeatInterestThread();

                try {
                    m_secureSignOnController.initialize(m_face, m_keyChain, m_homePrefix,
                            onRegisterSuccess, onRegisterFailed,
                            m_anchorCertificate);
                } catch (IOException e) {
                    Log.w(TAG, "Error initializing secure sign on controller: " + e.getMessage());
                    e.printStackTrace();
                    stopNetworkThread();
                } catch (SecurityException e) {
                    Log.w(TAG, "Error initializing secure sign on controller: " + e.getMessage());
                    e.printStackTrace();
                    stopNetworkThread();
                } catch (Exception e) {
                    Log.w(TAG, "Error initializing secure sign on controller: " + e.getMessage());
                    e.printStackTrace();
                    stopNetworkThread();
                }

                while (!m_networkThreadShouldStop) {
                    try {
                        m_face.processEvents();
                        Thread.sleep(100); // avoid hammering the CPU
                    } catch (IOException e) {
                        Log.w(TAG, "Error in processEvents loop: " + e.getMessage());
                        e.printStackTrace();
                        stopNetworkThread();
                    } catch (Exception e) {
                        Log.w(TAG, "Error in processEvents loop: " + e.getMessage());
                        e.printStackTrace();
                        stopNetworkThread();
                    }
                }
                doFinalCleanup();
                Log.d(TAG, "Network thread stopped.");
                LocalBroadcastManager.getInstance(NFDService.this).sendBroadcast(new Intent(NETWORK_THREAD_STOPPED));
            }
        });
        m_networkThreadShouldStop = false;
        m_networkThread.start();
    }

    private void createAndStartNewHeartbeatInterestThread() {
        // thread to periodically send localhop interests to NFD-Android to get notified of failures quickly
        m_heartbeatInterestThread = new Thread(new Runnable() {

            public void run() {
                while (!m_networkThreadShouldStop) {
                    try {
                        m_face.expressInterest(new Name("localhost/heartbeat"), onData, onTimeout, onNack);
                    } catch (IOException e) {
                        stopNetworkThread();
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        });

        m_heartbeatInterestThread.start();
    }

    private OnData onData = new OnData() {
        @Override
        public void onData(Interest interest, Data data) {

        }
    };

    private OnTimeout onTimeout = new OnTimeout() {
        @Override
        public void onTimeout(Interest interest) {

        }
    };

    private OnNetworkNack onNack = new OnNetworkNack() {
        @Override
        public void onNetworkNack(Interest interest, NetworkNack networkNack) {

        }
    };

    private final OnRegisterSuccess onRegisterSuccess = new OnRegisterSuccess() {
        @Override
        public void onRegisterSuccess(Name prefix, long registeredPrefixId) {
            Log.d(TAG, "Successfully register prefix: " + prefix);
        }
    };

    private final OnRegisterFailed onRegisterFailed = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            Log.d(TAG, "Failed to register prefix: " + prefix);
        }
    };

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();


        return filter;
    }

    private void stopNetworkThread() {
        m_networkThreadShouldStop = true;

    }

    private void doFinalCleanup() {
        if (m_face != null) m_face.shutdown();
        m_face = null;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(networkThreadStoppedListener);

        super.onDestroy();
        m_face.shutdown();
        m_networkThread.interrupt();
    }

}