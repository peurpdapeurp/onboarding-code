package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import org.apache.commons.lang3.ArrayUtils;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static com.example.edwar.ndniotcontroller.utils.LongToByteArrayUtils.*;
public class SecurityHelpers {

    private final static String TAG = "SecurityHelpers";
    public final static long DH_P = 10000831;
    public final static long DH_G = 10000769;

    public static byte[] hmacSha256(byte[] key, byte[] message)
    throws NoSuchAlgorithmException, InvalidKeyException {
        Mac sha256_HMAC = null;
        byte[] mac;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
            sha256_HMAC.init(secret_key);
            mac = sha256_HMAC.doFinal(message);
        } catch (NoSuchAlgorithmException e) {
            throw e;
        } catch (InvalidKeyException e) {
            throw e;
        }

        return mac;
    }

    public static byte[] asnEncodeRawECPublicKeyBytes(byte[] rawPublicKeyBytes) throws Exception {

        if (rawPublicKeyBytes.length != 64)
            throw new Exception("Currently only asn encoding of 64 byte raw public keys is supported.");

        byte[] pubKeyASNHeader = {
                0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, (byte) 0x86, 0x48, (byte) 0xCE,
                0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, (byte) 0x86, 0x48, (byte) 0xCE, 0x3D,
                0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04
        };

        byte[] pubKeyASNEncoded = new byte[rawPublicKeyBytes.length + pubKeyASNHeader.length];
        System.arraycopy(pubKeyASNHeader, 0, pubKeyASNEncoded, 0, pubKeyASNHeader.length);
        System.arraycopy(rawPublicKeyBytes, 0, pubKeyASNEncoded, pubKeyASNHeader.length, rawPublicKeyBytes.length);

        return pubKeyASNEncoded;
    }

    // security helper functions specific to split implementation

    public static long[] generateControllerToken(long[] controllerSecret) {

        long[] controllerToken = new long[4];

        controllerToken[0] = Montgomery(DH_G, controllerSecret[0], DH_P);
        controllerToken[1] = Montgomery(DH_G, controllerSecret[1], DH_P);
        controllerToken[2] = Montgomery(DH_G, controllerSecret[2], DH_P);
        controllerToken[3] = Montgomery(DH_G, controllerSecret[3], DH_P);

        return controllerToken;
    }

    public static byte[] deriveSharedSecretSplit(long[] controllerSecret, byte[] deviceTokenRawBytes) {

        long[] deviceToken = new long[4];
        byte[] deviceTokenBytes0 = new byte[8];
        System.arraycopy(deviceTokenRawBytes, 0, deviceTokenBytes0, 0, 8);
        ArrayUtils.reverse(deviceTokenBytes0);
        byte[] deviceTokenBytes1 = new byte[8];
        System.arraycopy(deviceTokenRawBytes, 8, deviceTokenBytes1, 0, 8);
        ArrayUtils.reverse(deviceTokenBytes1);
        byte[] deviceTokenBytes2 = new byte[8];
        System.arraycopy(deviceTokenRawBytes, 16, deviceTokenBytes2, 0, 8);
        ArrayUtils.reverse(deviceTokenBytes2);
        byte[] deviceTokenBytes3 = new byte[8];
        System.arraycopy(deviceTokenRawBytes, 24, deviceTokenBytes3, 0, 8);
        ArrayUtils.reverse(deviceTokenBytes3);
        deviceToken[0] = bytesToLong(deviceTokenBytes0);
        deviceToken[1] = bytesToLong(deviceTokenBytes1);
        deviceToken[2] = bytesToLong(deviceTokenBytes2);
        deviceToken[3] = bytesToLong(deviceTokenBytes3);

        Log.d(TAG, "Device token integer 0: " + deviceToken[0]);
        Log.d(TAG, "Device token integer 1: " + deviceToken[1]);
        Log.d(TAG, "Device token integer 2: " + deviceToken[2]);
        Log.d(TAG, "Device token integer 3: " + deviceToken[3]);

        long[] sharedSecret = new long[4];

        sharedSecret[0] = Montgomery(deviceToken[0], controllerSecret[0], DH_P);
        sharedSecret[1] = Montgomery(deviceToken[1], controllerSecret[1], DH_P);
        sharedSecret[2] = Montgomery(deviceToken[2], controllerSecret[2], DH_P);
        sharedSecret[3] = Montgomery(deviceToken[3], controllerSecret[3], DH_P);

        byte[] sharedSecretBytes = new byte[32];

        byte[] sharedSecretByteArray0 = longToBytes(sharedSecret[0]);
        ArrayUtils.reverse(sharedSecretByteArray0);
        byte[] sharedSecretByteArray1 = longToBytes(sharedSecret[1]);
        ArrayUtils.reverse(sharedSecretByteArray1);
        byte[] sharedSecretByteArray2 = longToBytes(sharedSecret[2]);
        ArrayUtils.reverse(sharedSecretByteArray2);
        byte[] sharedSecretByteArray3 = longToBytes(sharedSecret[3]);
        ArrayUtils.reverse(sharedSecretByteArray3);

        System.arraycopy(sharedSecretByteArray0, 0, sharedSecretBytes, 0, 8);
        System.arraycopy(sharedSecretByteArray1, 0, sharedSecretBytes, 8, 8);
        System.arraycopy(sharedSecretByteArray2, 0, sharedSecretBytes, 16, 8);
        System.arraycopy(sharedSecretByteArray3, 0, sharedSecretBytes, 24, 8);

        return sharedSecretBytes;

    }

    public static long Montgomery(long n, long p, long m)
    {
        long r = n % m;
        long tmp = 1;
        while (p > 1)
        {
            if ((p & 1)!=0)
            {
                tmp = (tmp * r) % m;
            }
            r = (r * r) % m;
            p >>= 1;
        }
        return (r * tmp) % m;
    }

}
