package com.example.edwar.ndniotcontroller.utils;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class LongToByteArrayUtils {

    private static ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);

    public static byte[] longToBytes(long x) {
        buffer.clear();
        buffer.putLong(0, x);
        byte[] copiedBufferArray = Arrays.copyOf(buffer.array(), 8);
        return copiedBufferArray;
    }

    public static long bytesToLong(byte[] bytes) {
        buffer.clear();
        buffer.put(bytes, 0, bytes.length);
        buffer.flip();//need flip
        return buffer.getLong();
    }

}
