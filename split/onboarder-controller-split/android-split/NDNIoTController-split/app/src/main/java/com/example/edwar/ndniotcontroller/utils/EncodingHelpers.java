package com.example.edwar.ndniotcontroller.utils;

import android.util.Log;

import net.named_data.jndn.Data;
import net.named_data.jndn.Name;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import org.apache.commons.lang3.ArrayUtils;

import java.security.PublicKey;
import java.util.Arrays;

import static com.example.edwar.ndniotcontroller.utils.LongToByteArrayUtils.*;

public class EncodingHelpers {

    private final static String TAG = "EncodingHelpers";

    public static Data constructBootstrappingResponseSplit(Name name, CertificateV2 anchorCertificate,
                                                           byte[] BKpubMac, byte[] controllerTokenBytes) {

        // push these things into a data packet so that they end up in this order:
        // 1) EKpub TLV (token2 bytes) (TLV type 130)
        // 2) BKpub mac (TLV type 129)
        // 3) wire encoded trust anchor

        // sign data packet by TSK and send it out

        byte[] anchorCertificateTLV = anchorCertificate.wireEncode().getImmutableArray();

        byte[] BKpubMacTLV = new byte[BKpubMac.length + 2];
        BKpubMacTLV[0] = (byte) 129;
        BKpubMacTLV[1] = (byte) BKpubMac.length;
        System.arraycopy(BKpubMac, 0, BKpubMacTLV, 2, BKpubMac.length);

        byte[] controllerTokenTLV = new byte[controllerTokenBytes.length + 2];
        controllerTokenTLV[0] = (byte) 130;
        controllerTokenTLV[1] = (byte) controllerTokenBytes.length;
        System.arraycopy(controllerTokenBytes, 0, controllerTokenTLV, 2, controllerTokenBytes.length);

        byte[] finalContentByteArray = new byte[controllerTokenTLV.length + BKpubMacTLV.length + anchorCertificateTLV.length];
        System.arraycopy(controllerTokenTLV, 0, finalContentByteArray, 0, controllerTokenTLV.length);
        System.arraycopy(BKpubMacTLV, 0, finalContentByteArray, controllerTokenTLV.length, BKpubMacTLV.length);
        System.arraycopy(anchorCertificateTLV, 0, finalContentByteArray,
                controllerTokenTLV.length + BKpubMacTLV.length, anchorCertificateTLV.length);

        Blob content = new Blob(finalContentByteArray);

        Name dataName = new Name(name);

        dataName.appendVersion(System.currentTimeMillis());
        Data data = new Data(dataName);
        data.setContent(content);

        return data;

    }

    public static Data constructCertificateRequestResponse(Name name, CertificateV2 CKpubCertificate) {

        Data data = new Data(name);
        data.getName().appendVersion(System.currentTimeMillis());
        data.setContent(CKpubCertificate.wireEncode());

        return data;

    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    // encoding helper functions specific to split implementation

    public static byte[] convertControllerTokenToByteArray(long[] controllerToken) {
        byte[] controllerTokenByteArray = new byte[32];

        byte[] controllerTokenSubByteArray0 = longToBytes(controllerToken[0]);
        ArrayUtils.reverse(controllerTokenSubByteArray0);
        Blob controllerTokenSubByteArray0Blob = new Blob(controllerTokenSubByteArray0);
        Log.d(TAG, "Controller token integer 0 value: " + controllerToken[0]);
        Log.d(TAG, "Controller token sub byte array 0 hex string: " + controllerTokenSubByteArray0Blob.toHex());
        byte[] controllerTokenSubByteArray1 = longToBytes(controllerToken[1]);
        ArrayUtils.reverse(controllerTokenSubByteArray1);
        Blob controllerTokenSubByteArray1Blob = new Blob(controllerTokenSubByteArray1);
        Log.d(TAG, "Controller token integer 1 value: " + controllerToken[1]);
        Log.d(TAG, "Controller token sub byte array 1 hex string: " + controllerTokenSubByteArray1Blob.toHex());
        byte[] controllerTokenSubByteArray2 = longToBytes(controllerToken[2]);
        ArrayUtils.reverse(controllerTokenSubByteArray2);
        Blob controllerTokenSubByteArray2Blob = new Blob(controllerTokenSubByteArray2);
        Log.d(TAG, "Controller token integer 2 value: " + controllerToken[2]);
        Log.d(TAG, "Controller token sub byte array 2 hex string: " + controllerTokenSubByteArray2Blob.toHex());
        byte[] controllerTokenSubByteArray3 = longToBytes(controllerToken[3]);
        ArrayUtils.reverse(controllerTokenSubByteArray3);
        Blob controllerTokenSubByteArray3Blob = new Blob(controllerTokenSubByteArray3);
        Log.d(TAG, "Controller token integer 3 value: " + controllerToken[3]);
        Log.d(TAG, "Controller token sub byte array 3 hex string: " + controllerTokenSubByteArray3Blob.toHex());

        System.arraycopy(controllerTokenSubByteArray0, 0, controllerTokenByteArray, 0, 8);
        System.arraycopy(controllerTokenSubByteArray1, 0, controllerTokenByteArray, 8, 8);
        System.arraycopy(controllerTokenSubByteArray2, 0, controllerTokenByteArray, 16, 8);
        System.arraycopy(controllerTokenSubByteArray3, 0, controllerTokenByteArray, 24, 8);

        Blob controllerTokenByteArrayBlob = new Blob(controllerTokenByteArray);
        Log.d(TAG, "Controller token byte array hex string (inside conversion): " + controllerTokenByteArrayBlob.toHex());

        return controllerTokenByteArray;

    }

}
