
#include "tlv-helpers.hpp"

#include "../logger.hpp"
#include "../consts.hpp"
#include "print-byte-array-helper.hpp"
#include <esp8266ndn.h>

#define LOG(...) LOGGER(TlvHelper, __VA_ARGS__)

bool
readTypeAndLength(ndn_TlvDecoder &decoder, uint8_t type, size_t &tlvTypeAndLengthSize, size_t &tlvValueSize, String debugID)
{
  size_t initialOffset = decoder.offset;

  ndn_Error scanResult = ndn_TlvDecoder_readTypeAndLength(&decoder, type, &tlvValueSize);
  if (scanResult != NDN_ERROR_success) {
    LOG("Failed to parse " << debugID << " from tlv." << endl);
    LOG("NDN Error: " << ndn_getErrorString(scanResult) << endl);
    return false;
  }

  tlvTypeAndLengthSize = decoder.offset - initialOffset;

  LOG("Successfully parsed " << debugID << " from tlv." << endl);
  LOG("Tlv type and length size: " << PriUint64<DEC>(tlvTypeAndLengthSize));
  LOG("Tlv value size: " << PriUint64<DEC>(tlvValueSize));
  LOG("Contents of TLV value: " << endl << PrintByteArray(decoder.input + decoder.offset, 0, tlvValueSize) << endl);

  decoder.offset += tlvValueSize;

  return true;
}