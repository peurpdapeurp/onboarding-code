#ifndef CERTIFICATE_REQUEST_RESPONSE_PARSER_HPP
#define CERTIFICATE_REQUEST_RESPONSE_PARSER_HPP

#include <esp8266ndn.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class CertificateRequestResponseParser
{
public:
  explicit
  CertificateRequestResponseParser(const uint8_t* payload, size_t payloadLen);

  ~CertificateRequestResponseParser();

public:
  // returns true if it successfully finds all the expected TLV types and TLV lengths with
  // correct lengthed TLV values
  // returns false if there are any errors during parsing
  bool
  checkForExpectedTlvs();

  bool
  verifyReceivedCertificateAndGetDeviceID(const uint8_t *AKpub, ndn::NameLite &nameDestination);

  // populates destination name with controller assigned device ID (one name component)
  bool
  getDeviceID(ndn::NameLite &nameDestination);

private:
  const uint8_t* m_payload;
  size_t m_payloadLen;

  ndn_TlvDecoder m_received_certificate_decoder;

  size_t m_receivedCertificateTlvTypeAndLengthSize;
  size_t m_receivedCertificateTlvValueSize;

  char m_keyNameComponentString[4] = "KEY";
  ndn_NameComponent m_keyNameComp[1];
  ndn::NameLite m_keyNameComponent;

};

#endif // CERTIFICATE_REQUEST_RESPONSE_PARSER_HPP