
#include "security-helper.hpp"
#include "../consts.hpp"
#include <security/detail/uECC.h>
#include <esp8266ndn.h>
#include "../logger.hpp"

#define LOG(...) LOGGER(SecurityHelper, __VA_ARGS__)

SecurityHelper::SecurityHelper()
{
  mbedtls_md_init(&m_ctx);
  mbedtls_md_setup(&m_ctx, mbedtls_md_info_from_type(m_md_type), 1);
}

SecurityHelper::~SecurityHelper()
{
  mbedtls_entropy_free(&m_entropy);
  mbedtls_ctr_drbg_free(&m_ctr_drbg);
  mbedtls_md_free(&m_ctx);
}

void
SecurityHelper::generateDeviceToken(uint64_t *deviceTokenDestination, uint32_t *deviceSecret) {
  deviceTokenDestination[0] = Montgomery(DH_G, deviceSecret[0], DH_P);
  deviceTokenDestination[1] = Montgomery(DH_G, deviceSecret[1], DH_P);
  deviceTokenDestination[2] = Montgomery(DH_G, deviceSecret[2], DH_P);
  deviceTokenDestination[3] = Montgomery(DH_G, deviceSecret[3], DH_P);
}

void
SecurityHelper::deriveSharedSecretSplit(const uint8_t *controllerTokenRawBytes, uint32_t *deviceSecret, uint8_t *sharedSecret) {

  uint64_t sharedSecretInteger0, sharedSecretInteger1, sharedSecretInteger2, sharedSecretInteger3; 

  uint64_t controllerTokenInteger0 = *((uint64_t *)controllerTokenRawBytes);
  uint64_t controllerTokenInteger1 = *((uint64_t *)(controllerTokenRawBytes + 8));
  uint64_t controllerTokenInteger2 = *((uint64_t *)(controllerTokenRawBytes + 16));
  uint64_t controllerTokenInteger3 = *((uint64_t *)(controllerTokenRawBytes + 24));

  LOG("Controller integer 0:" << PriUint64<DEC>(controllerTokenInteger0));
  LOG("Controller integer 1:" << PriUint64<DEC>(controllerTokenInteger1));
  LOG("Controller integer 2:" << PriUint64<DEC>(controllerTokenInteger2));
  LOG("Controller integer 3:" << PriUint64<DEC>(controllerTokenInteger3));

  sharedSecretInteger0 = Montgomery(controllerTokenInteger0, deviceSecret[0], DH_P);
  sharedSecretInteger1 = Montgomery(controllerTokenInteger1, deviceSecret[1], DH_P);
  sharedSecretInteger2 = Montgomery(controllerTokenInteger2, deviceSecret[2], DH_P);
  sharedSecretInteger3 = Montgomery(controllerTokenInteger3, deviceSecret[3], DH_P);

  memcpy(sharedSecret, &sharedSecretInteger0, 8);
  memcpy(sharedSecret + 8, &sharedSecretInteger1, 8);
  memcpy(sharedSecret + 16, &sharedSecretInteger2, 8);
  memcpy(sharedSecret + 24, &sharedSecretInteger3, 8);

}

void
SecurityHelper::generateBKpubMacByControllerToken(const uint8_t *controllerToken, const uint8_t *BKpub, uint8_t *BKpubMacByControllerToken) {

  mbedtls_md_hmac_starts(&m_ctx, controllerToken, 32);
  mbedtls_md_hmac_update(&m_ctx, BKpub, 64);
  mbedtls_md_hmac_finish(&m_ctx, BKpubMacByControllerToken);

}

bool
SecurityHelper::checkBKpubMacFromController(const uint8_t *myBKpubMac, const uint8_t *controllerBKpubMac)
{
  for (int i = 0; i < ndn_SHA256_DIGEST_SIZE; i++) {
    if (myBKpubMac[i] != controllerBKpubMac[i])
      return false;
  }

  return true;
}

bool
SecurityHelper::verifyDataByTSK(const uint8_t *tskBytes, const ndn::DataLite &data)
{
  ndn_NameComponent dataNameComps[MAX_NAME_COMPS];
  ndn_NameComponent dataKeyLocatorNameComps[MAX_NAME_COMPS];
  ndn::DataLite tempData(dataNameComps, MAX_NAME_COMPS, dataKeyLocatorNameComps, MAX_NAME_COMPS);

  tempData.set(data);

  ndn::HmacKey tsk(tskBytes, ndn_SHA256_DIGEST_SIZE);

  uint8_t encoding[3000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeData
      (tempData, &signedPortionBeginOffset,
      &signedPortionEndOffset, output, &encodingLength))) {
    LOG("Error encoding data from bootstrapping response, in order to do verification by TSK.");
    LOG("Error: " << ndn_getErrorString(error) << endl);
    return false;
  }

    if (tsk.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
        tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
      LOG("Successfully verified bootstrapping request response data packet.");
    }
    else {
      LOG("Bootstrapping request response data packet failed to verify.");
      return false;
    }

    return true;
}

uint64_t 
SecurityHelper::Montgomery(uint64_t n, uint32_t p, uint64_t m)     
{      
    uint64_t r = n % m;     
    uint64_t tmp = 1;     
    while (p > 1)     
    {     
        if ((p & 1)!=0)     
        {     
            tmp = (tmp * r) % m;     
        }     
        r = (r * r) % m;     
        p >>= 1;     
    }     
    return (r * tmp) % m;     
}