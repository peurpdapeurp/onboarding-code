#ifndef SECURITY_HELPER_HPP
#define SECURITY_HELPER_HPP

#include <stdint.h>
#include <stddef.h>

#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/entropy.h"    // entropy gathering
#include "mbedtls/ctr_drbg.h"   // CSPRNG
#include "mbedtls/error.h"      // error code to string conversion
#include "mbedtls/md.h"         // hash algorithms
#include "mbedtls/sha256.h"

#include <esp8266ndn.h>

/** \brief Parser to interpret the response to the bootstrapping request.
 */
class SecurityHelper
{
public:
  explicit
  SecurityHelper();

  ~SecurityHelper();

  void
  sha256(const uint8_t* input, size_t inputLen, uint8_t* output);

  bool
  verifyDataByTSK(const uint8_t *tskBytes, const ndn::DataLite &data);

  // security helper functions specific to split implementation

  void
  generateDeviceToken(uint64_t *deviceTokenDestination, uint32_t *device_secret);

  void
  deriveSharedSecretSplit(const uint8_t *controllerTokenRawBytes, uint32_t *deviceSecret, uint8_t *sharedSecret);

  void
  generateBKpubMacByControllerToken(const uint8_t *controllerToken, const uint8_t *BKpub, uint8_t *BKpubMacByControllerToken);

  bool
  checkBKpubMacFromController(const uint8_t *myBKpubMac, const uint8_t *controllerBKpubMac);

private:
  uint64_t
  Montgomery(uint64_t n, uint32_t p, uint64_t m);

private:

private:
  mbedtls_entropy_context m_entropy;
  mbedtls_ctr_drbg_context m_ctr_drbg;
  mbedtls_md_context_t m_ctx;
  mbedtls_md_type_t m_md_type = MBEDTLS_MD_SHA256;

};

#endif // SECURITY_HELPER_HPP