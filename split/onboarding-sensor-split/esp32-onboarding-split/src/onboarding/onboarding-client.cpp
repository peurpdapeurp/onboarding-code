#include "onboarding-client.hpp"
#include "../logger.hpp"
#include "../credentials.hpp"

#include <lwip/def.h>

#include <security/detail/uECC.h>
#include "mbedtls/sha256.h"

#include "../helpers/print-byte-array-helper.hpp"
#include "../helpers/other-helpers.hpp"

#include "bootstrapping-response-parser.hpp"
#include "certificate-request-response-parser.hpp"

#include <PriUint64.h>

#define LOG(...) LOGGER(OnboardingClient, __VA_ARGS__)

OnboardingClient::OnboardingClient(ndn::Face& face, const uint8_t* BKpub, const uint8_t* BKpri)
  : m_face(face)
  , m_bootstrapPrefix(m_bootstrapPrefixComps, 2)
  , m_certNameComponent(m_certNameComponentComps, 1)
  , m_dummyKeyName(m_dummyKeyComp, 1)
  , m_bootstrapPrivateKey(m_dummyKeyName)
  , m_deviceID(m_deviceIDNameComp, 1)
  , m_networkPrefix(m_networkPrefixComps, MAX_NAME_COMPS)
{
  m_BKpub = BKpub;
  m_BKpri = BKpri;

  m_bootstrapPrivateKey.import(BKpri);

  ndn_digestSha256(m_BKpub, 91, m_ECC_PUBLIC_DIGEST);

  for (int i = 0; i < 63; i += 2) {
    putInCharBuf(m_BKpubDigestChars, i, m_ECC_PUBLIC_DIGEST[i / 2]);
  }
  m_BKpubDigestChars[64] = '\0';

  ndn::parseNameFromUri(m_bootstrapPrefix, m_bootstrapPrefixString);
  ndn::parseNameFromUri(m_certNameComponent, m_certNameComponentString);
  
  LOG("Checking that bootstrap prefix was parsed correctly: " << ndn::PrintUri(m_bootstrapPrefix) << endl);
  LOG("Checking that certificate request name component was parsed correctly " << ndn::PrintUri(m_certNameComponent) << endl);

  // making sure that m_AKpub is all zeros so we can check to see if it has been initialized
  for (int i = 0; i < 64; i++) {
    m_AKpub[i] = 0;
  }

  m_onboarding_completed = false;
}


OnboardingClient::~OnboardingClient()
{
  m_face.removeHandler(this);
}

bool
OnboardingClient::begin(const ndn::NameLite& topName)
{
  m_face.addHandler(this);

  return true;
}

bool
OnboardingClient::initiateSignOn()
{
  sendBootstrappingRequest();
}

bool
OnboardingClient::sendBootstrappingRequest()
{
  ndn_NameComponent nameComps[10];
  ndn::InterestLite interest(nameComps, 10, nullptr, 0, nullptr, 0);
  interest.setName(m_bootstrapPrefix);
  interest.getName().append(m_BKpubDigestChars);
  interest.setMustBeFresh(true);

  // generate device secret
  /*
  m_device_secret[0] = esp_random();
  m_device_secret[1] = esp_random();
  m_device_secret[2] = esp_random();
  m_device_secret[3] = esp_random();
  */
  m_device_secret[0] = 4026398525;
  m_device_secret[1] = 2536564357;
  m_device_secret[2] = 1594809088;
  m_device_secret[3] = 2390369021;

  m_securityHelper.generateDeviceToken(m_device_token, m_device_secret);

  LOG(
    "Generated device secret:" << endl << 
    "Integer 0: " << PriUint64<DEC>(m_device_secret[0]) << endl <<
    "Integer 1: " << PriUint64<DEC>(m_device_secret[1]) << endl <<
    "Integer 2: " << PriUint64<DEC>(m_device_secret[2]) << endl <<
    "Integer 3: " << PriUint64<DEC>(m_device_secret[3]) << endl
  );

  uint8_t device_token_temp_array[32];
  memcpy(device_token_temp_array, m_device_token, 32);
  LOG("Bytes of generated device token: " << endl << PrintByteArray(device_token_temp_array, 0, 32));

  interest.getName().append(device_token_temp_array, 32);

  m_face.setSigningKey(m_bootstrapPrivateKey);
  m_face.sendSignedInterest(interest);

  LOG("<I " << ndn::PrintUri(interest.getName()) << endl << endl);

  return true;
}

bool
OnboardingClient::sendCertificateRequest()
{
  LOG("Sending certificate request interest.");

  ndn_NameComponent nameComps[MAX_NAME_COMPS];
  ndn::InterestLite interest(nameComps, MAX_NAME_COMPS, nullptr, 0, nullptr, 0);

  for (int i = 0; i < m_networkPrefix.size(); i++) {
    interest.getName().append(m_networkPrefix.get(i));
  }
  interest.getName().append("cert");
  interest.getName().append(m_BKpubDigestChars);

  uECC_make_key(m_CKpub, m_CKpri);

  LOG("Bytes of generated CKpub:" << endl << PrintByteArray(m_CKpub, 0, 64));

  LOG("Bytes of generated CKpri:" << endl << PrintByteArray(m_CKpri, 0, 32));

  interest.getName().append(m_CKpub, 64);

  // interest.setCanBePrefix(true);
  interest.setMustBeFresh(true);

  ndn::HmacKey tsk(m_sharedSecret, 32);

  m_face.setSigningKey(tsk);
  m_face.sendSignedInterest(interest);
  LOG("<I " << ndn::PrintUri(interest.getName()) << endl);

  return true;
}

bool
OnboardingClient::processData(const ndn::DataLite& data, uint64_t endpointId)
{
  LOG("<D " << ndn::PrintUri(data.getName()) << endl << endl);

  const ndn::NameLite& name = data.getName();

  bool (OnboardingClient::*f)(const ndn::DataLite&, uint64_t) = nullptr;
  if (name.get(-7).equals(m_bootstrapPrefix.get(0)) &&
      name.get(-6).equals(m_bootstrapPrefix.get(1))) {
    LOG("Received data in response to bootstrapping request." << endl);
    f = &OnboardingClient::processBootstrappingInterestResponse;
  }
  else if (name.get(-6).equals(m_certNameComponent.get(0))) {
    LOG("Received data in response to certificate request." << endl);
    f = &OnboardingClient::processCertificateRequestInterestResponse;
  }
  else {
    LOG("Did not recognize data response as bootstrapping response or certificate request response." << endl);
    return false;
  }

  return (this->*f)(data, endpointId);
}

bool
OnboardingClient::processBootstrappingInterestResponse(const ndn::DataLite& data, uint64_t endpointId)
{

  LOG("Processing bootstrapping interest response..." << endl);

  const uint8_t* dataContentBytes = data.getContent().buf();

  BootstrappingResponseParser bootstrappingResponseParser(dataContentBytes, data.getContent().size());

  if (!bootstrappingResponseParser.checkForExpectedTlvs()) {
    LOG("Failed to parse bootstrapping response data content for expected TLV's." << endl);
    return false;
  }

  LOG("Successfully parsed response data content for expected TLV's." << endl);

  m_securityHelper.deriveSharedSecretSplit(dataContentBytes + bootstrappingResponseParser.getControllerTokenTlvTypeAndLengthSize(), 
    m_device_secret, m_sharedSecret);

  LOG("Bytes of generated shared secret:" << endl << PrintByteArray(m_sharedSecret, 0, 32) << endl);

  if (!m_securityHelper.verifyDataByTSK(m_sharedSecret, data)) {
    LOG("Failed to verify response data by TSK, ignoring bootstrapping response...");
    return false;
  }
  else {
    LOG("Successfully verified bootstrapping response by tsk.");
  }

  m_securityHelper.generateBKpubMacByControllerToken(dataContentBytes + bootstrappingResponseParser.getControllerTokenTlvTypeAndLengthSize(),
  m_BKpub + 27, m_BKpubMacByControllerToken);

  if (!m_securityHelper.checkBKpubMacFromController(m_BKpubMacByControllerToken, 
  dataContentBytes + bootstrappingResponseParser.getBKpubMacValueOffset())) {
    LOG("Got the incorrect MAC of BKpub by controller token from controller, ignoring bootstrapping response...");
    return false;
  }
  else {
    LOG("Got correct MAC of BKpub by controller token from controller.");
  }

  if (!bootstrappingResponseParser.getNetworkPrefixAndAKpub(m_networkPrefix, m_AKpub)) {
    LOG("Failed to get network prefix and / or AKpub from received trust anchor.");
    return false;
  }
  else {
    LOG("Network prefix received: " << ndn::PrintUri(m_networkPrefix));
  }

  m_lastBootstrapResponse.reset(m_face.swapPacketBuffer(nullptr));

  sendCertificateRequest();
  
  return true;

}

bool
OnboardingClient::processCertificateRequestInterestResponse(const ndn::DataLite& data, uint64_t endpointId)
{
  
  LOG("Processing certificate request response..." << endl);

  if (!m_securityHelper.verifyDataByTSK(m_sharedSecret, data)) {
    LOG("Failed to verify certificate request response by TSK.");
  } else {
    LOG("Successfully verified certificate request response by TSK.");
  }

  const uint8_t* dataContentBytes = data.getContent().buf();

  CertificateRequestResponseParser certificateRequestResponseParser(dataContentBytes, data.getContent().size());
  if (!certificateRequestResponseParser.checkForExpectedTlvs()) {
    LOG("Failed to parse certificate request response data content for expected TLV's." << endl);
    return false;
  }

  if (!certificateRequestResponseParser.verifyReceivedCertificateAndGetDeviceID(m_AKpub, m_deviceID)) {
    m_deviceID.clear();
    LOG("Failed to verify received certificate or get device ID." << endl);
    return false;
  }
  else {
    LOG("Device ID assigned by controller: " << ndn::PrintUri(m_deviceID));
  }

  m_onboarding_completed = true;
  
}