#ifndef CONNECTIVITY_HPP
#define CONNECTIVITY_HPP

#include "consts.hpp"

#include <esp8266ndn.h>
#include <WiFi.h>

/** \brief Establish and maintain NDN connectivity over WiFi.
 */
class ConnectivityClass
{
public:
  ConnectivityClass();

  void
  begin();

  void
  loop();

  IPAddress
  localIP();

  ndn::Face&
  getFace()
  {
    return m_face;
  }

  const ndn::NameLite&
  getTopName() const
  {
    return m_topName;
  }

private:
  ndn::UdpTransport m_transport;
  ndn::Face m_face;

  ndn_NameComponent m_topComps[TOP_PREFIX_NCOMPS];
  ndn::NameLite m_topName;

  ndn_NameComponent m_pingComps[TOP_PREFIX_NCOMPS + 1];
  ndn::NameLite m_pingName;
  ndn::PingServer m_pingServer;
};

extern ConnectivityClass Connectivity;

#endif // CONNECTIVITY_HPP
