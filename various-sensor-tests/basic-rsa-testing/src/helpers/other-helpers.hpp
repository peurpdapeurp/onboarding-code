#ifndef OTHER_HELPERS_HPP
#define OTHER_HELPERS_HPP

#include <stdint.h>

void
putInCharBuf(char *buf, int pos, uint8_t X);

#endif // OTHER_HELPERS_HPP