#include "connectivity.hpp"
#include "credentials.hpp"
#include "consts.hpp"
#include "logger.hpp"

#include "mbedtls/rsa.h"
#include "mbedtls/pk.h"

#include "helpers/print-byte-array-helper.hpp"

#define LOG(...) LOGGER(Main, __VA_ARGS__)

mbedtls_rsa_context m_rsa_context;
mbedtls_pk_context m_pk_context;

void
setup()
{
  Serial.begin(9600);
  Serial.println();
  ndn::setLogOutput(Serial);

  Connectivity.begin();

  LOG("Made it past initializing connectivity.");

  mbedtls_pk_init(&m_pk_context);
  int signResult = mbedtls_pk_parse_key(&m_pk_context, RSA_3072_KEY_PAIR_PEM_FILE, sizeof(RSA_3072_KEY_PAIR_PEM_FILE), nullptr, 0);
  if (signResult != 0)
    LOG("Parsing RSA private key file failed: " << signResult << endl);

  uint8_t clearText[10] = { 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10};
  uint8_t clearTextSha256Hash[ndn_SHA256_DIGEST_SIZE];
  ndn_digestSha256(clearText, 10, clearTextSha256Hash);
  size_t sigLength;
  uint8_t sigBuf[500];

  mbedtls_pk_sign(&m_pk_context, MBEDTLS_MD_SHA256, clearTextSha256Hash, ndn_SHA256_DIGEST_SIZE, 
    sigBuf, &sigLength, NULL, NULL);

  LOG("Value of cleartext: " << endl << PrintByteArray(clearText, 0, 10) << endl);
  LOG("Value of signature generated: " << endl << PrintByteArray(sigBuf, 0, sigLength) << endl);

  int verifyResult = mbedtls_pk_verify(&m_pk_context, MBEDTLS_MD_SHA256, clearTextSha256Hash, ndn_SHA256_DIGEST_SIZE,
    sigBuf, sigLength);

  if (verifyResult != 0)
    LOG("Failed to verify signature." << endl);
  else 
    LOG("Signature succeeded in verifying." << endl);

}

void
loop()
{
  Connectivity.loop();
  delay(1);
}
