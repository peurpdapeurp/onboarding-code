#ifndef CONSTS_HPP
#define CONSTS_HPP

#include <cstddef>
#include <stdint.h>
#include <uECC.h>

#define LOG_OUTPUT Serial

#define PIN_BTN 0

#define IP_CLIENT IPAddress(192, 168, 137, 212)
#define IP_SERVER IPAddress(192, 168, 137, 187)
#define IP_SUBNET IPAddress(255, 255, 255, 0)
#define UDP_PORT 6363

#ifdef DEBUG
#undef DEBUG
#endif
#define DEBUG 1

// maximum number of name components that we will process
constexpr int MAX_NAME_COMPS = 15;

const unsigned char EC_SECP256R1_PRIVATE_PEM[] = "-----BEGIN EC PRIVATE KEY-----\nMHcCAQEEIBZg5VHzSqBDYHQJvqeYe9b5XLK5eLnq9yQouRrAssvGoAoGCCqGSM49\nAwEHoUQDQgAEnfqOOR4uADSPBSrru4tfmjkiJHBFByaVR9+n2SRRXz/Z1WZyMz9S\nBsOH7lcfL9wYEE5FYb7hIaHEV/5VQniwzA==\n-----END EC PRIVATE KEY-----";

const unsigned char EC_SECP160R1_PRIVATE_PEM[] = "-----BEGIN EC PRIVATE KEY-----\nMFECAQEEFQCQthWXZ80g0ZOzagxqaRKnfhcwJ6AHBgUrgQQACKEsAyoABNgoBwyP\nilb24cLZVEbIebIec81wwBc3XSCXVJ4T6kAtCAscdwyLWm4=\n-----END EC PRIVATE KEY-----";

const unsigned char DH_PARAMS_PEM[] = "-----BEGIN DH PARAMETERS-----\nMIIBCAKCAQEA7WJTTl5HMXOi8+kEeze7ftMRbIiX+P7tLkmwci30S+P6xc6wG1p4\nSwbpPyewFlyasdL2Dd8PkhYFtE1xD3Ssj1De+P8T0UcJn5rCHn+g2+0k/CalysKT\nXrobEzihlSLeQO1NsgBt1F1XCMO+6inLVvSGVbb3Cei4q+5Djnc7Yjjq0kxGY6Hd\nds/YQnyc1xdJU8NBi3zO1XY2Uk6BSd+NN5KnLh9zRq8t/b0RiIb/fY9mJ9BCtgPo\n2m4AfJE8+5dE1ttpQAJFSlA8Ku3/9Vp8sMMWATVk2Q1z9PdkikKQYRfMPYDBSIa/\n8Y2l9Hh7vNYOwXd4WF5Q55RHP46RB+F+swIBAg==\n-----END DH PARAMETERS-----";

#endif // CONSTS_HPP
